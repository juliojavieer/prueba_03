/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utilidades.Oraconex;

public class Articulo {

    private int codigo_articulo;
    private String rut_pvd;
    private String descripcion;
    private int largo;
    private int ancho;
    private int alto;
    private int precio;
    private String estado_asegurado;
    private int id_clasificacion;

    public Articulo() {
    }

    public Articulo(int codigo_articulo, String rut_pvd, String descripcion, int largo, int ancho, int alto, int precio, String estado_asegurado, int id_clasificacion) {
        this.codigo_articulo = codigo_articulo;
        this.rut_pvd = rut_pvd;
        this.descripcion = descripcion;
        this.largo = largo;
        this.ancho = ancho;
        this.alto = alto;
        this.precio = precio;
        this.estado_asegurado = estado_asegurado;
        this.id_clasificacion = id_clasificacion;
    }

        public int insertarListaSalida(){
        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();

        String sqlInsert = "insert into Articulo  values(?,?,?,?,?,?,?,?,?)";

        try {
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setInt(1, this.codigo_articulo);
            psipc.setString(2, this.rut_pvd);
            psipc.setString(3, this.descripcion);
            psipc.setInt(4, this.largo);
            psipc.setInt(5,this.ancho);
            psipc.setInt(6,this.alto);
            psipc.setInt(7,this.precio);
            psipc.setString(8,this.estado_asegurado);
            psipc.setInt(9,this.id_clasificacion);
            filasAfectadas = psipc.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Articulo.insertarArticulo(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }
        public Articulo buscarArticulo(){
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from Articulo where codigo_articulo like ? ";
        Articulo artRespuesta = null;
        try {
            PreparedStatement psba = oraconex.prepareStatement(sqlSelect);
            psba.setInt(1, this.codigo_articulo);
            ResultSet rs = psba.executeQuery();

            rs.next();
            int coar = rs.getInt(1);
            System.out.println("codigo articulo : "+coar);
            String rpro = rs.getString(2);
            System.out.println("rproveedor : "+rpro);
            String desc = rs.getString(3);
            System.out.println("descripcion : "+desc);
            int lar = rs.getInt(4);
            System.out.println("largo : "+lar);
            int anc = rs.getInt(5);
            System.out.println("ancho : "+anc);
            int alt = rs.getInt(6);
            System.out.println("alto : "+alt);
            int pre = rs.getInt(7);
            System.out.println("precio : "+pre);
            String esase = rs.getString(8);
            System.out.println("estado asegurado: "+esase);
            int idcla = rs.getInt(9);
            System.out.println("idcla : "+idcla);

            
            artRespuesta = new Articulo(coar,rpro,desc,lar,anc,alt,pre,esase,idcla);

        } catch (SQLException ex) {
            System.out.println("Cliente.buscarCliente(): " + ex.getMessage());
            return null;
        } finally {
            conora.closeConexionOracle();
            return artRespuesta;
        }
    }
    @Override
    public String toString() {
        return  codigo_articulo + "|" + rut_pvd + "|" + descripcion + "|" + largo + "|" + ancho + "|" + alto + "|" + precio + "|" + estado_asegurado + "|" + id_clasificacion;
    }
}
