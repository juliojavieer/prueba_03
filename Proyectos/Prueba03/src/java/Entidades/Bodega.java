
package Entidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utilidades.Oraconex;

public class Bodega {
    private int id_bodega;
    private String nombre_bodega;
    private String todasBodegas;

    public Bodega() {
    }

    public Bodega(int id_bodega, String nombre_bodega) {
        this.id_bodega = id_bodega;
        this.nombre_bodega = nombre_bodega;
    }

    public int insertarBodega() {

        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlInsert = "insert into bodega  values(?,?)";
        try {
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setInt(1, this.id_bodega);
            psipc.setString(2, this.nombre_bodega);
            filasAfectadas = psipc.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Bodega.insertarBodega(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;

        }

    }
        public Bodega buscarBodega(){
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from Bodega where id_bodega like ? ";
        Bodega bodRespuesta = null;
        try {
            PreparedStatement psbb = oraconex.prepareStatement(sqlSelect);
            psbb.setInt(1, this.id_bodega);
            ResultSet rs = psbb.executeQuery();

            rs.next();
            int id = rs.getInt(1);
            System.out.println("id bodega : "+id);
            String nom = rs.getString(2);
            System.out.println("nom bodega : "+nom);
            String ubi = rs.getString(3);
            
            
            bodRespuesta = new Bodega(id,nom);

        } catch (SQLException ex) {
            System.out.println("Bodega.buscarBodega(): " + ex.getMessage());
            return null;
        } finally {
            conora.closeConexionOracle();
            return bodRespuesta;
        }
    }
    
    public String getTodasBodegas(){
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        System.out.println("bod"+this.id_bodega);
        String sqlSelect = "select id_bodega from Bodega where id_bodega like '%" + this.id_bodega + "%' ";
        int cont = 0;
        try {
            PreparedStatement psbb = oraconex.prepareStatement(sqlSelect);

            ResultSet rs = psbb.executeQuery();
            while (rs.next()) {
                cont++;
            }
            System.out.println("contador" + cont);
            this.todasBodegas = "";
            rs = psbb.executeQuery();
            int i = 0;
            while (rs.next()) {
                this.todasBodegas += rs.getInt(1)+"|";
                System.out.println("bodegas : " + this.todasBodegas);
                i++;
            }
        } catch (Exception e) {return null;
        } finally {
            conora.closeConexionOracle();
            return this.todasBodegas.substring(0,this.todasBodegas.length()-1);
        }
    }
    
    @Override
    public String toString() {
        return id_bodega + "|" + nombre_bodega;
    }
    
}
