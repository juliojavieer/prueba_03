package Entidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import utilidades.Oraconex;

public class Entra_a_ubicacion {

    private String fecha_hora_ingreso;
    private int id_ubicacion;
    private String fecha_retiro_total;
    private int codigo_articulo;
    private int cantidad_inicial;
    private int stock;
    private int codigo_entrada;
    private String todosEntra_a_ubicacion;

    public Entra_a_ubicacion() {
    }

    public Entra_a_ubicacion(String fecha_hora_ingreso, int codigo_articulo, String fecha_retiro_total, int id_ubicacion, int cantidad_inicial, int stock, int codigo_entrada) {
        this.fecha_hora_ingreso = fecha_hora_ingreso;
        this.id_ubicacion = id_ubicacion;
        this.fecha_retiro_total = fecha_retiro_total;
        this.codigo_articulo = codigo_articulo;
        this.cantidad_inicial = cantidad_inicial;
        this.stock = stock;
        this.codigo_entrada = codigo_entrada;
    }

    public int insertarEntra_a_ubicacion() {

        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();

        Connection oraconex = conora.getConexionOracle();
        String sqlInsert = "insert into entra_a_ubicacion values(?,?,?,?,?,?,?)";
        try {

            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setString(1, this.fecha_hora_ingreso);
            psipc.setInt(2, this.id_ubicacion);
            psipc.setInt(3, this.codigo_articulo);
            psipc.setString(4, this.fecha_retiro_total);
            psipc.setInt(5, this.cantidad_inicial);
            psipc.setInt(6, this.stock);
            psipc.setInt(7, this.codigo_entrada);

            filasAfectadas = psipc.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Entra_a_ubicacion.insertarEntra_a_ubicacion(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;

        }

    }

    public Entra_a_ubicacion buscarEntra_a_ubicacion() {
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select *from entra_a_ubicacion where fecha_hora_ingreso like ? ";
        Entra_a_ubicacion entra_a_ubic = null;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);
            psbc.setInt(1, this.codigo_entrada);
            ResultSet rs = psbc.executeQuery();

            rs.next();
            String fecha_hora = rs.getString(1);
            System.out.println("fecha hora ingreso : " + fecha_hora);
            int cod_art = rs.getInt(2);
            System.out.println("codigo articulo : " + cod_art);
            String fecha_retiro = rs.getString(3);
            System.out.println("Fecha retiro total : " + fecha_retiro);
            int id_ubica = rs.getInt(4);
            System.out.println("id ubicacion : " + id_ubica);
            int canti = rs.getInt(5);
            System.out.println("cantidad inicial  : " + canti);
            int stoc = rs.getInt(6);
            System.out.println("stock : " + stoc);
            int cod_entra = rs.getInt(7);
            System.out.println("codigo entrada : " + cod_entra);

            entra_a_ubic = new Entra_a_ubicacion(fecha_hora, cod_art, fecha_retiro, id_ubica, canti, stoc, cod_entra);

        } catch (SQLException ex) {
            System.out.println("Entra_a_ubicacion.buscarEntra_a_ubicacion(): " + ex.getMessage());
            return null;
        } finally {
            conora.closeConexionOracle();
            return entra_a_ubic;
        }
    }

    public String getTodosDetalle_lista_cte() {

        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select folio from entra_a_ubicacion where fecha _hora_ingreso like '%" + this.fecha_hora_ingreso + "%'";
        int cont = 0;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);
            ResultSet rs = psbc.executeQuery();
            while (rs.next()) {
                cont++;
            }
            System.out.println("contador" + cont);
            this.todosEntra_a_ubicacion = "";
            rs = psbc.executeQuery();
            int i = 0;
            while (rs.next()) {
                this.todosEntra_a_ubicacion += rs.getString(1) + "|";
                System.out.println("todos entra a ubicacion : " + this.todosEntra_a_ubicacion);
                i++;
            }
        } catch (Exception e) {
            return null;
        } finally {
            conora.closeConexionOracle();
            return this.todosEntra_a_ubicacion.substring(0, this.todosEntra_a_ubicacion.length() - 1);
        }
    }

    @Override
    public String toString() {
        return fecha_hora_ingreso + "|" + codigo_articulo + "|" + fecha_retiro_total + "|" + id_ubicacion + "|" + cantidad_inicial + "|" + stock + "|" + codigo_entrada;
    }
}
