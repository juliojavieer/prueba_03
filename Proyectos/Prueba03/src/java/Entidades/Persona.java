package Entidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import utilidades.Oraconex;

public class Persona {

    private String rut;
    private String rsocial;
    private String direccion;
    private String ciudad;
    private String telefono;
    private String email;

    public Persona(String rut_pro, String rsocial, String direccion, String ciudad, String telefono, String email) {
        System.out.println("per " + rut);
        this.rut = rut_pro;
        this.rsocial = rsocial;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.telefono = telefono;
        this.email = email;
    }

    public Persona() {
    }

    public Persona(String rut) {
        this.rut = rut;
    }

    
    public int eliminarPersona() {
        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();

        String sqlInsert = "delete from persona where rut like ? ";

        try {
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setString(1, this.rut);

            filasAfectadas = psipc.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Persona.eliminarPersona(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }

    }

    public int insertarPersona() {
        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();

        String sqlInsert = "insert into persona  values(?,?,?,?,?,?)";

        try {
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setString(1, this.rut);
            psipc.setString(2, this.rsocial);
            psipc.setString(3, this.direccion);
            psipc.setString(4, this.ciudad);
            psipc.setString(5, this.telefono);
            psipc.setString(6, this.email);

            filasAfectadas = psipc.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Cliente.insertarPersona(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }

    public String getRut() {
        System.out.println("getRut" + this.rut);
        return this.rut;
    }

    @Override
    public String toString() {
        return this.rut + "|" + this.rsocial + "|" + this.direccion + "|" + this.ciudad + "|" + this.telefono + "|" + this.email;
    }
}