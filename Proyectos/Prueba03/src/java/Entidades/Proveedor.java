package Entidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utilidades.Oraconex;

public class Proveedor {

    private String rut_pro;
    private String rut_cliente;
    private String rsocial;
    private String direccion;
    private String ciudad;
    private String telefono;
    private String email;
    private String todosProovedores;
    Persona miper;

    public Proveedor(String rut_pro, String rut_cliente, String rsocial, String direccion, String ciudad, String telefono, String email) {

        this.rut_pro = rut_pro;
        this.rut_cliente = rut_cliente;
        this.rsocial = rsocial;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.telefono = telefono;
        this.email = email;
    }

    public Proveedor(String rut) {
        this.rut_pro = rut;
    }

    public Proveedor(String rut, String rut_cli) {
        this.rut_pro = rut;
        this.rut_cliente = rut_cli;
    }

    public int insertarProveedor() {
        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlInsert = "insert into proveedor values(?,?)";
        try {
            miper = new Persona(this.rut_pro, this.rsocial, this.direccion, this.ciudad, this.telefono, this.email);
            int fila = miper.insertarPersona();
            if (fila > 0) {
                PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
                psipc.setString(1, this.rut_pro);
                psipc.setString(2, this.rut_cliente);
                filasAfectadas = psipc.executeUpdate();
            } else {
                System.out.println("no se inserto");
            }
        } catch (SQLException ex) {
            System.out.println("Proveedor.insertarProveedor(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }

    public Persona buscarProveedor() {
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select *from persona inner join proveedor using (rut) where rut like ? and rut_cliente like ?";
        Persona proveedor = null;
        try {

            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);
            psbc.setString(1, this.rut_pro);
            psbc.setString(2, this.rut_cliente);
            ResultSet rs = psbc.executeQuery();

            rs.next();
            String rut1 = rs.getString(1);
            System.out.println("rut : " + rut1);
            //String rutCliente = rs.getString(2);
            //System.out.println("rut cliente : " + rutCliente);
            String rsoc = rs.getString(2);
            System.out.println("razon social : " + rsoc);
            String direc = rs.getString(3);
            System.out.println("dieccion : " + direc);
            String ciu = rs.getString(4);
            System.out.println("ciudad : " + ciu);
            String telef = rs.getString(5);
            System.out.println("teleofono : " + telef);
            String ema = rs.getString(6);
            System.out.println("email : " + ema);
            proveedor = new Persona(rut1, rsoc, direc, ciu, telef, ema);

        } catch (SQLException ex) {
            System.out.println("Proveedores.buscarProveedores(): " + ex.getMessage());
            return null;
        } finally {
            conora.closeConexionOracle();
            return proveedor;
        }
    }

    public int eliminarProveedor() {
        int filasAfectadas = 0;
        Persona per = new Persona(rut_pro);
        filasAfectadas=per.eliminarPersona();
        if(filasAfectadas>0){
            System.out.println("proveedor eliminado");
            return filasAfectadas;
        }else {
            System.out.println("proveedor no eliminado");
        }
        return filasAfectadas;
    }

    public String getTodosProveedores() {

        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from persona inner join proveedor using(rut) where rut like '%" + this.rut_pro + "%' and proveedor.rut_cliente like '%" + this.rut_cliente + "%'";
        int cont = 0;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);

            ResultSet rs = psbc.executeQuery();
            while (rs.next()) {
                cont++;
            }
            System.out.println("contador" + cont);
            this.todosProovedores = "";
            rs = psbc.executeQuery();
            int i = 0;
            while (rs.next()) {
                this.todosProovedores += rs.getString(1) + "," + rs.getString(2) + "," + rs.getString(3) + "," + rs.getString(4) + "," + rs.getString(5) + "," + rs.getString(6) + "|";
                i++;
            }
        } catch (Exception e) {
            return null;
        } finally {
            conora.closeConexionOracle();
            return this.todosProovedores.substring(0, this.todosProovedores.length() - 1);
        }
    }

    @Override
    public String toString() {
        return this.rut_pro + "|" + rut_cliente + "|" + this.rsocial;
    }
}
