package Servlet;

import Entidades.Camion;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "S_GestionCamion", urlPatterns = {"/S_GestionCamion"})
public class S_GestionCamion extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();


        try {
            String parametro_boton = request.getParameter("parametro_boton");
            System.out.println("parametro_boton: " + parametro_boton);
            HttpSession actual = request.getSession(true);
            String id = actual.getId();
            String rut_cliente = (String) actual.getAttribute("logueado");

            if (parametro_boton.equalsIgnoreCase("boton_guarda_camion")) {
                String elJSON = request.getParameter("jsonCamion");
                System.out.println("olas");
                 System.out.println(elJSON);
                Gson gson = new Gson();
                Camion jsonCamions = gson.fromJson(elJSON, Camion.class);
                System.out.println("jsonCamion: " + jsonCamions.toString());
                int filasIngresadas = jsonCamions.insertarCamion();
                if (filasIngresadas > 0) {
                    out.print("<font color='green'>Camion ingresado correctamente.</font>");
                } else {
                    out.print("<font color='red'>Error,Camion no ingresado.</font>");
                }
            }

        } catch (Exception e) {
            out.print("error, no se pudo ingresar el Camion");
            System.out.println("error: " + e.getMessage());
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
