package Servlet;

import Entidades.Usuarios;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utilidades.Oraconex;

/**
 *
 * @author jcf
 */
@WebServlet(name = "S_GestionUsuarios", urlPatterns = {"/S_GestionUsuarios"})
public class S_GestionUsuarios extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        if (request.getParameter("parametro_boton").equalsIgnoreCase("botonBuscarUsuarios")) {
            try {
                String usrJson = request.getParameter("jsonUsr");
                Gson gson = new Gson();
                Usuarios usr = gson.fromJson(usrJson, Usuarios.class);
                Statement st = oraconex.createStatement();
                String sql = "select type from usuarios where users='" + usr.getUsuario() + "' and pass='" + usr.getPass() + "'";
                ResultSet rs = st.executeQuery(sql);
                int i = 0;
                String type = "";
                while (rs.next()) {
                    i++;
                    type = rs.getString(1);
                }
                System.out.println("type: " + type);
                if (i == 1) {
                    HttpSession actual = request.getSession(true);
                    actual.setAttribute("logueado", usr.getUsuario());
                    if (type.equalsIgnoreCase("user")) {
                        out.print("Cliente.jsp");
                    }
                    else if(type.equalsIgnoreCase("admin")){
                        out.print("Administrador.jsp");
                    }
                } else {
                    out.print("error");
                    System.out.println("error");
                }
            } catch (Exception e) {
                System.out.println("s_gestionUsuarios() " + e.getMessage());
            } finally {
                conora.closeConexionOracle();
            }
        }
        if(request.getParameter("parametro_boton").equalsIgnoreCase("botonCerrarSesion")){
            System.out.println("cerrarSesion");
            response.setContentType("text/html");
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
            request.getSession().removeAttribute("logueado");
            HttpSession actual=request.getSession();
            actual.invalidate();
            out.print("index.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
