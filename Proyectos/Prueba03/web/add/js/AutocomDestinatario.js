var peticion_http;
var READY_STATE_COMPLETE = 4;

$(function() {
    peticion_http = inicializa_objeto_ajax();
    var datos = "parametro_boton=botonBuscarTodosDestinatarios&parametro_rut=" + document.getElementById("buscarutdestinatario").value;
    console.log("datos" + datos);
    peticion_http.onreadystatechange = procesaRespuestaTodosDestinatarios;
    peticion_http.open("POST", "/Prueba03/S_GestionDestinatario", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);
});

function procesaRespuestaTodosDestinatarios() {
    console.log("en  procesaRespuestatodosDestinatarios()");
    console.log("peticion_http.readyState: " + peticion_http.readyState);
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) {
        console.log("readyState: " + peticion_http.readyState);
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta = peticion_http.responseText;
                datosArt = respuesta;
                nuevo = [];
                if (respuesta !== "error") {
                    datosArt = datosArt.split("|");

                    for (i = 0; i < datosArt.length; i++) {
                        nuevo = datosArt[i].split(",");
                        datosArt[i] = {rut: nuevo[0], name: nuevo[1]};
                    }
                    console.log(datosArt);
                    $('#buscarutdestinatario').awesomecomplete({
                        noResultsMessage: '<p>no se encontraron resultados</p>',
                        staticData: datosArt,
                        valueFunction: function(dataItem) {
                            return dataItem['rut'];
                        }
                    });
                    
                }
            }
        }
    }
    return;
}