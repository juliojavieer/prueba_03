function validaEmail(id) {
    valor = document.getElementById(id).value;
    re = /^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$/;
    if (valor.length > 0) {
        if (!re.exec(valor))
        {
            document.getElementById(id).style['background'] = "#url(add/img/invalid.png) no-repeat 98% center";
            document.getElementById(id).style['boxShadow'] = "0 0 5px #d45252";
            document.getElementById(id).style['borderColor'] = "#b03535";
            return false;
        }
        else
        {
            document.getElementById(id).style['background'] = "#url(add/img/valid.png) no-repeat 98% center";
            document.getElementById(id).style['boxShadow'] = "0 0 5px #5cd053";
            document.getElementById(id).style['borderColor'] = "#28921f";
            return true;
        }
    }
    else {
        console.log("no ingreso correo");
        return false;
    }
}
