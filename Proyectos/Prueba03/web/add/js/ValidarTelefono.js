function validarTelefono(id) {
    var telefono = document.getElementById(id).value;
    telefono.trim();
    if (telefono.length > 3) {
        if (!isNaN(telefono)) {
            document.getElementById(id).style['background'] = "#url(add/img/valid.png) no-repeat 98% center";
            document.getElementById(id).style['boxShadow'] = "0 0 5px #5cd053";
            document.getElementById(id).style['borderColor'] = "#28921f";
            return true;
        }
        else {
            document.getElementById(id).style['background'] = "#url(add/img/invalid.png) no-repeat 98% center";
            document.getElementById(id).style['boxShadow'] = "0 0 5px #d45252";
            document.getElementById(id).style['borderColor'] = "#b03535";
            return false;
        }
    }
    else {
        document.getElementById(id).style['background'] = "#url(add/img/invalid.png) no-repeat 98% center";
        document.getElementById(id).style['boxShadow'] = "0 0 5px #d45252";
        document.getElementById(id).style['borderColor'] = "#b03535";
        return false;
    }
}

