var READY_STATE_COMPLETE = 4;
var peticion_http;


function guardarBodega() {
    peticion_http = inicializa_objeto_ajax();
    var id_bod = document.getElementById("id_bodega").value;
    var nom_bod = document.getElementById("nombre_bodega").value;
    if (true) {
        console.log("datos correctos");

        console.log("id_bodega: ");

        var jsonBod = encodeURIComponent(
                '{"id_bodega":"' + id_bod
                + '","nombre_bodega":"' + nom_bod + '"}');
        console.log("Ingresa Bodega");


        var datos = "parametro_boton=boton_guarda_bodega&jsonBod=" + jsonBod;

        peticion_http.onreadystatechange = procesaGuardarBodega;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()

        peticion_http.open("POST", "/Prueba03/S_GestionBodega", true);
        peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
        peticion_http.setRequestHeader("Content-length", datos.length);
        peticion_http.setRequestHeader("Connection", "close");
        peticion_http.send(datos);

        return;
    } else {
        alert("datos incorrectos");
    }


}

function procesaGuardarBodega() {
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        document.getElementById("div_resp_bod").innerHTML = "Enviando...";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                document.getElementById("div_resp_bod").innerHTML = peticion_http.responseText;
            }
        } else {
            document.getElementById("div_resp_bod").innerHTML = "proceso erroneo";
        }
    }
    return;
}