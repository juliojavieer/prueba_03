var READY_STATE_COMPLETE = 4;
var peticion_http;

function guardarCamion() {

    var pat = document.getElementById("patente").value;
    var marc = document.getElementById("marca").value;
    var model = document.getElementById("modelo").value;
    var years = document.getElementById("year").value;
    var rutprop = document.getElementById("rut_propi").value;
    if (pat.length > 0 && marc.length >0 && model.length > 0 && years.length > 1 ) {
        console.log("datos correctos");
        console.log("Ingresar Camion");
        console.log(pat + "|" + rutprop);
        var jsonCamion = encodeURIComponent(
                '{"patente":"' + pat
                + '","marca":"' + marc
                + '","modelo":"' + model
                + '","year":"' + years
                + '","rut_propietario":"' + rutprop
                + '"}');


        guardaCamion(jsonCamion);



        return true;
    }
    else {
        console.log("datos incorrectos");
        document.getElementById("div_resp_Camion").innerHTML = "Datos incompletos o incorrectos";
    }

}
function guardaCamion(jsonCamion) {
    peticion_http = inicializa_objeto_ajax();

    var datos = "parametro_boton=boton_guarda_camion&j&jsonCamion=" + jsonCamion;

    peticion_http.onreadystatechange = procesaGuardarCamion;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()

    peticion_http.open("POST", "/Prueba03/S_GestionCamion", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaGuardarCamion() {
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        document.getElementById("div_resp_Camion").innerHTML = "Enviando...";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                document.getElementById("div_resp_Camion").innerHTML = peticion_http.responseText;
            }
        } else {
            document.getElementById("div_resp_Camion").innerHTML = "proceso erroneo";
        }
    }
    return;
}