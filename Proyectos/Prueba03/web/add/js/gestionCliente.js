var READY_STATE_COMPLETE = 4;
var peticion_http;


function guardarCliente() {
    peticion_http = inicializa_objeto_ajax();

    var rut = "rut_pers";
    var cla = "clasif_pers";
    var des = "descuento_pers";
    var rso = "nombre_pers";
    var dir = "direccion_pers";
    var ema = "email_pers";
    var tel = "telefono_pers";
    var ciu = "ciudad_pers";
    var pasw = "pass_cli";
    if (true) {
        console.log("datos correctos");
        var rut1 = document.getElementById(rut).value;
        var des1 = document.getElementById(des).value;
        var cla1 = document.getElementById(cla).value;
        var rso1 = document.getElementById(rso).value;
        var dir1 = document.getElementById(dir).value;
        var ema1 = document.getElementById(ema).value;
        var tel1 = document.getElementById(tel).value;
        var ciu1 = document.getElementById(ciu).value;
        var pass = document.getElementById(pasw).value;
        console.log("rut_cliente: ");

        var jsonPro = encodeURIComponent(
                '{"rut_cliente":"' + rut1
                + '","descuento":"' + des1
                + '","clasificacion":"' + cla1
                + '","rsocial":"' + rso1
                + '","direccion":"' + dir1
                + '","ciudad":"' + ciu1
                + '","telefono":"' + tel1
                + '","email":"' + ema1 + '"}');
        console.log("Ingresa Cliente");
        
        var jsonUsr =encodeURIComponent(
                '{"usuario":"' + rut1
                + '","pass":"' + pass + '"}');
               
        console.log("Ingresa Usuario");
        var datos = "parametro_boton=boton_guarda_cliente&jsonPro=" + jsonPro+"&jsonUsr="+jsonUsr;

        peticion_http.onreadystatechange = procesaGuardarCliente;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()

        peticion_http.open("POST", "/Prueba03/S_GestionCliente", true);
        peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
        peticion_http.setRequestHeader("Content-length", datos.length);
        peticion_http.setRequestHeader("Connection", "close");
        peticion_http.send(datos);

        return;
    } else {
        alert("datos incorrectos");
    }


}

function procesaGuardarCliente() {
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        document.getElementById("div_resp_pers").innerHTML = "Enviando...";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                document.getElementById("div_resp_pers").innerHTML = peticion_http.responseText;
            }
        } else {
            document.getElementById("div_resp_pers").innerHTML = "proceso erroneo";
        }
    }
    return;
}