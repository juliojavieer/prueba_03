var READY_STATE_COMPLETE = 4;
var peticion_http;

function verSeleDest() {
    jQuery.fn.getCheckboxValues = function() {
        var values = [];
        var i = 0;
        this.each(function() {
            values[i++] = $(this).val();
            eliminarDestinatarios($(this).val());
        });
        console.log(values);
        return values;
    };
    var arr = $("input:checked").getCheckboxValues();
}



function eliminarDestinatarios(id) {
    console.log(id);
    peticion_http = inicializa_objeto_ajax();
    var rut_destinatario = id;
    console.log(rut_destinatario);
    var datos = "parametro_boton=botonEliminarDestinatarios&parametro_rut=" + rut_destinatario;
    console.log("datos: " + datos);

    console.log("antes onready");
    peticion_http.onreadystatechange = procesaEliminarDestinatario;
    console.log("despues onready");

    peticion_http.open("POST", "/Prueba03/S_GestionDestinatario", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;

}
function procesaEliminarDestinatario() {
    console.log("en  procesaEliminarDestinatario()");
    console.log("peticion_http.readyState: " + peticion_http.readyState);
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        console.log("readyState: " + peticion_http.readyState);
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta = peticion_http.responseText;
                if (respuesta !== "error") {
                    console.log("resp " + respuesta);
                    var datosArt = eval("(" + respuesta + ")");
                }
                else {
                }
            }
        } else {
        }
    }
    return;
}


function buscarDestinatario(id) {
    console.log(id);
    peticion_http = inicializa_objeto_ajax();
    var rut_proveedor = document.getElementById('buscarutdestinatario').value;
    console.log(rut_proveedor);
    var datos = "parametro_boton=botonBuscarDestinatario&parametro_rut=" + rut_proveedor;
    console.log("datos: " + datos);

    console.log("antes onready");
    peticion_http.onreadystatechange = procesaBuscarDestinatario;
    console.log("despues onready");

    peticion_http.open("POST", "/Prueba03/S_GestionDestinatario", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaBuscarDestinatario() {
    console.log("en  procesaRespuestaProveedor()");
    console.log("peticion_http.readyState: " + peticion_http.readyState);
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        console.log("readyState: " + peticion_http.readyState);
        document.getElementById("tabla_muestra_destinatario").innerHTML = "Enviando...";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta = peticion_http.responseText;
                if (respuesta !== "error") {
                    console.log("resp "+respuesta);
                    var datosArt = eval("(" + respuesta + ")");
                    var table = "\n\
                <thead><tr><th>Rut Dest</th><th>Nombre</th><th>Direccion</th><th>Ciudad</th><th>Telefono</th><th>Mail</th><th><button class='buttonconfirm' onclick='javascript:openDialog();'>Eliminar</button></tr></thead><tbody>\n\
                <tr><td>" + datosArt.rut+ "</td><td>" + datosArt.rsocial + "</td><td>" + datosArt.direccion + "</td>\n\
                <td>" + datosArt.ciudad + "</td><td>" + datosArt.telefono + "</td><td>" + datosArt.email + "</td><td><input type='checkbox' value='" + datosArt.rut + "' /></td></tr></tbody>";
                }
                else{
                    table = "No se encontró destinatario";
                }
                document.getElementById("tabla_muestra_destinatario").innerHTML = table;
            }
        } else {
            document.getElementById("tabla_muestra_destinatario").innerHTML = "proceso erroneo";
        }
    }
    return;
}



function buscarTodosDest() {
    peticion_http = inicializa_objeto_ajax();
    var datos = "parametro_boton=boton_buscar_todos_destinatarios";
    console.log("datos: " + datos);

    console.log("antes onready");
    peticion_http.onreadystatechange = procesaBuscarTodosDest;
    console.log("despues onready");

    peticion_http.open("POST", "/Prueba03/S_GestionDestinatario", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaBuscarTodosDest() {
    console.log("en  procesaRespuestaTodosProveedor()");
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { 
        console.log("readyState: " + peticion_http.readyState);
        document.getElementById("tabla_muestra_destinatario").innerHTML = "Enviando...";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta = peticion_http.responseText;
                if (respuesta !== "error") {
                    console.log("resp " + respuesta);
                    var datosArt = respuesta.split("|");
                    var table = "<thead><tr><th>Rut Trans</th><th>Nombre</th><th>Direccion</th><th>Ciudad</th><th>Telefono</th><th>Mail</th><th><button class='buttonconfirm' onclick='javascript:openDialog();'>Eliminar</button></th></tr></thead><tbody>";
                    document.getElementById("tabla_muestra_destinatario").innerHTML = table;
                    for (i = 0; i < datosArt.length; i++) {
                        var ele = datosArt[i].split(",");
                        table = "<tr><td>" + ele[0] + "</td><td>" + ele[1] + "</td><td>" + ele[2] + "</td>\n\
                <td>" + ele[3] + "</td><td>" + ele[4] + "</td><td>" + ele[5] + "</td><td><input type='checkbox' value='" + ele[0] + "' /></td></tr>";
                        document.getElementById("tabla_muestra_destinatario").innerHTML += table;
                    }
                    document.getElementById("tabla_muestra_destinatario").innerHTML += "</tbody>";
                }
                else {
                    document.getElementById("tabla_muestra_destinatario").innerHTML = "No se encontraron destinatarios";
                }
            }
        } else {
            document.getElementById("tabla_muestra_destinatario").innerHTML = "proceso erroneo";
        }
    }
    return;
}



function guardarDestinatario(jsonPro) {
    peticion_http = inicializa_objeto_ajax();

    var datos = "parametro_boton=boton_guarda_destinatario&jsonPro=" +jsonPro;
 
    peticion_http.onreadystatechange = procesaGuardarDestinatario;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()

    peticion_http.open("POST", "/Prueba03/S_GestionDestinatario", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');    
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaGuardarDestinatario() {
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        document.getElementById("div_resp_pers").innerHTML = "Enviando...";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                document.getElementById("div_resp_pers").innerHTML = peticion_http.responseText;
            }
        } else {
            document.getElementById("div_resp_pers").innerHTML = "proceso erroneo";
        }
    }
    return;
}