var READY_STATE_COMPLETE = 4;
var peticion_http;

function inicializa_objeto_ajax() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else
    if (window.ActiveXObject) {
        try {
            xmlHttpReq = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (exp1) {
            try {
                xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (exp2) {
                xmlHttpReq = false;
            }
        }
    }
}

function cerrarSesion(){
    peticion_http = inicializa_objeto_ajax();
    var datos = "parametro_boton=botonCerrarSesion";
    console.log("antes onready");
    peticion_http.onreadystatechange = procesaRespuestaUsuarios;
    console.log("despues onready");

    peticion_http.open("POST", "/Prueba03/S_GestionUsuarios", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function buscarUsuario(id) {
    console.log(id);
    peticion_http = inicializa_objeto_ajax();
    var usuario = document.getElementById('rut_usuario').value;
    var pass    = document.getElementById('clave_usuario').value;
    console.log("usuario : "+usuario+" ,clave: "+pass);
    
    var jsonPer = encodeURIComponent(
            '{"usuario":"' +usuario 
            +'","pass":"' +pass+'"}');
    
    var datos = "parametro_boton=botonBuscarUsuarios&jsonUsr=" +jsonPer;
    console.log("datos: " + datos);

    console.log("antes onready");
    peticion_http.onreadystatechange = procesaRespuestaUsuarios;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()
    console.log("despues onready");

    peticion_http.open("POST", "/Prueba03/S_GestionUsuarios", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaRespuestaUsuarios() {
    console.log("en  procesaRespuestaUsuarios()");
    console.log("peticion_http.readyState: " + peticion_http.readyState);
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        console.log("readyState: " + peticion_http.readyState);
        document.getElementById("div_respuesta").innerHTML = "<img src='add/img/loading1.gif'>";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta = peticion_http.responseText;
                if (respuesta !== "error") {
                    console.log("entro");
                    var tabla = "<img src='add/img/loading1.gif'>";
                    location.href=respuesta;    
                }
                else{
                    tabla = "Usuario inválido";
                }
                document.getElementById("div_respuesta").innerHTML = tabla;
            }
        } else {
            document.getElementById("div_respuesta").innerHTML = "proceso erroneo";
        }
    }
    return;
}