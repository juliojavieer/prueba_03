var peticion_http;
var READY_STATE_COMPLETE = 4;

function inicializa_objeto_ajax() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else
    if (window.ActiveXObject) {
        try {
            xmlHttpReq = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (exp1) {
            try {
                xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (exp2) {
                xmlHttpReq = false;
            }
        }
    }
}


$(function() {
    peticion_http = inicializa_objeto_ajax();
    var datos = "parametro_boton=botonBuscarTodosProveedores&parametro_rut=" + document.getElementById("friend_id").value;
    console.log("datos" + datos);
    peticion_http.onreadystatechange = procesaRespuestaTodosProveedores;
    peticion_http.open("POST", "/Prueba03/S_GestionProveedores", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);
});

function procesaRespuestaTodosProveedores() {
    console.log("en  procesaRespuestatodosproveedores()");
    console.log("peticion_http.readyState: " + peticion_http.readyState);
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) {
        console.log("readyState: " + peticion_http.readyState);
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta = peticion_http.responseText;
                datosArt = respuesta;
                nuevo = [];
                if (respuesta !== "error") {
                    datosArt = datosArt.split("|");

                    for (i = 0; i < datosArt.length; i++) {
                        nuevo = datosArt[i].split(",");
                        datosArt[i] = {rut: nuevo[0], name: nuevo[1]};
                    }
                    console.log(datosArt);
                    
                    $('#friend_id').awesomecomplete({
                        noResultsMessage: '<p>no se encontraron resultados</p>',
                        staticData: datosArt,
                        valueFunction: function(dataItem) {
                            return dataItem['rut'];
                        }
                    });
                    
                }
            }
        }
    }
    return;
}