<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="utilidades.Oraconex"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    try {
        HttpSession actual = request.getSession(true);
        String id = actual.getId();
        String nombre = (String) actual.getAttribute("logueado");
        System.out.println("usuario" + nombre);
        if (actual.isNew()) {
            response.sendRedirect("index.jsp");
            return;
        }
        if (actual == null) {
            response.sendRedirect("index.jsp");
        } else {
            if (actual.getAttribute("logueado") == null) {
                response.sendRedirect("index.jsp");
            }
        }
        out.print("<input type='hidden' id='rut_cliente' value='" + nombre + "'>");
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="add/css/busca_proveedor.css"/>
        <link rel="stylesheet" type="text/css" href="add/css/awesomecomplete.css"/>
        <script type="text/javascript" src="add/js/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="add/js/jquery.awesomecomplete.js"></script>
        <script type="text/javascript" src="add/js/AutocomTransportista.js"></script>  
        <script type="text/javascript" src="add/js/gestionTansportistas.js"></script> 
        <link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic' rel='stylesheet' type='text/css'>	
        <link rel="stylesheet" href="add/css/estiloconfirmar.css">
        <link rel="stylesheet" href="add/css/avgrund.css">
        <script type="text/javascript" src="add/js/avgrund.js"></script>
        <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
        <script>
            function openDialog() {
                Avgrund.show("#elim-trans");
            }
            function closeDialog() {
                Avgrund.hide();
            }
        </script>
    </head>
    <body>

        <aside id="elim-trans" class="avgrund-popup">
            <center>
            <h2>Eliminar transportista</h2>
            <p>
                Al eliminar estos datos, no los podrás recuperar nuevamente.
            </p>
            <p>
                ¿Estás seguro que deseas eliminar?
            </p>
            <button class="buttonconfirm" onclick="verSeleTrans();javascript:closeDialog();">Eliminar</button>
            <button class="buttonconfirm" onclick="javascript:closeDialog();">Cancelar</button>
            </center>
        </aside>



        <h1 class="titulo">Buscar Transportista</h1>
    <center>
        <br><br>
        <form id="form_busca" style="width: 428px; ">
            <input type="text" onblur="Rut(id);" id="buscaruttransportista" class="bbusca" placeholder="Ingrese rut..." required />
            <button id="boton_buscar_pro" class="bbusca" type="button" onclick="buscarTransportista(id);">Buscar</button>
            <button id="boton_buscar_todos_pro" class="bbusca" type="button" onclick="buscarTodosTrans();"  >Buscar todos</button>   
            <a href='Cliente.jsp'><img width="50" height="45" src="add/img/tick.png"></a>
        </form>

        <br><br><br>
        <div class="datagrid">
            <table id="tabla_muestra_transportista" >
            </table> 
        </div>
        
        <br><br><br><br><br><br><br><br><br><br><br><br>
        
    </center>



</body>
</html>
