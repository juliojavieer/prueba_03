<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    try {
        HttpSession actual = request.getSession(true);
        String id = actual.getId();
        String nombre = (String) actual.getAttribute("logueado");
        System.out.println("usuario"+nombre);
        if (actual.isNew()) {
            response.sendRedirect("InicioSesion.jsp");
            return;
        }
         if (actual == null) {
         response.sendRedirect("InicioSesion.jsp");
         } else {
         if (actual.getAttribute("logueado") == null) {
         response.sendRedirect("index.jsp");
         }
         }
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="add/js/gestionSesiones.js"></script>
        <title>JSP Page</title>
    </head>
    <body>
        <input type="button" value="Cerrar Sesión" onclick="cerrarSesion(id);">
    </body>
</html>
