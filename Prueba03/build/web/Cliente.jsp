<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="utilidades.Oraconex"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    try {
        response.setHeader("Content-Type", "text/html; charset=windows-1252");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "Mon, 04 Jan 2013 00:00:01 GTM");
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Cache-Control", "must-revalidate");
        response.setHeader("Cache-Control", "no-cache");
        HttpSession actual = request.getSession(true);
        String id = actual.getId();
        String nombre = (String) actual.getAttribute("logueado");
        System.out.println("usuario" + nombre);
        if (actual.isNew()) {
            response.sendRedirect("index.jsp");
            return;
        }
        if (actual == null) {
            response.sendRedirect("index.jsp");
        } else {
            if (actual.getAttribute("logueado") == null) {
                response.sendRedirect("index.jsp");
            }
        }
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        try {
            Statement st = oraconex.createStatement();
            String sql = "select rsocial from persona where rut like '" + nombre + "'";
            ResultSet rs = st.executeQuery(sql);
            rs.next();
            String nom = rs.getString(1);
            System.out.println(nom);
            out.print("<div class='codrops-top'><a href=''><strong>Bienvenido : " + nom + "</strong></a>");
            out.print("<span class='right'><a onclick='cerrarSesion()';><strong>Cerrar Sesión</strong></a></span> <div class='clr'></div></div>");
        } catch (Exception e) {
        } finally {
            conora.closeConexionOracle();
        }
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
%>
<!DOCTYPE html>
<html>
    <head>
    <a onclick=""></a>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="add/css/demo.css" />
    <link rel="stylesheet" type="text/css" href="add/css/style.css" />
    <script src="add/js/gestionSesiones.js"></script>
    <link rel="stylesheet" href="add/css/colorbox.css" />
    <script type="text/javascript" src="add/js/jquery.min.js"></script>
    <script src="add/js/jquery.colorbox.js"></script>
    <script src="add/js/formulario.js"></script>
    <script src="add/js/VerificaCampos.js"></script>
    <script src="add/js/ValidaRut.js"></script>
    <script src="add/js/ValidaCorreo.js"></script>
    <script src="add/js/ValidarTelefono.js"></script>
    <script src="add/js/ValidaRazonSocial.js"></script>
    <link rel="stylesheet" href="add/css/stylemenu.css" type="text/css" media="screen"/>
    <script type="text/javascript" src="add/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="add/js/jqueryimagenes.js"></script>
    <title>Cliente</title>
    <!--
    <script>
        $(document).ready(function() {
            $("#perfilCliente").click(function(evento) {
                evento.preventDefault();
                $("#principal").load("PerfilCliente.jsp");
                console.log("listo");
            });
        });
    </script>
    -->
</head>
<body>

    <!-- La barra de menu -->

    <div class="content">
        <h1 class="title">Deslizate</h1>
        <center>
            <ul id="sdt_menu" class="sdt_menu">
                <li>
                    <a href="#">
                        <img src="add/img/2.jpg" alt=""/>
                        <span class="sdt_active"></span>
                        <span class="sdt_wrap">
                            <span class="sdt_link">Registrar</span>
                            <span class="sdt_descr">Proveedores, etc..</span>
                        </span>
                    </a>
                    <div class="sdt_box">
                        <a href="#inline_content" class='inline' onclick="bloquearInputs(contact_form);">Proveedor<br>Destinatario <br> Transportista</a>
                        <a href="#">Lista entrada</a>
                        <a href="#">Lista salida</a>
                    </div>
                </li>
                <li>
                    <a href="#">
                        <img src="add/img/1.jpg" alt=""/>
                        <span class="sdt_active"></span>
                        <span class="sdt_wrap">
                            <span class="sdt_link">Ver</span>
                            <span class="sdt_descr">mis registros</span>
                        </span>
                    </a>
                    <div class="sdt_box">
                        <a href="#">Mis ubicaciones</a>
                        <a href="#">Mis proveedores</a>
                        <a href="#">Mis listas</a>
                    </div>
                </li>
                <li>
                    <a href="#">
                        <img src="add/img/3.jpg" alt=""/>
                        <span class="sdt_active"></span>
                        <span class="sdt_wrap">
                            <span class="sdt_link">Eliminar</span>
                            <span class="sdt_descr">Mis registros</span>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="add/img/4.jpg" alt=""/>
                        <span class="sdt_active"></span>
                        <span class="sdt_wrap">
                            <span class="sdt_link">Mi perfil</span>
                            <span class="sdt_descr"> avatar, contraseña</span>
                        </span>
                    </a>
                    <div class="sdt_box">
                        <a href="#" id="perfilCliente">Ver perfil</a>
                    </div>
                </li>
                <li>
                    <a href="#">
                        <img src="add/img/5.jpg" alt=""/>
                        <span class="sdt_active"></span>
                        <span class="sdt_wrap">
                            <span class="sdt_link">Blog</span>
                            <span class="sdt_descr">I write about design</span>
                        </span>
                    </a>
                </li>
            </ul>
        </center>
    </div>
    <br><br><br><br><br><br>
    <div id="principal" style='padding:10px;'>
    </div>


    <div style='display:none;'>
        <div id='inline_content' style='padding:10px;'>
            <p>
                <strong>
                    <form class="contact_form" action="#" method="post" name="contact_form" autocomplete="yes">
                        <center>
                            <h1 class="titulo">Ingresar Datos</h1>
                            <ul>
                                <li>
                                    <label for="name">Seleccionar persona</label>
                                    <div class="seleccion">
                                        <select id="select_id">
                                            <option selected="selected" onclick="bloquearInputs(contact_form);">Seleccione...</option>>
                                            <option onclick="desbloquearInputs(contact_form);">Ingresar Proveedor</option>
                                            <option onclick="desbloquearInputs(contact_form);">Ingresar Destinatario</option>
                                            <option onclick="desbloquearInputs(contact_form);">Ingresar Transportista</option>
                                        </select>
                                    </div>
                                </li>
                                <li>
                                    <label for="name">Rut:</label>
                                    <input id="rut_proveedor" onblur="Rut(id);" type="text" placeholder="12.988.829-2" required 
                                           style="padding-left: 30px; background: url('add/img/rut.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Tu Rut</span>
                                </li>
                                <li>
                                    <label for="name">Razon Social:</label>
                                    <input onblur="comprobar_nombre(id);" id="nombre_proveedor" type="text" name="email" placeholder="Julio javier" required 
                                           style="padding-left: 30px; background: url('add/img/nom.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Nombre</span>
                                </li>
                                <li>
                                    <label for="name">Dirección:</label>
                                    <input id="direccion_proveedor" type="text" name="email" placeholder="Los cantares #1212" required 
                                           style="padding-left: 30px; background: url('add/img/dir.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Nombre calle #direccion</span>
                                </li>
                                <li>
                                    <label for="name">Ciudad:</label>
                                    <input id="ciudad_proveedor" type="text" name="email" placeholder="Temuco" required 
                                           style="padding-left: 30px; background: url('add/img/ciu.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Ejemplo Santiago</span>
                                </li>
                                <li>
                                    <label for="name">Teléfono:</label>
                                    <input onblur="validarTelefono(id);" id="telefono_proveedor" type="text" placeholder="9897211" required
                                           style="padding-left: 30px; background: url('add/img/tel.png') no-repeat 2% center;" />
                                    <span class="form_hint">Ejemplo 1243121</span>
                                </li>
                                <li>
                                    <label for="email">Email:</label>
                                    <input style="padding-left: 30px; background: url('add/img/mai.png') no-repeat 2% center;" id="email_proveedor" onblur="validaEmail(id);" type="email" name="email" placeholder="tuemail@tuhost.com" required />
                                    <span class="form_hint">Ejemplo nombre@host.com</span>
                                </li>
                                <h1 class="titulo"></h1>
                                    
                            </ul>
                        </center>
                        
                    </form>
                    
                </strong>
            </p>
            <center>
            <label><button class="submit" type="reset">Limpiar</button></label>
            <label><button class="submit" type="button" onclick="guardarCampos();">Registrar</button></label>
            </center>
        </div>
    </div>
</body>
</html>
