package Entidades;

public class Usuarios {

    private String usuario;
    private String pass;

    public Usuarios() {
    }

    public Usuarios(String usuario, String pass) {
        this.usuario = usuario;
        this.pass = pass;
    }

    public String getUsuario() {
        return usuario;
    }
    public String getPass() {
        return pass;
    }    
}
