<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  -->
    <title>Ingreso de usuarios</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <meta name="description" content="Login and Registration Form with HTML5 and CSS3" />
    <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class" />
    <meta name="author" content="Codrops" />
    <link rel="shortcut icon" href="../favicon.ico"> 
    <link rel="stylesheet" type="text/css" href="add/css/demo.css" />
    <link rel="stylesheet" type="text/css" href="add/css/style.css" />
    <link rel="stylesheet" type="text/css" href="add/css/animate-custom.css" />
    <script src="add/js/gestionSesiones.js"></script>
    <script src="add/js/ValidaRut.js"></script>
    <script src="add/js/jquery.min.js"></script>
</head>
<body>
    <div class="container">
        <!-- Codrops top bar -->
        <div class="codrops-top">
            <a href="">
                <strong>Inicia Sesión en nuestra página </strong>
            </a>
            <div class="clr"></div>
        </div>
         <header>
             <h1><span>Sistema</span> gestion de <span>Bodegas</span></h1>
         </header>
        
        <section>				
            <br><br>
            <div id="container_demo" >
                <a class="hiddenanchor" id="toregister"></a>
                <a class="hiddenanchor" id="tologin"></a>
                <div id="wrapper">
                    <div id="login" class="animate form">
                        <form autocomplete="on"> 
                            <h1>Iniciar Sesion</h1> 
                            <p> 
                                <label for="username" class="uname" data-icon="u" > Tu nombre de usuario </label>
                                <input id="rut_usuario" onblur="Rut(id);" required="required" type="text" placeholder="Ingresa tu rut ej: 10.988.921-1"/>
                            </p>
                            <p> 
                                <label for="password" class="youpasswd" data-icon="p"> Your password </label>
                                <input id="clave_usuario" required="required" type="password" placeholder="Ingresa tu password" /> 
                            </p>
                            <p class="keeplogin"> 
                                <input type="checkbox" name="loginkeeping" id="loginkeeping" value="loginkeeping" /> 
                                <label for="loginkeeping">Recordarme</label>
                            </p>
                            <p class="login button">
                                <input type="button" id="boton" value="Entrar" onclick="buscarUsuario(id);"/> 
                            </p>
                            <p class="change_link" id="div_respuesta">
                                Contáctate con nosotros para tener acceso.
                            </p>
                        </form>
                    </div>
                </div>
            </div>  
        </section>
    </div>
</body>
</html>