
package Entidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utilidades.Oraconex;

public class Camion {

    private String patente;
    private String marca;
    private String modelo;
    private int year;
    private String rut_propietario;
    private String todosCamiones;

    public Camion() {
    }

    public Camion(String patente, String marca, String modelo, int year, String rut_propietario) {
        this.patente = patente;
        this.marca = marca;
        this.modelo = modelo;
        this.year = year;
        this.rut_propietario = rut_propietario;
    }

    public int insertarCamion() {

        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlInsert = "insert into camion  values(?,?,?,?,?)";
        try {
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setString(1, this.patente);
            psipc.setString(2, this.marca);
            psipc.setString(3, this.modelo);
            psipc.setInt(4, this.year);
            psipc.setString(5, this.rut_propietario);
            filasAfectadas = psipc.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Camion.insertarCamion(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;

        }

    }
     public Camion buscarCamion(){
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from Camion where patente like ?";
        Camion camRespuesta = null;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);
            psbc.setString(1, this.patente);
            ResultSet rs = psbc.executeQuery();

            rs.next();
            String pate = rs.getString(1);
            System.out.println("patente camion : " + pate);
            String marc = rs.getString(2);
            System.out.println("marca camion : " + marc);
            String mode = rs.getString(3);
            System.out.println("modelo camion : " + mode);
            int yea = rs.getInt(4);
            System.out.println("año camion : " + yea);
            String rut_pro = rs.getString(5);
            System.out.println("rut propietario camion : " + rut_pro);
           
            camRespuesta = new Camion(pate, marc, mode, yea, rut_pro);

        } catch (SQLException ex) {
            System.out.println("Cliente.buscarCliente(): " + ex.getMessage());
            return null;
        } finally {
            conora.closeConexionOracle();
            return camRespuesta;
        }
    }
    
    public String getTodosCamiones() {

        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select patente from Camion where patente like '%" + this.patente + "%' ";
        int cont = 0;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);

            ResultSet rs = psbc.executeQuery();
            while (rs.next()) {
                cont++;
            }
            System.out.println("contador" + cont);
            this.todosCamiones = "";
            rs = psbc.executeQuery();
            int i = 0;
            while (rs.next()) {
                this.todosCamiones += rs.getString(1)+"|";
                System.out.println("articulos : " + this.todosCamiones);
                i++;
            }
        } catch (Exception e) {return null;
        } finally {
            conora.closeConexionOracle();
            return this.todosCamiones.substring(0,this.todosCamiones.length()-1);
        }


    }
    @Override
    public String toString() {
        return patente + "|" + marca + "|" + modelo + "|" + year + "|" + rut_propietario;
    }

}
