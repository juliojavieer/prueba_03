package Entidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utilidades.Oraconex;

public class Clasificacion {

    private int id_clasificacion;
    private String descripcion;
    private String todaClasificaciones;

    public Clasificacion() {
    }

    public Clasificacion(int id_clasificacion, String descripcion) {
        this.id_clasificacion = id_clasificacion;
        this.descripcion = descripcion;
    }

    public int insertarClasificacion() {

        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlInsert = "insert into Clasificacion  values(?,?)";
        try {
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setInt(1, this.id_clasificacion);
            psipc.setString(2, this.descripcion);
            filasAfectadas = psipc.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Clasificacion.insertarClasificacion(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;

        }

    }

    public Clasificacion buscarClasificacion() {
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from clasificacion where id_clasificacion like ?";
        Clasificacion claRespuesta = null;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);
            psbc.setInt(1, this.id_clasificacion);
            ResultSet rs = psbc.executeQuery();

            rs.next();
            int id_clasifica = rs.getInt(1);
            System.out.println("id clasificacion: " + id_clasifica);
            String descri = rs.getString(2);
            System.out.println("descripcion : " + descri);


            claRespuesta = new Clasificacion(id_clasifica, descri);

        } catch (SQLException ex) {
            System.out.println("Cliente.buscarCliente(): " + ex.getMessage());
            return null;
        } finally {
            conora.closeConexionOracle();
            return claRespuesta;
        }
    }

    public String getTodoClasificacion() {

        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select id_clasificacion from clasificacion where id_clasificacion=" + this.id_clasificacion +";";
        int cont = 0;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);

            ResultSet rs = psbc.executeQuery();
            while (rs.next()) {
                cont++;
            }
            System.out.println("contador" + cont);
            this.todaClasificaciones = "";
            rs = psbc.executeQuery();
            int i = 0;
            while (rs.next()) {
                this.todaClasificaciones += rs.getInt(1) + "|";
                System.out.println("clasificacion : " + this.todaClasificaciones);
                i++;
            }
        } catch (Exception e) {
            return null;
        } finally {
            conora.closeConexionOracle();
            return this.todaClasificaciones.substring(0, this.todaClasificaciones.length() - 1);
        }


    }

    @Override
    public String toString() {
        return id_clasificacion + "|" + descripcion ;
    }
}
