package Entidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utilidades.Oraconex;

public class Cliente {

    private String rut_cliente;
    private int descuento;
    private String clasificacion;
    private String rsocial;
    private String direccion;
    private String ciudad;
    private String telefono;
    private String email;
    private String todosClientes;

    public Cliente() {
    }

    public Cliente(String rut) {
        this.rut_cliente = rut;
    }
    
    public Cliente(String rut_cliente, int descuento, String clasificacion){
        this.rut_cliente   = rut_cliente;
        this.descuento     = descuento;
        this.clasificacion = clasificacion;
    }

    public Cliente(String rut_cliente, int descuento, String clasificacion, String rsocial, String direccion, String ciudad, String telefono, String email) {

        this.rut_cliente = rut_cliente;
        this.descuento = descuento;
        this.clasificacion = clasificacion;
        this.rsocial = rsocial;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.telefono = telefono;
        this.email = email;
    }

    public Cliente buscarCliente() {
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select *from persona inner join cliente using(rut) where rut like ? ";
        Cliente clie = null;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);
            psbc.setString(1, this.rut_cliente);
            ResultSet rs = psbc.executeQuery();

            rs.next();
            String rut = rs.getString(1);
            System.out.println("rut : " + rut);
            String rsoc = rs.getString(2);
            System.out.println("razon social : " + rsoc);
            String direc = rs.getString(3);
            System.out.println("dieccion : " + direc);
            String ciu = rs.getString(4);
            System.out.println("ciudad : " + ciu);
            String telef = rs.getString(5);
            System.out.println("teleofono : " + telef);
            String ema = rs.getString(6);
            System.out.println("email : " + ema);
            int desc = rs.getInt(7);
            System.out.println("descuento : " + desc);
            String clasi = rs.getString(8);
            System.out.println("calificacion : " + clasi);
            clie = new Cliente(rut, desc, clasi, rsoc, direc, ciu, telef, ema);

        } catch (SQLException ex) {
            System.out.println("Cliente.buscarCliente(): " + ex.getMessage());
            return null;
        } finally {
            conora.closeConexionOracle();
            return clie;
        }
    }

    public int insertarCliente() {
        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();

        String sqlInsert = "insert into cliente  values(?,?,?)";
        try {
            Persona pers = new Persona(this.rut_cliente, this.rsocial, this.direccion, this.ciudad, this.telefono, this.email);
            int fila = pers.insertarPersona();
            if (fila > 0) {
                PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
                psipc.setString(1, this.rut_cliente);
                psipc.setInt(2, this.descuento);
                psipc.setString(3, this.clasificacion);
                filasAfectadas = psipc.executeUpdate();
            } else {
                System.out.println("no se inserto");
            }
        } catch (SQLException ex) {
            System.out.println("Cliente.insertarCliente(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }
    
    
    public int eliminarCliente() {
        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlDelete = "delete from cliente  where rut like ?";
        try {
            PreparedStatement psipc = oraconex.prepareStatement(sqlDelete);
            psipc.setString(1, this.rut_cliente);
            filasAfectadas = psipc.executeUpdate();
            if (filasAfectadas > 0) {
                Persona per = new Persona(this.rut_cliente);
                per.eliminarPersona();
            } else {
                System.out.println("Cliente no ha podido ser eliminado");
            }
        } catch (SQLException ex) {
            System.out.println("Cliente.eliminarCliente(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }
    
    public int actualizarCliente(){
        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlUpdate = "update cliente set descuento=?, clasificacion=? where rut like ? ";
        try {
            PreparedStatement psupc = oraconex.prepareStatement(sqlUpdate);
            psupc.setInt(1, this.descuento);
            psupc.setString(2, this.clasificacion);
            psupc.setString(3, this.rut_cliente);

            filasAfectadas = psupc.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Cliente.actualizarCliente(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }

    public String getTodosClientes() {

        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from cliente inner join persona using (rut) where rut like '%" + this.rut_cliente + "%' ";
        int cont = 0;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);

            ResultSet rs = psbc.executeQuery();
            while (rs.next()) {
                cont++;
            }
            System.out.println("contador" + cont);
            this.todosClientes = "";
            rs = psbc.executeQuery();
            int i = 0;
            while (rs.next()) {
                this.todosClientes += rs.getString(1) + ","+rs.getInt(2) + ","+rs.getString(3) + ","+rs.getString(4) + ","+rs.getString(5) + ","+rs.getString(6) + ","+rs.getString(7) + ","+rs.getString(8) +"|";
                System.out.println("todos los clientes : " + this.todosClientes);
                i++;
            }
        } catch (Exception e) {
            return null;
        } finally {
            conora.closeConexionOracle();
            return this.todosClientes.substring(0, this.todosClientes.length() - 1);
        }
    }

    @Override
    public String toString() {
        return super.toString() + ", descuento=" + descuento + ", clasificacion=" + clasificacion;
    }
}
