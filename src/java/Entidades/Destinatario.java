package Entidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utilidades.Oraconex;

public class Destinatario {
    private String rut_dest;
    private String rut_cliente;
    private String rsocial;
    private String direccion;
    private String ciudad;
    private String telefono;
    private String email;

    private String todosDestinatarios;
    public Destinatario() {
    }

    public Destinatario(String rut, String rut_cliente,String rsocial, String direccion, String ciudad, String telefono, String email) {
        this.rut_dest = rut;
        this.rut_cliente = rut_cliente;
        this.rsocial=rsocial;
        this.direccion=direccion;
        this.ciudad=ciudad;
        this.telefono=telefono;
        this.email=email;
    }
    
    public Destinatario (String rut, String rut_cliente){
    this.rut_dest=rut;
    this.rut_cliente=rut_cliente;
    }
    
     public int insertarDestinatario(){
        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        
        String sqlInsert = "insert into destinatario  values(?,?)";
        try {
            Persona pers=new Persona(this.rut_dest,this.rsocial,this.direccion,this.ciudad,this.telefono,this.email);
            int fila = pers.insertarPersona();
            if(fila>0){
                PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
                psipc.setString(1, this.rut_dest);
                psipc.setString(2, this.rut_cliente);
                filasAfectadas = psipc.executeUpdate();
            }else {
                System.out.println("no se inserto");
            }
        } catch (SQLException ex) {
            System.out.println("Destinatario.insertarDestinatario(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }
     
     public int eliminarDestinatario() {
        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlInsert = "delete from destinatario  where rut like ? and rut_cliente like ?";
        try {
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setString(1, this.rut_dest);
            psipc.setString(2, this.rut_cliente);
            filasAfectadas = psipc.executeUpdate();
            if (filasAfectadas > 0) {
                Persona per = new Persona(rut_dest);
                per.eliminarPersona();
            } else {
                System.out.println("Destinatario no ha podido ser eliminado");
            }
        } catch (SQLException ex) {
            System.out.println("Destinatario.eliminarDestinatario(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }
     
    public Persona buscarDestinatario() {
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select *from persona  inner join destinatario using (rut) where rut like ? and rut_cliente like ? ";
        Persona perso = null;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);
            psbc.setString(1, this.rut_dest);
            psbc.setString(2, this.rut_cliente);
            ResultSet rs = psbc.executeQuery();

            rs.next();
            String rut = rs.getString(1);
            System.out.println("rut : " + rut);
            String rsoc = rs.getString(2);
            System.out.println("razon social : " + rsoc);
            String direc = rs.getString(3);
            System.out.println("dieccion : " + direc);
            String ciu = rs.getString(4);
            System.out.println("ciudad : " + ciu);
            String telef = rs.getString(5);
            System.out.println("teleofono : " + telef);
            String ema = rs.getString(6);
            System.out.println("email : " + ema);
            perso = new Persona(rut,rsoc, direc, ciu, telef, ema);

        } catch (SQLException ex) {
            System.out.println("Destinatario.buscarDestinatario(): " + ex.getMessage());
            return null;
        } finally {
            conora.closeConexionOracle();
            return perso;
        }
    }

    public String getTodosDestinatario() {

        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from persona inner join destinatario using(rut) where rut like '%" + this.rut_dest + "%' and rut_cliente like '%"+this.rut_cliente+"%' ";
        int cont = 0;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);

            ResultSet rs = psbc.executeQuery();
            while (rs.next()) {
                cont++;
            }
            System.out.println("contador" + cont);
            this.todosDestinatarios = "";
            rs = psbc.executeQuery();
            int i = 0;
            while (rs.next()) {
                this.todosDestinatarios += rs.getString(1) + ","+rs.getString(2) + ","+rs.getString(3) + ","+rs.getString(4) + ","+rs.getString(5) + ","+rs.getString(6) + "|";
                System.out.println("todos los destinatarios : " + this.todosDestinatarios);
                i++;
            }
        } catch (Exception e) {
            return null;
        } finally {
            conora.closeConexionOracle();
            return this.todosDestinatarios.substring(0, this.todosDestinatarios.length() - 1);
        }
    }
    @Override
    public String toString() {
        return   super.toString()+"|" + rut_cliente ;
    }
    
    
}
