
package Entidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utilidades.Oraconex;

public class Detalle_entrada {
    private int codigo_entrada;
    private int codigo_articulo;
    private int cantidad;
    private String todosDetalle_entrada;
    
    public Detalle_entrada() {
    }

    public Detalle_entrada(int codigo_entrada) {
        this.codigo_entrada = codigo_entrada;
    }
    

    public Detalle_entrada(int codigo_entrada, int codigo_articulo, int cantidad) {
        this.codigo_entrada = codigo_entrada;
        this.codigo_articulo = codigo_articulo;
        this.cantidad = cantidad;
    }
    public int insertarDetalle_entrada() {

        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlInsert = "insert into detalle_entrada  values(?,?,?)";
        try {
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setInt(1, this.codigo_entrada);
            psipc.setInt(2, this.codigo_articulo);
            psipc.setInt(3,this.cantidad);
            filasAfectadas = psipc.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Detalle_entrada.insertarDetalle_entrada(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;

        }

    }   
    
        public Detalle_entrada buscarDetalle_Entrada() {
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from detalle_entrada where codigo_entrada = ? ";
        Detalle_entrada detalle_entra = null;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);
            psbc.setInt(1, this.codigo_entrada);
            ResultSet rs = psbc.executeQuery();
            
            rs.next();
            int cod_entra = rs.getInt(1);
            System.out.println("codigo entrada : " + cod_entra);
            int cod_arti = rs.getInt(2);
            System.out.println("codigo articulo : " + cod_arti);
            int canti = rs.getInt(3);
            System.out.println("cantidad : " + canti);
           
            detalle_entra = new Detalle_entrada(cod_entra, cod_arti, canti);

        } catch (SQLException ex) {
            System.out.println("Detalle_entrada.buscarDetalle_entrada(): " + ex.getMessage());
            return null;
        } finally {
            conora.closeConexionOracle();
            return detalle_entra;
        }
    }


    public String getTodosDetalle_entrada() {

        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from detalle_entrada where codigo_entrada="+this.codigo_entrada+" ";
        int cont = 0;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);

            ResultSet rs = psbc.executeQuery();
            while (rs.next()) {
                cont++;
            }
            System.out.println("contador" + cont);
            this.todosDetalle_entrada = "";
            rs = psbc.executeQuery();
            int i = 0;
            while (rs.next()) {
                this.todosDetalle_entrada += rs.getInt(1)+","+rs.getInt(2)+","+rs.getInt(3) + "*";
                System.out.println("todos los detalles entrada : " + this.todosDetalle_entrada);
                i++;
            }
        } catch (Exception e) {
            return null;
        } finally {
            conora.closeConexionOracle();
            return this.todosDetalle_entrada.substring(0, this.todosDetalle_entrada.length() - 1);
        }
    }

    
    @Override
    public String toString() {
        return codigo_entrada + "|" + codigo_articulo + "|" + cantidad;
    }
   
}