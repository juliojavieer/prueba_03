
package Entidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import utilidades.Oraconex;


public class Detalle_lista_cte {
    private int folio;
    private int codigo_articulo;
    private String fecha_salida;
    private int cantidad;
    private String rut_destinatario;
    private int codigo_entrada;
    private String todosDetalle_lista_cte;

    public Detalle_lista_cte() {
    }

    public Detalle_lista_cte(int folio) {
        this.folio = folio;
    }

    public Detalle_lista_cte(int folio, int codigo_articulo, String fecha_salida, int cantidad, String rut_destinatario, int codigo_entrada) {
        this.folio = folio;
        this.codigo_articulo = codigo_articulo;
        this.fecha_salida = fecha_salida;
        this.cantidad = cantidad;
        this.rut_destinatario = rut_destinatario;
        this.codigo_entrada = codigo_entrada;
    }
    
    public int insertarDetalle_lista_cte() {

        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
 
        Connection oraconex = conora.getConexionOracle();
        String sqlInsert = "insert into detalle_lista_cte values(?,?,?,?,?,?)";
        try {
 
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setInt(1, this.folio);
            psipc.setInt(2, this.codigo_articulo);
            psipc.setString(3, this.fecha_salida);
            psipc.setInt(4, this.cantidad);
            psipc.setString(5, this.rut_destinatario);
            psipc.setInt(6, this.codigo_entrada);
            
            filasAfectadas = psipc.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Detalle_lista_cte.insertarDetalle_lista_cte(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;

        }

    }   
    
        public Detalle_lista_cte buscarDetalle_lista_cte() {
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select *from detalle_lista_cte where folio = ? ";
        Detalle_lista_cte detalle_lista_cte = null;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);
            psbc.setInt(1, this.folio);
            ResultSet rs = psbc.executeQuery();
            
            rs.next();
            int folio = rs.getInt(1);
            System.out.println("folio : " + folio);
            int cod_arti = rs.getInt(2);
            System.out.println("codigo articulo : " + cod_arti);
            String fecha= rs.getString(3);
            System.out.println("Fecha salida : "+fecha);
            int canti = rs.getInt(4);
            System.out.println("cantidad : " + canti);
            String rut_dest=rs.getString(5);
            System.out.println("rut destinatario : "+rut_dest);
            int cod_entra=rs.getInt(6);
            System.out.println("codigo entrada : "+cod_entra);
           
            detalle_lista_cte = new Detalle_lista_cte(folio, cod_arti,fecha, canti,rut_dest,cod_entra);

        } catch (SQLException ex) {
            System.out.println("Detalle_lista_cte.buscarDetalle_lista_cte(): " + ex.getMessage());
            return null;
        } finally {
            conora.closeConexionOracle();
            return detalle_lista_cte;
        }
    }


    public String getTodosDetalle_lista_cte() {


        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from detalle_lista_cte where folio="+folio+"";
        int cont = 0;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);

            ResultSet rs = psbc.executeQuery();
            while (rs.next()) {
                cont++;
            }
            System.out.println("contador" + cont);
            this.todosDetalle_lista_cte = "";
            rs = psbc.executeQuery();
            int i = 0;
            while (rs.next()) {
                this.todosDetalle_lista_cte += rs.getString(1) + "," + rs.getString(2) + "," + rs.getString(3) + "," + rs.getString(4) + "," + rs.getString(5) + "," + rs.getString(6) + "|";
                i++;
            }
        } catch (Exception e) {
            return null;
        } finally {
            conora.closeConexionOracle();
            return this.todosDetalle_lista_cte.substring(0, this.todosDetalle_lista_cte.length() - 1);
        }
    }

    @Override
    public String toString() {
        return folio + "|" + codigo_articulo + "|" + fecha_salida + "|" + cantidad + "|" + rut_destinatario + "|" + codigo_entrada;
    }
    
}