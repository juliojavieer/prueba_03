
package Entidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import utilidades.Oraconex;

public class Entrada_mercancia {
    private int codigo_entrada;
    private String fecha;
    private String patente_camion;
    private String rut_cliente;
    private String todosEntrada_mercancia;
    public Entrada_mercancia() {
            }

    public Entrada_mercancia(int codigo_entrada, String rut_cliente) {
        this.codigo_entrada = codigo_entrada;
        this.rut_cliente = rut_cliente;
    }

    public Entrada_mercancia(String rut_cliente) {
        this.rut_cliente = rut_cliente;
    }

    
    public Entrada_mercancia(int codigo_entrada, String fecha, String patente_camion, String rut_cliente) {
        this.codigo_entrada = codigo_entrada;
        this.fecha = fecha;
        this.patente_camion = patente_camion;
        this.rut_cliente = rut_cliente;
    }
    public int insertarEntrada_mercancia() {

        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlInsert = "insert into entrada_mercancia  values(?,?,?,?)";

        try {
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setInt(1, this.codigo_entrada);
            psipc.setString(2, this.fecha);
            psipc.setString(3, this.patente_camion);            
            psipc.setString(4,this.rut_cliente);
            filasAfectadas = psipc.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Entrada_mercancia.insertarDetalle_mercancia(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;

        }

    }   
    
        public Entrada_mercancia buscarEntrada_mercancia() {
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select *from entrada_mercancia where codigo_entrada = ? ";
        Entrada_mercancia entrada_merca = null;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);
            psbc.setInt(1, this.codigo_entrada);
            ResultSet rs = psbc.executeQuery();

            rs.next();
            int cod_entra = rs.getInt(1);
            System.out.println("codigo entrada : " + cod_entra);
            String fech = rs.getString(2);
            System.out.println("Fecha : " + fecha);
            String patente = rs.getString(3);
            System.out.println("patente camion : " + patente);
            String rut_cliente = rs.getString(4);
            System.out.println("rut cliente : " + rut_cliente);
           
            entrada_merca = new Entrada_mercancia(cod_entra, fech, patente,rut_cliente);

        } catch (SQLException ex) {
            System.out.println("Entrada_mercancia.buscarEntrada_mercancia(): " + ex.getMessage());
            return null;
        } finally {
            conora.closeConexionOracle();
            return entrada_merca;
        }
    }


    public String getTodosEntrada_mercancia() {

        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from entrada_mercancia where rut_cliente='"+this.rut_cliente+"' ";
        int cont = 0;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);

            ResultSet rs = psbc.executeQuery();
            while (rs.next()) {
                cont++;
            }
            System.out.println("contador" + cont);
            this.todosEntrada_mercancia = "";
            rs = psbc.executeQuery();
            int i = 0;
            while (rs.next()) {
                this.todosEntrada_mercancia += rs.getInt(1) +","+rs.getString(2)+","+rs.getString(3) + "|";
                System.out.println("todos las entrada mercancia  : " + this.todosEntrada_mercancia);
                i++;
            }
        } catch (Exception e) {
            return null;
        } finally {
            conora.closeConexionOracle();
            return this.todosEntrada_mercancia.substring(0, this.todosEntrada_mercancia.length() - 1);
        }
    }
    
    @Override
    public String toString() {
        return  codigo_entrada + "|" + fecha + "|" + patente_camion + "|" + rut_cliente;
    }

}