package Entidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import utilidades.Oraconex;

public class Lista_salida {
    private int folio;
    private String rut_cliente;
    private String fecha;
    private String todosListasalida;
  
    public Lista_salida() {
    }

    public Lista_salida(int folio, String rut_cliente, String fecha) {
        this.folio = folio;
        this.rut_cliente = rut_cliente;
        this.fecha = fecha;
    }

    public Lista_salida(String rut_cliente) {
        this.rut_cliente = rut_cliente;
    }
    

    public Lista_salida(int folio, String rut_cliente) {
        this.folio = folio;
        this.rut_cliente = rut_cliente;
    }
    
     public int insertarLista_salida() {

        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlInsert = "insert into lista_salida  values(?,?,?)";
        try {

            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setInt(1, this.folio);
            psipc.setString(2, this.rut_cliente);
            psipc.setString(3, this.fecha);
            filasAfectadas = psipc.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Lista_Salida.insertarLista_Salida(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;

        }

    }   
    
        public Lista_salida buscarLista_salida() {
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select *from lista_salida where folio = ? ";
        Lista_salida lista_salia = null;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);
            psbc.setInt(1, this.folio);
            ResultSet rs = psbc.executeQuery();
            
            rs.next();
            int fol = rs.getInt(1);
            System.out.println("folio : " + fol);
            String rut_c = rs.getString(2);
            System.out.println("rut cliente : " + rut_c);
           String fech = rs.getString(3);
            System.out.println("Fecha : " + fech);
            lista_salia = new Lista_salida(fol,rut_c, fech);

        } catch (SQLException ex) {
            System.out.println("Lista_salida.buscarLista_salida(): " + ex.getMessage());
            return null;
        } finally {
            conora.closeConexionOracle();
            return lista_salia;
        }
    }


    public String getTodosLista_salida() {

        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from lista_salida where rut_cliente='"+rut_cliente+"' ";
        int cont = 0;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);

            ResultSet rs = psbc.executeQuery();
            while (rs.next()) {
                cont++;
            }
            System.out.println("contador" + cont);
            this.todosListasalida = "";
            rs = psbc.executeQuery();
            int i = 0;
            while (rs.next()) {
                this.todosListasalida += rs.getInt(1) + ","+rs.getString(3)+"|";
                System.out.println("todos las lista salida : " + this.todosListasalida);
                i++;
            }
        } catch (Exception e) {
            return null;
        } finally {
            conora.closeConexionOracle();
            return this.todosListasalida.substring(0, this.todosListasalida.length() - 1);
        }
    }
    @Override
    public String toString() {
        return folio + "|" + rut_cliente + "|" + fecha;
    }
    
}
