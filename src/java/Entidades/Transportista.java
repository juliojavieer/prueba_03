package Entidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utilidades.Oraconex;

public class Transportista {

    private String rut_trans;
    private String rut_cliente;
    private String rsocial;
    private String direccion;
    private String ciudad;
    private String telefono;
    private String email;
    private String todosTransportistas;

    public Transportista() {
    }

    public Transportista(String rut) {
        this.rut_trans = rut;
    }
    
    public Transportista(String rut_trans, String rut_cliente){
        this.rut_trans = rut_trans;
        this.rut_cliente = rut_cliente;
    }

    public Transportista(String rut_trans, String rut_cliente, String rsocial, String direccion, String ciudad, String telefono, String email) {
        //super(rut_trans, rsocial, direccion, ciudad, telefono, email);
        this.rut_trans = rut_trans;
        this.rut_cliente = rut_cliente;
        this.rsocial = rsocial;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.telefono = telefono;
        this.email = email;
    }

    public int insertarTransportista() {
        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();

        String sqlInsert = "insert into transportista  values(?,?)";
        Persona pertrans = new Persona(this.rut_trans, this.rsocial, this.direccion, this.ciudad, this.telefono, this.email);
        try {
            int fila = pertrans.insertarPersona();
            if (fila > 0) {
                PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
                psipc.setString(1, this.rut_trans);
                psipc.setString(2, this.rut_cliente);
                filasAfectadas = psipc.executeUpdate();
            } else {
                System.out.println("no se inserto");
            }
        } catch (SQLException ex) {
            System.out.println("transportista.insertarTransportista(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }
    
    public int eliminarTransportista() {
        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlInsert = "delete from transportista  where rut like ? and rut_cliente like ?";
        try {
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setString(1, this.rut_trans);
            psipc.setString(2,this.rut_cliente);
            filasAfectadas = psipc.executeUpdate();
            if (filasAfectadas > 0) {
                Persona per = new Persona(rut_trans);
                per.eliminarPersona();
            } else {
                System.out.println("Transportista no ha podido ser eliminado");
            }
        } catch (SQLException ex) {
            System.out.println("Transportista.eliminarTransportista(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }

    public Persona buscarTransportista() {
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select *from persona inner join transportista using(rut) where rut like ? and rut_cliente like ?";
        Persona transportista = null;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);
            psbc.setString(1, this.rut_trans);
            psbc.setString(2, this.rut_cliente);
            ResultSet rs = psbc.executeQuery();

            rs.next();
            String rut = rs.getString(1);
            System.out.println("rut : " + rut);
            //String rutCliente = rs.getString(2);
            //System.out.println("rut cliente : " + rutCliente);
            String rsoc = rs.getString(2);
            System.out.println("razon social : " + rsoc);
            String direc = rs.getString(3);
            System.out.println("dieccion : " + direc);
            String ciu = rs.getString(4);
            System.out.println("ciudad : " + ciu);
            String telef = rs.getString(5);
            System.out.println("teleofono : " + telef);
            String ema = rs.getString(6);
            System.out.println("email : " + ema);
            transportista = new Persona(rut, rsoc, direc, ciu, telef, ema);

        } catch (SQLException ex) {
            System.out.println("Transportista.buscarTransportista(): " + ex.getMessage());
            return null;
        } finally {
            conora.closeConexionOracle();
            return transportista;
        }
    }

    public String getTodosTransportistas() {

        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from persona inner join transportista using(rut) where rut like '%" + this.rut_trans + "%' and rut_cliente like '%"+this.rut_cliente+"%'";
        int cont = 0;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);

            ResultSet rs = psbc.executeQuery();
            while (rs.next()) {
                cont++;
            }
            System.out.println("contador" + cont);
            this.todosTransportistas = "";
            rs = psbc.executeQuery();
            int i = 0;
            while (rs.next()) {
                this.todosTransportistas += rs.getString(1) + "," + rs.getString(2) +","+ rs.getString(3) + ","+rs.getString(4) + ","+rs.getString(5) + ","+rs.getString(6) + "|";
                System.out.println("todos los transportistas : " + this.todosTransportistas);
                i++;
            }
        } catch (Exception e) {
            return null;
        } finally {
            conora.closeConexionOracle();
            return this.todosTransportistas.substring(0, this.todosTransportistas.length() - 1);
        }
    }

    @Override
    public String toString() {
        return super.toString() + "|" + rut_cliente;
    }
}
