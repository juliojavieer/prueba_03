package Entidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import utilidades.Oraconex;

public class Ubicacion {

    private int id_ubicacion;
    private int pasillo;
    private int ancho;
    private int largo;
    private int altura;
    private int estado;
    private int id_bodega;
    private String rut_cliente;
    private String todosUbicacion;

    public Ubicacion() {
    }

    public Ubicacion(int id_ubicacion) {
        this.id_ubicacion = id_ubicacion;
    }

    public Ubicacion(String rut_cliente) {
        this.rut_cliente = rut_cliente;
    }

    public Ubicacion(int id_ubicacion, int pasillo, int ancho, int largo, int altura, int estado, int id_bodega, String rut_cliente) {
        this.id_ubicacion = id_ubicacion;
        this.pasillo = pasillo;
        this.ancho = ancho;
        this.largo = largo;
        this.altura = altura;
        this.estado = estado;
        this.id_bodega = id_bodega;
        this.rut_cliente = rut_cliente;
    }

    public Ubicacion(int id_ubicacion, String rut_cliente) {
        this.id_ubicacion = id_ubicacion;
        this.rut_cliente = rut_cliente;
    }

    public int insertarUbicacion() {

        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();

        Connection oraconex = conora.getConexionOracle();
        String sqlInsert = "insert into ubicacion values(?,?,?,?,?,?,?,?)";
        try {

            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setInt(1, this.id_ubicacion);
            psipc.setInt(2, this.pasillo);
            psipc.setInt(3, this.ancho);
            psipc.setInt(4, this.largo);
            psipc.setInt(5, this.altura);
            psipc.setInt(6, this.estado);
            psipc.setInt(7, this.id_bodega);
            psipc.setString(8, this.rut_cliente);

            filasAfectadas = psipc.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Ubicacion.insertarUbicacion(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;

        }

    }

    public int actualizarEstado() {
        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlUpdate = "update ubicacion set estado = 1 where id_ubicacion =" + this.id_ubicacion;
        try {
            PreparedStatement psupc = oraconex.prepareStatement(sqlUpdate);
            filasAfectadas = psupc.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Ubicacion.actualizarUbicacion(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }

    public Ubicacion buscarUbicacion() {
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select *from ubicacion where id_ubicacion = ? and rut_cliente like ?";
        Ubicacion ubicatex = null;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);
            psbc.setInt(1, this.id_ubicacion);
            psbc.setString(2, this.rut_cliente);
            ResultSet rs = psbc.executeQuery();

            rs.next();
            int id_ubi = rs.getInt(1);
            System.out.println("id ubicacion : " + id_ubi);
            int pass = rs.getInt(2);
            System.out.println("pasillo : " + pass);
            int anch = rs.getInt(3);
            System.out.println("ancho : " + anch);
            int largo = rs.getInt(4);
            System.out.println("largo : " + largo);
            int altu = rs.getInt(5);
            System.out.println("altura : " + altu);
            int esta = rs.getInt(6);
            System.out.println("estado : " + esta);
            int id_bod = rs.getInt(7);
            System.out.println("id bodega : " + id_bod);
            String rut = rs.getString(8);
            System.out.println("id bodega : " + id_bod);
            ubicatex = new Ubicacion(id_ubi, pass, anch, largo, altu, esta, id_bod, rut);

        } catch (SQLException ex) {
            System.out.println("Detalle_lista_cte.buscarDetalle_lista_cte(): " + ex.getMessage());
            return null;
        } finally {
            conora.closeConexionOracle();
            return ubicatex;
        }
    }

    public String getTodosUbicaciones() {

        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from ubicacion inner join cliente on(cliente.rut=ubicacion.rut_cliente)  where rut like '" + rut_cliente + "' ";
        int cont = 0;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);

            ResultSet rs = psbc.executeQuery();
            while (rs.next()) {
                cont++;
            }
            System.out.println("contador" + cont);
            this.todosUbicacion = "";
            rs = psbc.executeQuery();
            int i = 0;
            while (rs.next()) {
                this.todosUbicacion += rs.getString(1) + "," + rs.getString(2) + "," + rs.getString(3) + "," + rs.getString(4) + "," + rs.getString(5) + "," + rs.getString(6) + "," + rs.getString(7) + "|";
                System.out.println("todos las ubicaciones : " + this.todosUbicacion);
                i++;
            }
        } catch (Exception e) {
            return null;
        } finally {
            conora.closeConexionOracle();
            return this.todosUbicacion.substring(0, this.todosUbicacion.length() - 1);
        }
    }

    public String getUbicacionesLibres() {

        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlSelect = "select * from ubicacion inner join cliente on(cliente.rut=ubicacion.rut_cliente)  where rut like '" + rut_cliente + "' and estado = 0 ";
        int cont = 0;
        try {
            PreparedStatement psbc = oraconex.prepareStatement(sqlSelect);

            ResultSet rs = psbc.executeQuery();
            while (rs.next()) {
                cont++;
            }
            System.out.println("contador" + cont);
            this.todosUbicacion = "";
            rs = psbc.executeQuery();
            int i = 0;
            while (rs.next()) {
                this.todosUbicacion += rs.getString(1) + "," + rs.getString(2) + "," + rs.getString(3) + "," + rs.getString(4) + "," + rs.getString(5) + "," + rs.getString(6) + "," + rs.getString(7) + "|";
                System.out.println("todos las ubicaciones : " + this.todosUbicacion);
                i++;
            }

        } catch (Exception e) {
            return null;
        } finally {
            conora.closeConexionOracle();
            return this.todosUbicacion.substring(0, this.todosUbicacion.length() - 1);
        }
    }

    public int eliminarUbicacion() {
        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        String sqlInsert = "delete from ubicacion  where id_bodega = ? and rut_cliente like ?";
        try {
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setInt(1, this.id_bodega);
            psipc.setString(2, this.rut_cliente);
            filasAfectadas = psipc.executeUpdate();


        } catch (SQLException ex) {
            System.out.println("Ubicacion.eliminarUbicacion()(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }
    }

    @Override
    public String toString() {
        return id_ubicacion + "|" + pasillo + "|" + ancho + "|" + largo + "|" + altura + "|" + estado + "|" + id_bodega + "|" + rut_cliente;
    }
}