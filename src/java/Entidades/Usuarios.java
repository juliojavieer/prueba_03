package Entidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import utilidades.Oraconex;

public class Usuarios {

    private String usuario;
    private String pass;

    public Usuarios() {
    }

    public int insertarUsuarioCliente() {
        int filasAfectadas = 0;
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();

        String sqlInsert = "insert into usuarios values(?,?,'user')";

        try {
            PreparedStatement psipc = oraconex.prepareStatement(sqlInsert);
            psipc.setString(1, this.usuario);
            psipc.setString(2, this.pass);


            filasAfectadas = psipc.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Usuario.insertarUsuarioCliente(): " + ex.getMessage());
        } finally {
            conora.closeConexionOracle();
            return filasAfectadas;
        }

    }

    public Usuarios(String usuario, String pass) {
        this.usuario = usuario;
        this.pass = pass;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getPass() {
        return pass;
    }

    @Override
    public String toString() {
        return usuario + "|" + pass;
    }
}