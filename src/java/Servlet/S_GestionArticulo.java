package Servlet;

import Entidades.Articulo;
import Entidades.Cliente;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "S_GestionArticulo", urlPatterns = {"/S_GestionArticulo"})
public class S_GestionArticulo extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession actual = request.getSession(true);
        String id = actual.getId();
        String nombre = (String) actual.getAttribute("logueado");

        try {
            String parametro_boton = request.getParameter("parametro_boton");
            System.out.println("parametro_boton: " + parametro_boton);

            if (parametro_boton.equalsIgnoreCase("botonBuscarArticulo")) {
                String id_art = request.getParameter("parametro_id");
                System.out.println("rut : " + id_art);
                String laBusqueda = buscarArticulo(id_art);
                out.print(laBusqueda);
            }else if(parametro_boton.equalsIgnoreCase("botonEliminarArticulo")){
                String id_art = request.getParameter("parametro_id");
                System.out.println("id articulo :"+id_art);
                String elim = eliminarArticulo(id_art);
                out.print(elim);
            }
            else if (parametro_boton.equalsIgnoreCase("botonBuscarTodosArticulos")) {
                String rutpro = request.getParameter("parametro_rut_pro");
                System.out.println("rut proveedor : " + rutpro);
                String laBusqueda = buscarTodosArticulos(rutpro, nombre);
                out.print(laBusqueda);
            } else if (parametro_boton.equalsIgnoreCase("boton_buscar_todos_art")) {
                String laBusqueda = buscarTodosArticulos("", nombre);
                out.print(laBusqueda);
            } else if (parametro_boton.equalsIgnoreCase("boton_guarda_articulo")) {
                String elJSON = request.getParameter("jsonArt");
                System.out.println(elJSON);
                Gson gson = new Gson();
                Articulo jsonArt = gson.fromJson(elJSON, Articulo.class);
                System.out.println("jsonPro: " + jsonArt.toString());
                int filasIngresadas = jsonArt.insertarArticuloCompleto();
                if (filasIngresadas > 0) {
                    out.print("<font color='green'>Articulo ingresado correctamente.</font>");
                } else {
                    out.print("<font color='red'>Error, Articulo no ingresado.</font>");
                }
            }

        } catch (Exception e) {
            out.print("error");
            System.out.println("error: " + e.getMessage());
        } finally {
            out.close();
        }
    }

    private String buscarArticulo(String rut) {
        Cliente clien = new Cliente(rut);
        Cliente proRespuesta = clien.buscarCliente();
        if (proRespuesta != null) {
            System.out.println("respuesta : " + proRespuesta.toString());
            Gson gsonToPerson = new Gson();
            String jsonPersona = gsonToPerson.toJson(proRespuesta);
            System.out.println("json per " + jsonPersona);
            return jsonPersona;
        } else {
            return "error";
        }
    }

    private String buscarTodosArticulos(String rut, String rutcli) {
        String todos;
        Articulo art = new Articulo(rut);
        todos = art.getTodosArticulos();
        if (todos != null) {
            System.out.println("respuesta");
            return todos;
        } else {
            return todos;
        }
    }
    
    private String eliminarArticulo(String id_art) {
        Articulo art = new Articulo(Integer.parseInt(id_art));
        int fila = art.eliminarArticulo();
        if (fila != 0) {
            System.out.println("Articulo eliminado");
            return "<font color='green'>Articulo eliminado correctamente.</font>";
        } else {
            System.out.println("Articulo no eliminado");
            return "<font color='red'>Error, Articulos no eliminados.</font>";
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
