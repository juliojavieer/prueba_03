package Servlet;

import Entidades.Bodega;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "S_GestionBodega", urlPatterns = {"/S_GestionBodega"})
public class S_GestionBodega extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();


        try {
            String parametro_boton = request.getParameter("parametro_boton");
            System.out.println("parametro_boton: " + parametro_boton);

            HttpSession actual = request.getSession(true);
            String id = actual.getId();
            String nombre = (String) actual.getAttribute("logueado");

            if (parametro_boton.equalsIgnoreCase("boton_guarda_bodega")) {
                String elJSON = request.getParameter("jsonBod");
                System.out.println(elJSON);
                Gson gson = new Gson();
                Bodega jsonBodg = gson.fromJson(elJSON, Bodega.class);
                System.out.println("jsonPro: " + jsonBodg.toString());
                int filasIngresadas = jsonBodg.insertarBodega();

                if (filasIngresadas > 0) {
                    out.print("<font color='green'>Bodega ingresado correctamente.</font>");
                } else {
                    out.print("<font color='red'>Error, Bodega no ingresado.</font>");
                }
            }
            else if(parametro_boton.equalsIgnoreCase("botonBuscarBodega")){
                String id_bod = request.getParameter("parametro_id");
                System.out.println("id bodega : "+id_bod);
                String laBusqueda = buscarBodega(id_bod);
                out.print(laBusqueda);
            }
            else if(parametro_boton.equalsIgnoreCase("boton_autocom_bodega")){
                System.out.println("Autocom bodega");
                String laBusqueda = buscarTodasBodegas("-1");
                out.print(laBusqueda);
            }
            
            else if (parametro_boton.equalsIgnoreCase("boton_actualiza_bodega")) {
                String elJSON = request.getParameter("jsonBod");
                System.out.println("actualiza bodga "+elJSON);
                Gson gson = new Gson();
                Bodega jsonBod = gson.fromJson(elJSON, Bodega.class);
                System.out.println(" persona actualizar "+jsonBod.toString());
                int filasIngresadas = jsonBod.actualizarBodega();
                if (filasIngresadas > 0) {
                    out.print("<font color='green'>Bodega actualizada correctamente</font>");
                } else {
                    out.print("<font color='red'>Error, bodega no actualizado</font>");
                }
            }
            
            else if(parametro_boton.equalsIgnoreCase("botonBuscarTodasBodegas")){
                System.out.println("Buscar todas bodegas");
                String laBusqueda = buscarTodasBodegas("-1");
                out.print(laBusqueda);
            }

        } catch (Exception e) {
            out.print("error");
            System.out.println("error: " + e.getMessage());
        } finally {
            out.close();
        }
        
        
    }
    private String buscarBodega(String id_bod) {
        Bodega bod = new Bodega(Integer.parseInt(id_bod));
        Bodega bodRespuesta = bod.buscarBodega();
        if (bodRespuesta != null) {
            System.out.println("respuesta : " + bodRespuesta.toString());
            Gson gsonToPerson = new Gson();
            String jsonPersona = gsonToPerson.toJson(bodRespuesta);
            System.out.println("json per "+jsonPersona);
            return jsonPersona;
        } else {
            return "error";
        }
    }
    
    private String buscarTodasBodegas(String id) {
        String todos;
        System.out.println("id_bod : "+Integer.parseInt(id));
        Bodega bod = new Bodega(Integer.parseInt(id));
        todos = bod.getTodasBodegas();
        if (todos != null) {
            System.out.println("respuesta");
            return todos;
        } else {
            return todos;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
