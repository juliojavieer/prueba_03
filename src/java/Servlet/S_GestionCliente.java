package Servlet;

import Entidades.Cliente;
import Entidades.Persona;
import Entidades.Usuarios;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "S_GestionCliente", urlPatterns = {"/S_GestionCliente"})
public class S_GestionCliente extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();


        try {
            String parametro_boton = request.getParameter("parametro_boton");
            System.out.println("parametro_boton: " + parametro_boton);

            if (parametro_boton.equalsIgnoreCase("botonBuscarCliente")) {
                String rut = request.getParameter("parametro_rut");
                System.out.println("rut : " + rut);
                String laBusqueda = buscarCliente(rut);
                out.print(laBusqueda);
            } else if (parametro_boton.equalsIgnoreCase("botonBuscarTodosClientes")) {
                String rut = request.getParameter("parametro_rut");
                System.out.println("rut : " + rut);
                String laBusqueda = buscarTodosClientes(rut);
                out.print(laBusqueda);
            } else if (parametro_boton.equalsIgnoreCase("boton_buscar_todos_cli")) {
                String laBusqueda = buscarTodosClientes("");
                out.print(laBusqueda);
            } else if (parametro_boton.equalsIgnoreCase("boton_guarda_cliente")) {
                String elJSON = request.getParameter("jsonCli");
                System.out.println(elJSON);
                String elJSOND = request.getParameter("jsonUsr");
                Gson gson = new Gson();
                Cliente jsonPro = gson.fromJson(elJSON, Cliente.class);
                System.out.println("jsonPro: " + jsonPro.toString());
                Gson gsoncito = new Gson();
                Usuarios jsonUser = gsoncito.fromJson(elJSOND, Usuarios.class);
                int filasIngresadas = jsonPro.insertarCliente();
                int filaIngresada = jsonUser.insertarUsuarioCliente();
                if (filasIngresadas > 0 && filaIngresada > 0) {
                    out.print("<font color='green'>Cliente ingresado correctamente.</font>");
                } else {
                    out.print("<font color='red'>Error, Cliente no ingresado.</font>");
                }
            } else if (parametro_boton.equalsIgnoreCase("botonEliminarClientes")) {
                String rut = request.getParameter("parametro_rut");
                System.out.println("rut : " + rut);
                Cliente cliente = new Cliente(rut);
                int fila = cliente.eliminarCliente();
                if(fila>0)
                    out.print("<font color='green'>Cliente eliminado correctamente.</font>");
                else
                    out.print("<font color='red'>Error, Cliente no eliminado.</font>");
            }
            else if (parametro_boton.equalsIgnoreCase("boton_actualiza_persona_cliente")) {
                String elJSON = request.getParameter("jsonPer");
                System.out.println("actualiza "+elJSON);
                Gson gson = new Gson();
                Persona jsonPer = gson.fromJson(elJSON, Persona.class);
                System.out.println(" persona actualizar "+jsonPer.toString());
                int filasIngresadas = jsonPer.actualizarPersona();
                if (filasIngresadas > 0) {
                    out.print("<font color='green'>Actualizado correctamente</font>");
                } else {
                    out.print("<font color='red'>Error, no actualizado</font>");
                }
            }
            else if (parametro_boton.equalsIgnoreCase("boton_actualiza_cliente")) {
                String elJSON = request.getParameter("jsonCli");
                System.out.println("actualiza "+elJSON);
                Gson gson = new Gson();
                Cliente jsonCli = gson.fromJson(elJSON, Cliente.class);
                System.out.println(" persona actualizar "+jsonCli.toString());
                int filasIngresadas = jsonCli.actualizarCliente();
                if (filasIngresadas > 0) {
                    out.print("<font color='green'>Cliente actualizado correctamente</font>");
                } else {
                    out.print("<font color='red'>Error, cliente no actualizado</font>");
                }
            }
            else if(parametro_boton.equalsIgnoreCase("boton_completa_datos_cliente")){
                String rut = request.getParameter("parametro_rut");
                System.out.println("rut: "+rut);
                String laBusqueda = buscarCliente(rut);
                out.print(laBusqueda);
            }

        } catch (Exception e) {
            out.print("error");
            System.out.println("error: " + e.getMessage());
        } finally {
            out.close();
        }
    }
    
    
    

    private String buscarCliente(String rut) {
        Cliente clien = new Cliente(rut);
        Cliente proRespuesta = clien.buscarCliente();
        if (proRespuesta != null) {
            System.out.println("respuesta : " + proRespuesta.toString());
            Gson gsonToPerson = new Gson();
            String jsonPersona = gsonToPerson.toJson(proRespuesta);
            System.out.println("json per " + jsonPersona);
            return jsonPersona;
        } else {
            return "error";
        }
    }

    private String buscarTodosClientes(String rut) {
        String todos;
        Cliente clien = new Cliente(rut);
        todos = clien.getTodosClientes();
        if (todos != null) {
            System.out.println("respuesta");

            return todos;
        } else {
            return todos;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
