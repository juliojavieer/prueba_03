package Servlet;

import Entidades.Articulo;
import Entidades.Cliente;
import Entidades.Detalle_entrada;
import Entidades.Detalle_lista_cte;
import Entidades.Entrada_mercancia;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "S_GestionEntradaMercancia", urlPatterns = {"/S_GestionEntradaMercancia"})
public class S_GestionEntradaMercancia extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession actual = request.getSession(true);
        String id = actual.getId();
        String nombre = (String) actual.getAttribute("logueado");

        try {
            String parametro_boton = request.getParameter("parametro_boton");
            System.out.println("parametro_boton: " + parametro_boton);

            if (parametro_boton.equalsIgnoreCase("boton_buscar_lista_entrada")) {
                String codigo = request.getParameter("codigo_entrada");
                int cod= Integer.parseInt(codigo);
                System.out.println("codigo : " + codigo);
                String laBusqueda = buscarListaEntrada(cod,nombre);
                out.print(laBusqueda);
            }else if(parametro_boton.equalsIgnoreCase("botonEliminarEntrada")){
                String id_art = request.getParameter("parametro_id");
                System.out.println("id articulo :"+id_art);
               // String elim = (id_art);
                //out.print(elim);
            }
            else if (parametro_boton.equalsIgnoreCase("botonBuscarTodasEntradas")) {
                String rutpro = request.getParameter("parametro_rut_pro");
                System.out.println("rut proveedor : " + rutpro);
                String laBusqueda = buscarTodoasEntradas(rutpro, nombre);
                out.print(laBusqueda);
            } else if (parametro_boton.equalsIgnoreCase("botonBuscarTodasListasEntrada")) {
                String laBusqueda = buscarTodaListaEntrada(nombre);
                out.print(laBusqueda);
            } else if (parametro_boton.equalsIgnoreCase("boton_guarda_entrada_mercancia")) {
                String elJSON = request.getParameter("jsonEntrada");
                System.out.println(elJSON);
                Gson gson = new Gson();
                Entrada_mercancia jsonEntrada = gson.fromJson(elJSON, Entrada_mercancia.class);
                System.out.println("jsonEntrada: " + jsonEntrada.toString());
                int filasIngresadas = jsonEntrada.insertarEntrada_mercancia();
                if (filasIngresadas > 0) {
                    out.print("<font color='green'>Entrada ingresada correctamente.</font>");
                } else {
                    out.print("<font color='red'>Error, Entrada no ingresado.</font>");
                }
            }

        } catch (Exception e) {
            out.print("error");
            System.out.println("error: " + e.getMessage());
        } finally {
            out.close();
        }
    }

    private String buscarCodig(String rut) {
        Cliente clien = new Cliente(rut);
        Cliente proRespuesta = clien.buscarCliente();
        if (proRespuesta != null) {
            System.out.println("respuesta : " + proRespuesta.toString());
            Gson gsonToPerson = new Gson();
            String jsonPersona = gsonToPerson.toJson(proRespuesta);
            System.out.println("json per " + jsonPersona);
            return jsonPersona;
        } else {
            return "error";
        }
    }

    private String buscarTodoasEntradas(String rut, String rutcli) {
        String todos;
        Articulo art = new Articulo(rut);
        todos = art.getTodosArticulos();
        if (todos != null) {
            System.out.println("respuesta");
            return todos;
        } else {
            return todos;
        }
    }
    
   
    private String buscarListaEntrada(int cod, String nombre) {
        Entrada_mercancia entrada = new Entrada_mercancia(cod, nombre);
        Entrada_mercancia listRespuesta = entrada.buscarEntrada_mercancia();
        Detalle_entrada detalle = new Detalle_entrada(cod);
        String detRespuesta = detalle.getTodosDetalle_entrada();
        if (listRespuesta != null && detRespuesta != "") {
            Gson gsonToList = new Gson();
            String jsonList = gsonToList.toJson(listRespuesta);
            System.out.println("jsonList " + jsonList);
            System.out.println("respuesta : " + listRespuesta.toString());
            System.out.println("det respuesta"+detRespuesta);
            String json = jsonList + "#" + detRespuesta;
           
            return json;
        } else {
            return "error";
        }
    }

    private String buscarTodaListaEntrada(String rutcli) {
        String todos;
        Entrada_mercancia lista = new Entrada_mercancia(rutcli);
        todos = lista.getTodosEntrada_mercancia();
        if (todos != null) {
            System.out.println("respuesta");

            return todos;
        } else {
            return todos;
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}