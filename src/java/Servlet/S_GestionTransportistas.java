package Servlet;

import Entidades.Persona;
import Entidades.Transportista;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "S_GestionTransportistas", urlPatterns = {"/S_GestionTransportistas"})
public class S_GestionTransportistas extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();


        try {
            String parametro_boton = request.getParameter("parametro_boton");
            System.out.println("parametro_boton: " + parametro_boton);

            HttpSession actual = request.getSession(true);
            String id = actual.getId();
            String nombre = (String) actual.getAttribute("logueado");

            if (parametro_boton.equalsIgnoreCase("botonBuscarTransportistas")) {
                String rut = request.getParameter("parametro_rut");
                System.out.println("rut : " + rut + "user " + nombre);
                String laBusqueda = buscarTransportista(rut, nombre);
                out.print(laBusqueda);
            } else if (parametro_boton.equalsIgnoreCase("botonBuscarTodosTransportistas")) {
                String rut = request.getParameter("parametro_rut");
                System.out.println("rut : " + rut + " user: " + nombre);
                String laBusqueda = buscarTodosTransportistas(rut, nombre);
                out.print(laBusqueda);
            } else if (parametro_boton.equalsIgnoreCase("boton_buscar_todos_transportistas")) {
                System.out.println("user: " + nombre);
                String laBusqueda = buscarTodosTransportistas("", nombre);
                out.print(laBusqueda);
            }
            
            else if (parametro_boton.equalsIgnoreCase("boton_actualiza_transportista")) {
                String elJSON = request.getParameter("jsonPer");
                System.out.println("actualiza "+elJSON);
                Gson gson = new Gson();
                Persona jsonPer = gson.fromJson(elJSON, Persona.class);
                System.out.println(" persona actualizar "+jsonPer.toString());
                int filasIngresadas = jsonPer.actualizarPersona();
                if (filasIngresadas > 0) {
                    out.print("<font color='green'>Transportista actualizado correctamente</font>");
                } else {
                    out.print("<font color='red'>Error, Transportista no actualizado</font>");
                }
            }
            
            else if (parametro_boton.equalsIgnoreCase("botonEliminarTransportistas")) {
                String rut = request.getParameter("parametro_rut");
                System.out.println("rut : " + rut + "user " + nombre);
                String laBusqueda = eliminarTransportista(rut, nombre);
                out.print(laBusqueda);

            } else if (parametro_boton.equalsIgnoreCase("boton_guarda_transportista")) {
                String elJSON = request.getParameter("jsonPro");
                System.out.println(elJSON);
                Gson gson = new Gson();
                Transportista jsonPro = gson.fromJson(elJSON, Transportista.class);
                System.out.println("jsonPro: " + jsonPro.toString());
                int filasIngresadas = jsonPro.insertarTransportista();

                if (filasIngresadas > 0) {
                    out.print("<font color='green'>Transpoprtista ingresado correctamente.</font>");
                } else {
                    out.print("<font color='red'>Error, transportista no ingresado.</font>");
                }
            }

        } catch (Exception e) {
            out.print("error");
            System.out.println("error: " + e.getMessage());
        } finally {
            out.close();
        }
    }

    private String buscarTransportista(String rut, String rutcli) {
        Transportista transp = new Transportista(rut, rutcli);
        Persona proRespuesta = transp.buscarTransportista();
        if (proRespuesta != null) {
            System.out.println("respuesta : " + proRespuesta.toString());
            Gson gsonToPerson = new Gson();
            String jsonPersona = gsonToPerson.toJson(proRespuesta);
            System.out.println("json per " + jsonPersona);
            return jsonPersona;
        } else {
            return "error";
        }
    }

    private String buscarTodosTransportistas(String rut, String rutcli) {
        String todos;
        Transportista trans = new Transportista(rut, rutcli);
        todos = trans.getTodosTransportistas();
        if (todos != null) {
            System.out.println("respuesta");

            return todos;
        } else {
            return todos;
        }
    }

    private String eliminarTransportista(String rut, String rutcli) {
        Transportista trans = new Transportista(rut, rutcli);
        int fila = trans.eliminarTransportista();
        if (fila != 0) {
            System.out.println("proveedor eliminado");
            return "eliminado";
        } else {
            System.out.println("proveedor no eliminado");
            return "error";
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
