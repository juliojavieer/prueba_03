package Servlet;

import Entidades.Ubicacion;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "S_GestionUbicacion", urlPatterns = {"/S_GestionUbicacion"})
public class S_GestionUbicacion extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();


        try {
            String parametro_boton = request.getParameter("parametro_boton");
            System.out.println("parametro_boton: " + parametro_boton);

            HttpSession actual = request.getSession(true);
            String id = actual.getId();
            String nombre = (String) actual.getAttribute("logueado");

            if (parametro_boton.equalsIgnoreCase("boton_guarda_ubicacion")) {
                String elJSON = request.getParameter("jsonUbi");
                System.out.println(elJSON);
                Gson gson = new Gson();
                Ubicacion jsonUbi = gson.fromJson(elJSON, Ubicacion.class);
                System.out.println("jsonUbi: " + jsonUbi.toString());
                int filasIngresadas = jsonUbi.insertarUbicacion();

                if (filasIngresadas > 0) {
                    out.print("<font color='green'>Ubicacion ingresada correctamente.</font>");
                } else {
                    out.print("<font color='red'>Error, Ubicacion no ingresada.</font>");
                }
            } else if (parametro_boton.equalsIgnoreCase("boton_buscar_ubicacion")) {
                String id_ubicacion = request.getParameter("id_ubicacion");
                System.out.println("rut : " + id_ubicacion + "user " + nombre);
                int id_u = Integer.parseInt(id_ubicacion);
                String laBusqueda = buscarUbicacion(id_u, nombre);
                out.print(laBusqueda);
            } else if (parametro_boton.equalsIgnoreCase("boton_buscar_ubicaciones_libres")) {
                System.out.println("user: " + nombre);
                String laBusqueda = buscarUbicacionesLibres(nombre);
                out.print(laBusqueda);
             } else if (parametro_boton.equalsIgnoreCase("boton_buscar_todos_ubicacion")) {
                System.out.println("user: " + nombre);
                String laBusqueda = buscarTodosUbicacion(nombre);
                out.print(laBusqueda);
            }
            else if (parametro_boton.equalsIgnoreCase("botonEliminarUbicacion")) {
                String id_ubica = request.getParameter("id_ubicacion");
                int id_ubicac = Integer.parseInt(id_ubica);
                System.out.println("id_bodega : " + id_ubica + "user " + nombre);
                String laBusqueda = eliminarUbicacion(id_ubicac, nombre);
                out.print(laBusqueda);
            }
        } catch (Exception e) {
            out.print("error");
            System.out.println("error: " + e.getMessage());
        } finally {
            out.close();
        }
    }

    private String buscarUbicacion(int id_ubi, String rutcli) {
        Ubicacion ubica = new Ubicacion(id_ubi, rutcli);
        Ubicacion ubiRespuesta = ubica.buscarUbicacion();
        if (ubiRespuesta != null) {
            System.out.println("respuesta : " + ubiRespuesta.toString());
            Gson gsonToPerson = new Gson();
            String jsonUbicacion = gsonToPerson.toJson(ubiRespuesta);
            System.out.println("json per " + jsonUbicacion);
            return jsonUbicacion;
        } else {
            return "error";
        }
    }

    private String buscarTodosUbicacion(String rutcli) {
        String todos;
        Ubicacion ubica = new Ubicacion(rutcli);
        todos = ubica.getTodosUbicaciones();
        if (todos != null) {
            System.out.println("respuesta");

            return todos;
        } else {
            return todos;
        }
    }
    private String buscarUbicacionesLibres(String rutcli) {
        String todos;
        Ubicacion ubica = new Ubicacion(rutcli);
        todos = ubica.getUbicacionesLibres();
        if (todos != null) {
            System.out.println("respuesta");

            return todos;
        } else {
            return todos;
        }
    }

    private String eliminarUbicacion(int id, String rutcli) {
        Ubicacion ubica = new Ubicacion(id, rutcli);
        int fila = ubica.eliminarUbicacion();
        if (fila != 0) {
            System.out.println("ubicacion eliminado");
            return "eliminado";
        } else {
            System.out.println("ubicacion no eliminado");
            return "error";
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}