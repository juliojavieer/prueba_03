package Servlet;

import Entidades.Detalle_lista_cte;
import Entidades.Lista_salida;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "S_Gestion_Lista_Salida", urlPatterns = {"/S_Gestion_Lista_Salida"})
public class S_Gestion_Lista_Salida extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();


        try {
            String parametro_boton = request.getParameter("parametro_boton");
            System.out.println("parametro_boton: " + parametro_boton);
            HttpSession actual = request.getSession(true);
            String id = actual.getId();
            String rut_cliente = (String) actual.getAttribute("logueado");


            if (parametro_boton.equalsIgnoreCase("boton_buscar_lista_salida")) {
                String fol = request.getParameter("folio");
                int foli = Integer.parseInt(fol);
                String laBusqueda = buscarListaSalida(foli, rut_cliente);
                out.print(laBusqueda);
            } else if (parametro_boton.equalsIgnoreCase("botonBuscarTodasListasSalida")) {

                System.out.println("user: " + rut_cliente);
                String laBusqueda = buscarTodasListasSalida(rut_cliente);
                out.print(laBusqueda);
            } else if (parametro_boton.equalsIgnoreCase("boton_guarda_Lista_Salida")) {
                String elJSON = request.getParameter("jsonList");
                System.out.println(elJSON);
                Gson gson = new Gson();
                Lista_salida jsonLista = gson.fromJson(elJSON, Lista_salida.class);
                System.out.println("jsonLista: " + jsonLista.toString());
                String elJSOND = request.getParameter("jsonDetalle");
                System.out.println(elJSOND);
                Gson gsoncito = new Gson();
                Detalle_lista_cte jsonDetalle = gsoncito.fromJson(elJSOND, Detalle_lista_cte.class);
                System.out.println("jsonDetalle" + jsonDetalle.toString());
                int filasIngresadas = jsonLista.insertarLista_salida();
                int filaIngresada = jsonDetalle.insertarDetalle_lista_cte();
                System.out.println("filaIngresada" + filaIngresada);
                if (filasIngresadas > 0 && filaIngresada > 0) {
                    out.print("<font color='green'>Lista Salida ingresado correctamente.</font>");
                } else {
                    out.print("<font color='red'>Error, Lista Salida no ingresado.</font>");
                }
            }

        } catch (Exception e) {
            out.print("error, no se pudo ingresar la lista");
            System.out.println("error: " + e.getMessage());
        } finally {
            out.close();
        }
    }

    private String buscarListaSalida(int fol, String nombre) {
        Lista_salida lista = new Lista_salida(fol, nombre);
        Lista_salida listRespuesta = lista.buscarLista_salida();
        Detalle_lista_cte detalle = new Detalle_lista_cte(fol);
        String detRespuesta = detalle.getTodosDetalle_lista_cte();
        if (listRespuesta != null && detRespuesta != "") {
            Gson gsonToList = new Gson();
            String jsonList = gsonToList.toJson(listRespuesta);
            System.out.println("jsonList " + jsonList);
            System.out.println("respuesta : " + listRespuesta.toString());

            String json = jsonList + "#" + detRespuesta;
            return json;
        } else {
            return "error";
        }
    }

    private String buscarTodasListasSalida(String rutcli) {
        String todos;
        Lista_salida lista = new Lista_salida(rutcli);
        todos = lista.getTodosLista_salida();
        if (todos != null) {
            System.out.println("respuesta");

            return todos;
        } else {
            return todos;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}