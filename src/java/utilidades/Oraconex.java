package utilidades;

import java.sql.Connection;
import java.sql.DriverManager;

public class Oraconex {
    private Connection oracon = null;
    private String ip;
    private String port;
    private String baseDatos;
    private String url;
    private String user;
    private String pws;
    
    public Oraconex(){
        conectar();
    }
    
    // configura la conexion a Oracle y retorno el objeto conectado
    private Connection conectar(){
        ip="localhost";
        port="1521";
        baseDatos="xe";
        url="jdbc:oracle:thin:@"+ip +":" +port +":" +baseDatos;
        user = "empresariales";
        pws="empresariales";        
        System.out.println("URL: " +url);
        
        try{
            //Inicializar y registrar el Driver
            Class.forName("oracle.jdbc.driver.OracleDriver");
            
            //realizar la conexion
            oracon =DriverManager.getConnection(url,user,pws);
            System.out.println("conectado a Oracle: " +baseDatos);
            return oracon;
        }catch(Exception ex){
            System.out.println("Error de Conexion. Oraconex.conectar()"
                    + " no conectado: \n" 
                    +ex.getMessage());
            return null;
        }
    }//conectar()
    
    public Connection getConexionOracle(){
        return this.oracon;
    }    
    
    public void closeConexionOracle(){
        try {    
            this.oracon.close();
        } catch (Exception ex) {
             System.out.println("Error de desconexion. Oraconex.closeConexionOracle()");
        }        
    }    
}//class