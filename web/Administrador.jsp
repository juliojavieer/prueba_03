<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="utilidades.Oraconex"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    try {
        HttpSession actual = request.getSession(true);
        String id = actual.getId();
        String nombre = (String) actual.getAttribute("logueado");
        String tipo = (String) actual.getAttribute("tipo");
        System.out.println("usuario " + nombre + " tipo "+tipo);
        if (actual.isNew()) {
            response.sendRedirect("index.jsp");
            return;
        }
        if (actual == null) {
            response.sendRedirect("index.jsp");
        } else {
            if (actual.getAttribute("logueado") == null) {
                response.sendRedirect("index.jsp");
            }
            if(tipo.equalsIgnoreCase("user")){
                System.out.println("user");
                response.sendRedirect("Cliente.jsp");
            }
        }
        
        
        
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        try {
            Statement st = oraconex.createStatement();
            String sql = "select rsocial from persona where rut like '" + nombre + "'";
            ResultSet rs = st.executeQuery(sql);
            rs.next();
            String nom = rs.getString(1);
            System.out.println(nom);
            out.print("<div class='codrops-top'><a href=''><strong>Bienvenido : " + nom + "</strong></a>");
            out.print("<span class='right'><a onclick='cerrarSesion()';><strong>Cerrar Sesión</strong></a></span> <div class='clr'></div></div>");
            out.print("<input type='hidden' id='rut_cliente' value='" + nombre + "'>");
        } catch (Exception e) {
        } finally {
            conora.closeConexionOracle();
        }
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
%>
<!DOCTYPE html>
<html>
    <head>
    <a onclick=""></a>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="add/css/demo.css" />
    <link rel="stylesheet" type="text/css" href="add/css/style.css" />
    <link rel="stylesheet" type="text/css" href="add/css/busca_proveedor.css" />
    <script src="add/js/gestionSesiones.js"></script>
    <link rel="stylesheet" href="add/css/colorbox.css" />
    <script type="text/javascript" src="add/js/jquery.min.js"></script>
    <script src="add/js/jquery.colorbox.js"></script>
    <script src="add/js/formulario.js"></script>
    <script src="add/js/GuardaPTD.js"></script>
    <script src="add/js/ValidaRut.js"></script>
    <script src="add/js/ValidaCorreo.js"></script>
    <script src="add/js/ValidarTelefono.js"></script>
    <script src="add/js/ValidaRazonSocial.js"></script>
    <script src="add/js/gestionCliente.js"></script>
    <script src="add/js/gestionBodega.js"></script>
    <script src ="add/js/gestionUbicacion.js"></script>
    <script src ="add/js/gestionEntra_a_ubicacion.js"></script>
    <link rel="stylesheet" href="add/css/stylemenu.css" type="text/css" media="screen"/>
    <script type="text/javascript" src="add/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="add/js/jqueryimagenes.js"></script>
    <title>Administrador</title>

    <script>
        $(document).ready(function() {
            $("#verClientes").click(function(evento) {
                evento.preventDefault();
                $("#principal").load("buscarClientes.jsp");
                console.log("buscarClientes abierto");
            });
        });
        $(document).ready(function() {
            $("#verBodegas").click(function(evento) {
                evento.preventDefault();
                $("#principal").load("buscarBodegas.jsp");
                console.log("buscarBodegas abierto");
            });
        });
    </script>

</head>
<body>

    <!-- La barra de menu -->

    <div class="content">
        <h1 class="title">Deslizate</h1>
        <center>
            <ul id="sdt_menu" class="sdt_menu" style="width: 350px">
                <li>
                    <a href="#">
                        <img src="add/img/2.jpg" alt=""/>
                        <span class="sdt_active"></span>
                        <span class="sdt_wrap">
                            <span class="sdt_link">Ingresar</span>
                            <span class="sdt_descr">Proveedores, etc..</span>
                        </span>
                    </a>
                    <div class="sdt_box">
                        <a href="#agrega_cliente" class='inline'>Cliente</a>
                        <a href="#agrega_bodega" class='inline'>Bodega</a>
                        <a href="#agrega_ubicacion" class='inline'>Ubicacion</a>
                        <a href="#asignar_ubicacion" class='inline'>Asignar Ubicacion</a>

                    </div>
                </li>
                <li>
                    <a href="#">
                        <img src="add/img/1.jpg" alt=""/>
                        <span class="sdt_active"></span>
                        <span class="sdt_wrap">
                            <span class="sdt_link">Buscar</span>
                            <span class="sdt_descr">mis registros</span>
                        </span>
                    </a>
                    <div class="sdt_box">
                        <a id="verBodegas" href="#">Bodegas</a>
                        <a id="verClientes" href="#">Clientes</a>
                        <a href="#">Mis listas</a>
                    </div>
                </li>
                <li style="display: none;"></li>
                
            </ul>
        </center>
    </div>
    <br><br><br><br><br><br>



    <div style='display:none;'>

        <div id='agrega_cliente' style='padding:10px; width: 600px'>
            <p>
                <strong>
                    <form class="contact_form" action="#" method="post" name="contact_form" autocomplete="yes">
                        <center>
                            <h1 class="titulo">Ingresar Cliente</h1>
                            <ul>
                                <li>
                                    <label for="name">Rut:</label>
                                    <input id="rut_pers" onblur="Rut(id);" type="text" placeholder="12.988.829-2" required 
                                           style="padding-left: 30px; background: url('add/img/rut.png') no-repeat 2% center;"/>
                                    <span class="form_hint">rut cliente</span>
                                </li>
                                <li>
                                    <label for="name">Descuento:</label>
                                    <input id="descuento_pers"  type="text" placeholder="ej: 30" required 
                                           style="padding-left: 30px; background: url('add/img/rut.png') no-repeat 2% center;"/>
                                    <span class="form_hint">rut cliente</span>
                                </li>
                                <li>
                                    <label for="name">Clasificacion:</label>
                                    <input id="clasif_pers" type="text" placeholder="bueno o malo" required 
                                           style="padding-left: 30px; background: url('add/img/rut.png') no-repeat 2% center;"/>
                                    <span class="form_hint">rut cliente</span>
                                </li>
                                <li>
                                    <label for="name">Razon Social:</label>
                                    <input onblur="comprobar_nombre(id);" id="nombre_pers" type="text" name="email" placeholder="ej: Juan Ahumada" required 
                                           style="padding-left: 30px; background: url('add/img/nom.png') no-repeat 2% center;"/>
                                    <span class="form_hint">nombre</span>
                                </li>
                                <li>
                                    <label for="name">Dirección:</label>
                                    <input id="direccion_pers" type="text" name="email" placeholder="ej: Los cantares #1212" required 
                                           style="padding-left: 30px; background: url('add/img/dir.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Nombre calle #direccion</span>
                                </li>
                                <li>
                                    <label for="name">Ciudad:</label>
                                    <input id="ciudad_pers" type="text" name="email" placeholder="ej: Temuco" required 
                                           style="padding-left: 30px; background: url('add/img/ciu.png') no-repeat 2% center;"/>
                                    <span class="form_hint">ciudad</span>
                                </li>
                                <li>
                                    <label for="name">Teléfono:</label>
                                    <input onblur="validarTelefono(id);" id="telefono_pers" type="text" placeholder="ej: 9897211" required
                                           style="padding-left: 30px; background: url('add/img/tel.png') no-repeat 2% center;" />
                                    <span class="form_hint">numero</span>
                                </li>
                                <li>
                                    <label for="email">Email:</label>
                                    <input style="padding-left: 30px; background: url('add/img/mai.png') no-repeat 2% center;" id="email_pers" onblur="validaEmail(id);" type="email" name="email" placeholder="ej: elemail@host.com" required />
                                    <span class="form_hint">nombre@host.com</span>
                                </li>
                                <li>
                                    <label for="pass">Pass Usuario Cliente:</label>
                                    <input type="password" id="pass_cli" placeholder="ej: mipassword" required
                                            style="padding-left: 30px; background: url('add/img/mai.png') no-repeat 2% center;"/>
                                    <span class="form_hint">contraseña </span>
                                </li>
                                <h1 class="titulo"></h1>

                            </ul>
                            <div id="div_resp_pers">&nbsp;</div> 
                        </center>
                        <label><button class="bbusca" type="reset">Limpiar</button></label>
                        <label><button class="bbusca" type="button" onclick="guardarCliente();">Registrar cliente</button></label>

                    </form>

                </strong>
            </p>
        </div>
    </div>





    <div style='display:none;'>
        <div id='agrega_bodega' style='padding:10px; width: 500px'>
            <p>
                <strong>
                    <form class="contact_form" action="#" method="post" name="contact_form" autocomplete="yes">
                        <center>
                            <h1 class="titulo">Ingresar Bodega</h1>
                            <ul>
                                <li>
                                    <label for="name">ID Bodega</label>
                                    <input id="id_bodega"  type="text" placeholder="Ej: 00231" required 
                                           style="padding-left: 30px; background: url('add/img/dir.png') no-repeat 2% center;"/>
                                    <span class="form_hint">id</span>
                                </li>
                                <li>
                                    <label for="name">Nombre Bodega</label>
                                    <input id="nombre_bodega"  type="text" placeholder="ej: Bodega X" required 
                                           style="padding-left: 30px; background: url('add/img/dir.png') no-repeat 2% center;"/>
                                    <span class="form_hint">nombre</span>
                                </li>


                                <h1 class="titulo"></h1>

                            </ul>
                            <div id="div_resp_bod">&nbsp;</div> 
                        </center>
                        <label><button class="bbusca" type="reset">Limpiar</button></label>
                        <label><button class="bbusca" id="boton_guarda_bodega" type="button" onclick="guardarBodega();">Registrar Bodega</button></label>

                    </form>

                </strong>
            </p>
        </div>
    </div>


    <div style='display:none;'>
        <div id='agrega_ubicacion' style='padding:10px; width: 600px'>
            <p>
                <strong>
                    <form class="contact_form" action="#" method="post" name="contact_form" autocomplete="yes">
                        <center>
                            <h1 class="titulo">Ingresar Ubicacion</h1>
                            <ul>
                                <li>
                                    <label for="name">Id ubicacion:</label>
                                    <input id="id_ub"  type="text" placeholder="Ej: 3" required 
                                           style="padding-left: 30px; background: url('add/img/dir.png') no-repeat 2% center;"/>
                                    <span class="form_hint">id ubicacion</span>
                                </li>
                                <li>
                                    <label for="name">Pasillo</label>
                                    <input id="pasillo"  type="text" placeholder="Ej: 30" required 
                                           style="padding-left: 30px; background: url('add/img/dir.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Pasillo</span>
                                </li>
                                <li>
                                    <label for="name">Ancho</label>
                                    <input id="ancho" type="text" placeholder="Ej: 20" required 
                                           style="padding-left: 30px; background: url('add/img/regla.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Ancho</span>
                                </li>
                                <li>
                                    <label for="name">Largo:</label>
                                    <input  id="largo" type="text"  placeholder="Ej: 42" required 
                                            style="padding-left: 30px; background: url('add/img/regla.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Largo</span>
                                </li>
                                <li>
                                    <label for="name">Altura</label>
                                    <input id="altura" type="text"  placeholder="Ej :4" required 
                                           style="padding-left: 30px; background: url('add/img/regla.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Altura </span>
                                </li>
                                <li>
                                    <label for="name">Estado</label>
                                    <input id="estado" type="text"  placeholder="1 o 0" required 
                                           style="padding-left: 30px; background: url('add/img/dir.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Libre = 1   Ocupado = 0</span>
                                </li>
                                <li>
                                    <label for="name">id Bodega</label>
                                    <input id="id_bod" type="text" placeholder="Ej: 2" required
                                           style="padding-left: 30px; background: url('add/img/dir.png') no-repeat 2% center;" />
                                    <span class="form_hint">Id Bodega</span>
                                </li>
                                <li>
                                    <label for="name">Rut Cliente</label>
                                    <input id="rut_clien" onblur="Rut(id);" type="text" placeholder="Ej: 18.485.118-0" required
                                           style="padding-left: 30px; background: url('add/img/rut.png') no-repeat 2% center;" />
                                    <span class="form_hint">Rut Cliente</span>
                                </li>

                                <h1 class="titulo"></h1>

                            </ul>
                            <div id="div_resp_ubic">&nbsp;</div> 
                        </center>
                        <label><button class="bbusca" type="reset">Limpiar</button></label>
                        <label><button class="bbusca" type="button" id ="boton_guarda_ubicacion" onclick="guardarUbicacion();">Registrar Ubicacion</button></label>

                    </form>

                </strong>
            </p>
        </div></div>


    <div style='display:none;'>
        <div id='asignar_ubicacion' style='padding:10px; width: 600px'>
            <p>
                <strong>
                    <form class="contact_form" action="#" method="post" name="contact_form" autocomplete="yes">
                        <center>
                            <h1 class="titulo">Asignar ubicacion a entrada</h1>
                            <ul>
                                <li>
                                    <label for="name">Fecha hora ingreso</label>
                                    <input id="fecha_hora_ingreso"  type="text" placeholder="Ej: 2013/10/10" required 
                                           style="padding-left: 30px; background: url('add/img/calend.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Fecha hora ingreso</span>
                                </li>
                                <li>
                                    <label for="name">Codigo Articulo</label>
                                    <input id="codigo_articulo"  type="text" placeholder="Ej: 3" required 
                                           style="padding-left: 30px; background: url('add/img/dir.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Codigo articulo</span>
                                </li>
                                <li>
                                    <label for="name">Fecha retito total</label>
                                    <input id="fecha_retiro_total" type="text" placeholder="Ej: 2012/12/13" required 
                                           style="padding-left: 30px; background: url('add/img/calend.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Fecha retiro total</span>
                                </li>
                                <li>
                                    <label for="name">Id ubicacion</label>
                                    <input  id="id_ubicacion" type="text"  placeholder="Ej: 42" required 
                                            style="padding-left: 30px; background: url('add/img/dir.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Id ubicacion</span>
                                </li>
                                <li>
                                    <label for="name">Cantidad inicial</label>
                                    <input id="cantidad_inicial" type="text"  placeholder="Ej :4" required 
                                           style="padding-left: 30px; background: url('add/img/dir.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Cantidad inicial </span>
                                </li>
                                <li>
                                    <label for="name">Stock</label>
                                    <input id="stock" type="text"  placeholder="Ej: 100" required 
                                           style="padding-left: 30px; background: url('add/img/dir.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Stock</span>
                                </li>
                                <li>
                                    <label for="name">Codigo Entrada</label>
                                    <input id="codigo_entrada" type="text" placeholder="Ej: 2" required
                                           style="padding-left: 30px; background: url('add/img/dir.png') no-repeat 2% center;" />
                                    <span class="form_hint">Id Codigo entrada</span>
                                </li>
                                <h1 class="titulo"></h1>

                            </ul>
                            <div id="div_resp_entra_a_ubic">&nbsp;</div> 
                        </center>
                        <label><button class="bbusca" type="reset">Limpiar</button></label>
                        <label><button class="bbusca" type="button" id ="boton_guarda_entra_a_ubicacion" onclick="guardarEntra_a_ubicacion();">Registrar Ubicacion</button></label>

                    </form>

                </strong>
            </p>
        </div>
    </div>



    <div id="principal"></div>
</body>
</html>