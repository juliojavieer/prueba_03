<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="utilidades.Oraconex"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    try {
        HttpSession actual = request.getSession(true);
        String id = actual.getId();
        String tipo = (String) actual.getAttribute("tipo");
        String nombre = (String) actual.getAttribute("logueado");
        System.out.println("usuario" + nombre);
        if (actual.isNew()) {
            response.sendRedirect("index.jsp");
            return;
        }
        if (actual == null) {
            response.sendRedirect("index.jsp");
        } else {
            if (actual.getAttribute("logueado") == null) {
                response.sendRedirect("index.jsp");
            }
            if(tipo.equalsIgnoreCase("admin")){
                System.out.println("admin");
                response.sendRedirect("Administrador.jsp");
            }
        }
        
        Oraconex conora = new Oraconex();
        Connection oraconex = conora.getConexionOracle();
        try {
            Statement st = oraconex.createStatement();
            String sql = "select rsocial from persona where rut like '" + nombre + "'";
            ResultSet rs = st.executeQuery(sql);
            rs.next();
            String nom = rs.getString(1);
            System.out.println(nom);
            actual.setAttribute("nombre", nom);
            out.print("<div class='codrops-top'><a href=''><strong>Bienvenido : " + nom + "</strong></a>");
            out.print("<span class='right'><a onclick='cerrarSesion()';><strong>Cerrar Sesión</strong></a></span> <div class='clr'></div></div>");
            out.print("<input type='hidden' id='rut_cliente' value='" + nombre + "'>");
        } catch (Exception e) {
        } finally {
            conora.closeConexionOracle();
        }
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
%>
<!DOCTYPE html>
<html>
    <head>
    <a onclick=""></a>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="add/css/demo.css" />
    <link rel="stylesheet" type="text/css" href="add/css/style.css" />
    <link rel="stylesheet" type="text/css" href="add/css/busca_proveedor.css" />
    <script src="add/js/gestionSesiones.js"></script>
    <link rel="stylesheet" href="add/css/colorbox.css" />
    <script type="text/javascript" src="add/js/jquery.min.js"></script>
    <script src="add/js/jquery.colorbox.js"></script>
    <script src="add/js/formulario.js"></script>
    <script src="add/js/GuardaPTD.js"></script>
    <script src="add/js/ValidaRut.js"></script>
    <script src="add/js/ValidaCorreo.js"></script>
    <script src="add/js/ValidarTelefono.js"></script>
    <script src="add/js/ValidaRazonSocial.js"></script>
    <script src="add/js/gestionProveedores.js"></script>
    <script src="add/js/gestionArticulo.js"></script>
    <script src="add/js/gestionTransportistas.js"></script>
    <script src="add/js/gestionDestinatarios.js"></script>
    <script src="add/js/gestionLista_Salida.js"></script>
    <script src="add/js/gestionEntradaMercancia.js"></script>
    <script src="add/js/gestionCamion.js"></script>
    <link rel="stylesheet" href="add/css/stylemenu.css" type="text/css" media="screen"/>
    <script type="text/javascript" src="add/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="add/js/jqueryimagenes.js"></script>
    
    <title>Cliente</title>

    <script>
        $(document).ready(function() {
            $("#verProveedores").click(function(evento) {
                evento.preventDefault();
                $("#principal").load("buscarProveedores.jsp");
                console.log("buscarProveedores abierto");
            });
            $("#verDestinatarios").click(function(evento) {
                evento.preventDefault();
                $("#principal").load("buscarDestinatarios.jsp");
                console.log("buscarProveedores abierto");
            });
            $("#verTransportistas").click(function(evento) {
                evento.preventDefault();
                $("#principal").load("buscarTransportistas.jsp");
                console.log("buscarProveedores abierto");
            });
            $("#verArticulos").click(function(evento) {
                evento.preventDefault();
                $("#principal").load("buscarArticulos.jsp");
                console.log("buscarArticulos abierto");
            });
            $("#verUbicaciones").click(function(evento) {
                evento.preventDefault();
                $("#principal").load("buscarUbicaciones.jsp");
                console.log("buscarUbicaciones abierto");
            });
            $("#verListaSalida").click(function(evento) {
                evento.preventDefault();
                $("#principal").load("buscarListaSalida.jsp");
                console.log("buscaListaSalida abierto");
            });
            $("#verListaEntrada").click(function(evento) {
                evento.preventDefault();
                $("#principal").load("buscarListaEntrada.jsp");
                console.log("buscaListaEntrada abierto");
            });
            
            $("#editarPerfil").click(function(evento) {
                evento.preventDefault();
                $("#principal").load("editarPerfilCliente.jsp");
                console.log("editar Perfil Cliente abierto");
            });
        });
    </script>

</head>
<body>

    <!-- La barra de menu -->

    <div class="content">
        <h1 class="title">Deslizate</h1>
        <center>
            <ul id="sdt_menu" class="sdt_menu">
                <li>
                    <a href="#">
                        <img src="add/img/2.jpg" alt=""/>
                        <span class="sdt_active"></span>
                        <span class="sdt_wrap">
                            <span class="sdt_link">Ingresar</span>
                            <span class="sdt_descr">Proveedores, etc..</span>
                        </span>
                    </a>
                    <div class="sdt_box">
                        <a href="#inline_content" class='inline' onclick="bloquearInputs(contact_form);">Persona</a>
                        <a href="#lista_de_entrada" class="inline" onclick="poneFecha();">Lista entrada</a>
                        <a href="#lista_de_salida" class="inline">Lista salida</a>
                        <a href="#camion" class="inline">Camión</a>
                        <a href="#agrega_articulo" class="inline">Artículos</a>
                    </div>
                </li>
                <li>
                    <a href="#">
                        <img src="add/img/1.jpg" alt=""/>
                        <span class="sdt_active"></span>
                        <span class="sdt_wrap">
                            <span class="sdt_link">Buscar</span>
                            <span class="sdt_descr">mis registros</span>
                        </span>
                    </a>
                    <div class="sdt_box">
                        <a id="verUbicaciones" href="#">Mis ubicaciones</a>
                        <a id="verProveedores" href="#">Mis proveedores</a>
                        <a id="verDestinatarios" href="#">Mis destinatarios</a>
                        <a id="verTransportistas" href="#">Mis transportistas</a>
                        <a id="verArticulos" href="#">Articulos</a>
                    </div>
                </li>
               
                <li>
                    <a href="#">
                        <img src="add/img/1.jpg" alt=""/>
                        <span class="sdt_active"></span>
                        <span class="sdt_wrap">
                            <span class="sdt_link">Mis Listas</span>
                            <span class="sdt_descr">entrada, salida..</span>
                        </span>
                    </a>
                    <div class="sdt_box">
                        <a id ="verListaSalida"href="#">Listas Salida</a>
                        <a id ="verListaEntrada"href="#">Listas Entrada</a>
                    </div>
                </li>
                
                <li>
                    <a href="#">
                        <img src="add/img/1.jpg" alt=""/>
                        <span class="sdt_active"></span>
                        <span class="sdt_wrap">
                            <span class="sdt_link">Mi Perfil</span>
                            <span class="sdt_descr">mis datos, perfil</span>
                        </span>
                    </a>
                    <div class="sdt_box">
                        <a id="editarPerfil" href="#">Editar perfil</a>
                    </div>
                </li>
                <li style="display: none;">
                </li>

            </ul>
        </center>
    </div>
    <br><br><br><br><br><br>



    <div style='display:none;'>
        <div id='inline_content' style='padding:10px; width: 600px'>
            <p>
                <strong>
                    <form id="contact_form" class="contact_form" action="#" method="post" name="contact_form" autocomplete="yes">
                        <center>
                            <h1 class="titulo">Ingresar Datos</h1>
                            <ul>
                                <li>
                                    <label for="name">Tipo</label>
                                    <div class="seleccion">
                                        <select id="select_id">
                                            <option selected="selected" onclick="bloquearInputs(contact_form);">Seleccione...</option>>
                                            <option onclick="desbloquearInputs(contact_form);">Ingresar Proveedor</option>
                                            <option onclick="desbloquearInputs(contact_form);">Ingresar Destinatario</option>
                                            <option onclick="desbloquearInputs(contact_form);">Ingresar Transportista</option>
                                        </select>
                                    </div>
                                </li>
                                <li>
                                    <label for="name">Rut:</label>
                                    <input id="rut_pers" onblur="Rut(id);" type="text" placeholder="12.988.829-2" required 
                                           style="padding-left: 30px; background: url('add/img/rut.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Tu Rut</span>
                                </li>
                                <li>
                                    <label for="name">Razon Social:</label>
                                    <input onblur="comprobar_nombre(id);" id="nombre_pers" type="text" name="email" placeholder="Julio javier" required 
                                           style="padding-left: 30px; background: url('add/img/nom.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Nombre</span>
                                </li>
                                <li>
                                    <label for="name">Dirección:</label>
                                    <input id="direccion_pers" type="text" name="email" placeholder="Los cantares #1212" required 
                                           style="padding-left: 30px; background: url('add/img/dir.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Nombre calle #direccion</span>
                                </li>
                                <li>
                                    <label for="name">Ciudad:</label>
                                    <input id="ciudad_pers" type="text" name="email" placeholder="Temuco" required 
                                           style="padding-left: 30px; background: url('add/img/ciu.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Ejemplo Santiago</span>
                                </li>
                                <li>
                                    <label for="name">Teléfono:</label>
                                    <input onblur="validarTelefono(id);" id="telefono_pers" type="text" placeholder="9897211" required
                                           style="padding-left: 30px; background: url('add/img/tel.png') no-repeat 2% center;" />
                                    <span class="form_hint">Ejemplo 1243121</span>
                                </li>
                                <li>
                                    <label for="email">Email:</label>
                                    <input style="padding-left: 30px; background: url('add/img/mai.png') no-repeat 2% center;" id="email_pers" onblur="validaEmail(id);" type="email" name="email" placeholder="tuemail@tuhost.com" required />
                                    <span class="form_hint">Ejemplo nombre@host.com</span>
                                </li>
                                <h1 class="titulo"></h1>

                            </ul>
                            <div id="div_resp_pers">&nbsp;</div> 
                        </center>
                        <label><button class="bbusca" type="reset">Limpiar</button></label>
                        <label><button class="bbusca" type="button" onclick="guardarCampos();">Registrar</button></label>

                    </form>

                </strong>
            </p>
        </div>
    </div>


    <div style="display: none; height: 800px;">
        <div id='lista_de_entrada' style='padding:10px; width: 600px;height: 500px;'>
            <p>
                <strong>
                    <form class="contact_form" action="#" method="post" autocomplete="yes">
                        <center>
                            <h1 class="titulo">Ingresar datos de entrada</h1>
                            <ul>
                                <li>
                                    <label for="name">Código entrada</label>
                                    <input placeholder="id para entrada" id="codigo_lista_entrada" type="text"
                                           style="padding-left: 30px; background: url('add/img/date.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Solo números</span>
                                </li>

                                <li>
                                    <label for="name">Fecha</label>
                                    <input readonly="readonly" id="fecha_entrada" type="text"
                                           style="padding-left: 30px; background: url('add/img/date.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Fecha de hoy</span>
                                </li>

                                <li>
                                    <label for="name">Patente camión</label>
                                    <input placeholder="patente del camion" id="patente_camion_entrada" type="text"
                                           style="padding-left: 30px; background: url('add/img/date.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Patente</span>
                                </li>

                                <li>
                                    <div class="datagrid">
                                        <table style="width: 300px" >
                                            <thead>
                                                <tr>
                                                    <th>Artículo</th>
                                                    <th>Cantidad</th>
                                                    <th><a href="#"><img src="add/img/agregar.png" onclick="agregaFila('tabla_entrada');"></th>
                                                            </tr>
                                                            </thead>

                                                            <tbody id="tabla_entrada">
                                                                <tr>
                                                                    <td><input class="texto" type="text" style="width: 150px;"/></td>
                                                                    <td><input class="texto" type="text" style="width: 150px"/></td>          
                                                                    <td><a href="#"><img src="add/img/elim.png"></a></td>
                                                                </tr>
                                                            </tbody>
                                        </table>
                                    </div>
                                </li>
                            </ul>
                            <div id="div_resp_listaEntrada"></div>
                            <input type="button" class="bbusca" id="guarda_lista_entrada" onclick="guardarEntradaMercancia();" value="Guardar lista" >

                        </center>
                    </form>
                </strong>
            </p>
        </div>
    </div>

    <div style='display:none;'>
        <div id='lista_de_salida' style='padding:10px; width: 600px'>
            <p>
                <strong>
                    <form class="contact_form" action="#" method="post" autocomplete="yes">
                        <center>
                            <h1 class="titulo">Ingresar Datos</h1>
                            <ul>

                                <li>
                                    <label for="name">folio:</label>
                                    <input id="folio"  type="text" placeholder="123" required 
                                           style="padding-left: 30px; background: url('add/img/dir.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Folio</span>
                                </li>
                                <li>
                                    <label for="name">Fecha</label>
                                    <input id="fecha" type="date" name="fecha" placeholder="01/01/2013" required 
                                           style="padding-left: 30px; background: url('add/img/calend.png') no-repeat 2% center;"/>
                                    <span class="form_hint">dia/mes/año</span>
                                </li>
                                <li>
                                    <label for="name">Cantidad</label>
                                    <input id="cantidad" type="text" name="cantidad" placeholder="2" required 
                                           style="padding-left: 30px; background: url('add/img/dir.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Ejemplo 2</span>
                                </li>
                                <li>
                                    <label for="name">Codigo Articulo</label>
                                    <input id="codigo_articulo" type="text" name="fecha" placeholder="234" required 
                                           style="padding-left: 30px; background: url('add/img/dir.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Ejemplo 234</span>
                                </li>
                                <li>
                                    <label for="name">Codigo entrada</label>
                                    <input id="cod_entrada"  type="text" placeholder="213" required 
                                           style="padding-left: 30px; background: url('add/img/dir.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Ej 213</span>
                                </li>
                                <li>
                                    <label for="name">Rut Destinatario</label>
                                    <input id="rut_des" onblur="Rut(id);" type="text" placeholder="17.590.684-3" required 
                                           style="padding-left: 30px; background: url('add/img/rut.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Rut Destinatario</span>
                                </li>
                                <h1 class="titulo"></h1>

                            </ul>
                            <div id="div_resp_listaSalida">&nbsp;</div> 
                        </center>
                        <label><button class="bbusca" type="reset">Limpiar</button></label>
                        <label><button class="bbusca" id ="boton_guarda_Lista_Salida "type="button" onclick="guardarListaSalida();">Registrar</button></label>

                    </form>

                </strong>
            </p>
        </div>
    </div>
    <div style='display:none;'>
        <div id='camion' style='padding:10px; width: 500px'>
            <p>
                <strong>
                    <form class="contact_form" action="#" method="post" autocomplete="yes">
                        <center>
                            <h1 class="titulo">Asignar Camion</h1>
                            <ul>

                                <li>
                                    <label for="name">Patente</label>
                                    <input id="patente"  type="text" name="patente"  placeholder="BBAA31" required 
                                           style="padding-left: 30px; background: url('add/img/camion.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Patente</span>
                                </li>
                                <li>
                                    <label for="name">Marca</label>
                                    <input id="marca" type="text" name="marca" placeholder="Kenworth" required 
                                           style="padding-left: 30px; background: url('add/img/camion.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Marca</span>
                                </li>
                                <li>
                                    <label for="name">Modelo</label>
                                    <input id="modelo" type="text" name="modelo" placeholder="T880" required 
                                           style="padding-left: 30px; background: url('add/img/camion.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Modelo</span>
                                </li>
                                <li>
                                    <label for="name">Año</label>
                                    <input id="year" type="text" name="year" placeholder="2013" required 
                                           style="padding-left: 30px; background: url('add/img/dir.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Ejemplo 2013</span>
                                </li>
                                <li>
                                    <label for="name">Rut Propietario</label>
                                    <input id="rut_propi" onblur="Rut(id);" name ="rut_propi" type="text" placeholder="17.590.684-3" required 
                                           style="padding-left: 30px; background: url('add/img/rut.png') no-repeat 2% center;"/>
                                    <span class="form_hint">Rut Propietario</span>
                                </li>
                                <h1 class="titulo"></h1>

                            </ul>
                            <div id="div_resp_Camion">&nbsp;</div> 
                        </center>
                        <label><button class="bbusca" type="reset">Limpiar</button></label>
                        <label><button class="bbusca" id ="boton_guarda_camion "type="button" onclick="guardarCamion();">Registrar</button></label>

                    </form>

                </strong>
            </p>
        </div>
    </div>



    <div style='display:none;'>
        <div id='agrega_articulo' style='height: 500px; width: 1100px'>
            <p>
                <strong>
                    <form action="#" method="post" autocomplete="yes">
                        <center>
                            <br><br>
                            <h1 class="titulo">Insertar Articulos</h1>
                            <ul>
                                <li>
                                    <div class="datagrid">
                                        <table>
                                            <thead>
                                                <tr>
                                                    <th>cod</th>
                                                    <th>rut_pvd</th>
                                                    <th>descrip</th>
                                                    <th>largo</th>
                                                    <th>ancho</th>
                                                    <th>alto</th>
                                                    <th>precio</th>
                                                    <th>est aseg</th>
                                                    <th>id clas</th>
                                                    <th><a href="#"><img src="add/img/agregar.png" onclick="agregaFilaArticulo('tabla_articulos');"></th>
                                                            </tr>
                                                            </thead>

                                                            <tbody id="tabla_articulos">
                                                                <tr>
                                                                    <td><input class="bbusca" type="text" style="width: 100px;"/></td>
                                                                    <td><input class="bbusca" type="text" style="width: 150px;"/></td>
                                                                    <td><input class="bbusca" type="text" style="width: 150px;"/></td>
                                                                    <td><input class="bbusca" type="text" style="width: 70px;"/></td>
                                                                    <td><input class="bbusca" type="text" style="width: 70px;"/></td>
                                                                    <td><input class="bbusca" type="text" style="width: 70px;"/></td>
                                                                    <td><input class="bbusca" type="text" style="width: 70px;"/></td>
                                                                    <td><input class="bbusca" type="text" style="width: 60px;"/></td>
                                                                    <td><input class="bbusca" type="text" style="width: 60px;"/></td>
                                                                    <td style="width: 30px;"><a href="#"><img src="add/img/elim.png"></a></td>
                                                                </tr>
                                                            </tbody>
                                        </table>
                                    </div>
                                </li>
                                <h1 class="titulo"></h1>

                            </ul>
                            <div id="div_resp_art">&nbsp;</div> 

                            <label><button class="bbusca" style="width: 300px" type="reset">Limpiar</button></label>
                            <label><button class="bbusca" style="width: 300px" id ="boton_guarda_articulo "type="button" onclick="guardarArticulos();">Guardar</button></label>
                        </center>
                    </form>

                </strong>
            </p>
        </div>
    </div>



    <div id="principal" style='padding:10px;'>
    </div>
</body>
</html>