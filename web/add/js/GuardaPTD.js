function guardarCampos() {

    var rut = "rut_pers";
    var rso = "nombre_pers";
    var dir = "direccion_pers";
    var ema = "email_pers";
    var tel = "telefono_pers";
    var ciu = "ciudad_pers";

    if (Rut(rut) && comprobar_nombre(rso) && validaEmail(ema) && validarTelefono(tel) && dir.length > 3 && ciu.length > 3) {
        console.log("datos correctos");
        rut1 = document.getElementById(rut).value;
        rcli = document.getElementById("rut_cliente").value;
        rso1 = document.getElementById(rso).value;
        dir1 = document.getElementById(dir).value;
        ema1 = document.getElementById(ema).value;
        tel1 = document.getElementById(tel).value;
        ciu1 = document.getElementById(ciu).value;
        console.log("rut_cliente: "+rcli);

        if (document.getElementById("select_id").value === "Ingresar Proveedor") {
            var jsonPro = encodeURIComponent(
                    '{"rut_pro":"' + rut1
                    + '","rut_cliente":"' + rcli
                    + '","rsocial":"' + rso1
                    + '","direccion":"' + dir1
                    + '","ciudad":"' + ciu1
                    + '","telefono":"' + tel1
                    + '","email":"' + ema1 + '"}');
            console.log("Ingresa Proveedor");
            guardarProveedor(jsonPro);
        }

        if (document.getElementById("select_id").value === "Ingresar Transportista") {
            console.log("Ingresa Transportista");
            var jsonPro = encodeURIComponent(
                    '{"rut_trans":"' + rut1
                    + '","rut_cliente":"' + rcli
                    + '","rsocial":"' + rso1
                    + '","direccion":"' + dir1
                    + '","ciudad":"' + ciu1
                    + '","telefono":"' + tel1
                    + '","email":"' + ema1 + '"}');
            guardarTransportista(jsonPro);
        }


        if (document.getElementById("select_id").value === "Ingresar Destinatario") {
            console.log("Ingresa Destinatario");
            var jsonPro = encodeURIComponent(
                    '{"rut_dest":"' + rut1
                    + '","rut_cliente":"' + rcli
                    + '","rsocial":"' + rso1
                    + '","direccion":"' + dir1
                    + '","ciudad":"' + ciu1
                    + '","telefono":"' + tel1
                    + '","email":"' + ema1 + '"}');
            guardarDestinatario(jsonPro);
        }
        return true;
    }
    else {
        console.log("datos incorrectos");
        document.getElementById("div_resp_pers").innerHTML = "Datos incompletos o incorrectos";
    }
}