var READY_STATE_COMPLETE = 4;
var peticion_http;


function verSeleArticulos() {
    jQuery.fn.getCheckboxValues = function() {
        var values = [];
        var i = 0;
        this.each(function() {
            values[i++] = $(this).val();
            eliminarArticulo($(this).val());
        });
        console.log(values);
        return values;
    };
    var arr = $("input:checked").getCheckboxValues();
}

function agregaFilaArticulo(id) {
    var fila = '<tr><td><input class="bbusca" type="text" style="width: 100px;"/></td><td><input class="bbusca" type="text" style="width: 150px;"/></td>';
    fila += '<td><input class="bbusca" type="text" style="width: 150px;"/></td><td><input class="bbusca" type="text" style="width: 70px;"/></td>';
    fila += '<td><input class="bbusca" type="text" style="width: 70px;"/></td><td><input class="bbusca" type="text" style="width: 70px;"/></td>';
    fila += '<td><input class="bbusca" type="text" style="width: 70px;"/></td><td><input class="bbusca" type="text" style="width: 60px;"/></td>';
    fila += '<td><input class="bbusca" type="text" style="width: 60px;"/></td><td style="width: 30px;"><a href="#"><img src="add/img/elim.png"></a></td></tr>';
    document.getElementById(id).innerHTML += fila;
}

function guardarArticulos() {
    var tbl = document.getElementById("tabla_articulos");
    var tds = tbl.getElementsByTagName("input");
    for (i = 0; i < tds.length; i += 9) {
        if(tds[i].value.length>0&&tds[i+1].value.length>0&&tds[i+2].value.length>0&&tds[i+3].value.length>0&&
            tds[i+4].value.length>0&&tds[i+5].value.length>0&&tds[i+6].value.length>0&&
                tds[i+7].value.length>0&&tds[i+8].value.length>0
        )
        var jsonArt = encodeURIComponent(
                '{"codigo_articulo":"' + tds[i].value
                + '","rut_pvd":"' + tds[i + 1].value
                + '","descripcion":"' + tds[i + 2].value
                + '","largo":"' + tds[i + 3].value
                + '","ancho":"' + tds[i + 4].value
                + '","alto":"' + tds[i + 5].value
                + '","precio":"' + tds[i + 6].value
                + '","estado_asegurado":"' + tds[i + 7].value
                + '","id_clasificacion":"' + tds[i + 8].value + '"}');
        
        guardarArticulo(jsonArt);
    }
}

function guardarArticulo(jsonArt){
    peticion_http = inicializa_objeto_ajax();

    var datos = "parametro_boton=boton_guarda_articulo&jsonArt=" +jsonArt;
 
    peticion_http.onreadystatechange = procesaGuardarArticulo;

    peticion_http.open("POST", "/Prueba03/S_GestionArticulo", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');    
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaGuardarArticulo() {
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        document.getElementById("div_resp_art").innerHTML = "<img src='add/img/loading1.gif'>";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                document.getElementById("div_resp_art").innerHTML = peticion_http.responseText;
            }
        } else {
            document.getElementById("div_resp_art").innerHTML = "proceso erroneo";
        }
    }
    return;
}


function buscarTodosArt() {
    peticion_http = inicializa_objeto_ajax();
    var rpro = document.getElementById('buscarutproveedor').value;
    console.log(rpro);
    var datos = "parametro_boton=botonBuscarTodosArticulos&parametro_rut_pro=" + rpro;
    console.log("datos: " + datos);

    console.log("antes onready");
    peticion_http.onreadystatechange = procesaBuscarTodosArt;
    console.log("despues onready");

    peticion_http.open("POST", "/Prueba03/S_GestionArticulo", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaBuscarTodosArt() {
    console.log("en  procesaRespuestaTodosArticulo()");
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        console.log("readyState: " + peticion_http.readyState);
        document.getElementById("tabla_muestra_articulo").innerHTML = "<img src='add/img/loading1.gif'>";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta = peticion_http.responseText;
                if (respuesta !== "error") {
                    console.log("resp " + respuesta);
                    var datosArt = respuesta.split("|");
                    var table = "<thead><tr><th>Código</th><th>Rut pvd</th><th>Descripción</th><th>Largo</th><th>Ancho</th><th>Alto</th><th>Precio</th><th>Asegurado</th><th>Id clas</th><th><button class='buttonconfirm' onclick='javascript:openDialog();'>Eliminar</button></tr></thead><tbody>";
                    for(i=0;i<datosArt.length;i++){
                        var ele=datosArt[i].split(",");
                        table += "<tr><td>" + ele[0] + "</td><td>"+ ele[1] + "</td><td>"+ele[2]+"</td><td>"+ele[3]+"</td><td>"+ele[4]+"</td><td>"+ele[5]+"</td><td>"+ele[6]+"</td><td>"+ele[7]+"</td><td>"+ele[8]+"</td><td><input type='checkbox' value='" + ele[0]+ "' /></td></tr></tbody>";
                    }
                    table += "</tbody>";
                
                }
                else {
                    table = "No se encontró artículo";
                }
                document.getElementById("tabla_muestra_articulo").innerHTML = table;
            }
        } else {
            document.getElementById("tabla_muestra_articulo").innerHTML = "proceso erroneo";
        }
    }
    return;
}


function eliminarArticulo(id_art) {
    console.log(id_art);
    peticion_http = inicializa_objeto_ajax();
    var datos = "parametro_boton=botonEliminarArticulo&parametro_id=" + id_art;
    console.log("datos: " + datos);

    console.log("antes onready");
    peticion_http.onreadystatechange = procesaEliminarArticulo;
    console.log("despues onready");

    peticion_http.open("POST", "/Prueba03/S_GestionArticulo", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;

}
function procesaEliminarArticulo() {
    console.log("en  procesaEliminarArticulo()");
    console.log("peticion_http.readyState: " + peticion_http.readyState);
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        console.log("readyState: " + peticion_http.readyState);
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta = peticion_http.responseText;
                if (respuesta !== "error") {
                }
                else {
                }
                document.getElementById("div_muestra_articulos_eliminados").innerHTML=respuesta;
                
            }
        } else {
        }
    }
    return;
}