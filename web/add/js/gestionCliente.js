var READY_STATE_COMPLETE = 4;
var peticion_http;

function verSeleClientes() {
    jQuery.fn.getCheckboxValues = function() {
        var values = [];
        var i = 0;
        this.each(function() {
            values[i++] = $(this).val();
            eliminarClientes($(this).val());
        });
        console.log(values);
        return values;
    };
    var arr = $("input:checked").getCheckboxValues();
}



function eliminarClientes(id) {
    console.log(id);
    peticion_http = inicializa_objeto_ajax();
    var rut_cliente = id;
    console.log(rut_cliente);
    var datos = "parametro_boton=botonEliminarClientes&parametro_rut=" + rut_cliente;
    console.log("datos: " + datos);

    console.log("antes onready");
    peticion_http.onreadystatechange = procesaEliminarCliente;
    console.log("despues onready");

    peticion_http.open("POST", "/Prueba03/S_GestionCliente", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;

}
function procesaEliminarCliente() {
    console.log("en  procesaEliminarCliente()");
    console.log("peticion_http.readyState: " + peticion_http.readyState);
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        console.log("readyState: " + peticion_http.readyState);

    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta = peticion_http.responseText;
                if (respuesta !== "error") {
                    console.log("resp " + respuesta);

                }
                else {
                }
                document.getElementById("div_elimina_cliente").innerHTML = respuesta;
            }
        } else {
        }
    }
    return;
}


function buscarCliente() {
    peticion_http = inicializa_objeto_ajax();
    var rut_proveedor = document.getElementById('buscarutcliente').value;
    console.log(rut_proveedor);
    var datos = "parametro_boton=botonBuscarCliente&parametro_rut=" + rut_proveedor;
    console.log("datos: " + datos);

    console.log("antes onready");
    peticion_http.onreadystatechange = procesaBuscarCliente;
    console.log("despues onready");

    peticion_http.open("POST", "/Prueba03/S_GestionCliente", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaBuscarCliente() {
    console.log("en  procesaRespuestaCliente()");
    console.log("peticion_http.readyState: " + peticion_http.readyState);
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        console.log("readyState: " + peticion_http.readyState);
        document.getElementById("tabla_muestra_cliente").innerHTML = "<img src='add/img/loading1.gif'>";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta = peticion_http.responseText;
                if (respuesta !== "error") {
                    console.log("resp " + respuesta);
                    var datosArt = eval("(" + respuesta + ")");
                    var table = "\n\
                <thead><tr><th>Rut</th><th>Desc</th><th>Clasificación</th><th>Razón social</th><th>Dirección</th><th>Ciudad</th><th>Teléfono</th><th>Correo</th><th><button class='buttonconfirm' onclick='javascript:openDialog();'>Eliminar</button></tr></thead><tbody>\n\
                <tr><td  onclick='modCliente(0);'>" + datosArt.rut_cliente + "</td><td>" + datosArt.descuento + "</td><td>" + datosArt.clasificacion + "</td><td>" + datosArt.rsocial + "</td><td>" + datosArt.direccion + "</td>\n\
                <td>" + datosArt.ciudad + "</td><td>" + datosArt.telefono + "</td><td>" + datosArt.email + "</td><td><input type='checkbox' value='" + datosArt.rut + "' /></td></tr></tbody>";
                }
                else {
                    table = "No se encontró cliente";
                }
                document.getElementById("tabla_muestra_cliente").innerHTML = table;
            }
        } else {
            document.getElementById("tabla_muestra_cliente").innerHTML = "proceso erroneo";
        }
    }
    return;
}



function buscarTodosCli() {
    peticion_http = inicializa_objeto_ajax();
    var datos = "parametro_boton=boton_buscar_todos_cli";
    console.log("datos: " + datos);

    console.log("antes onready");
    peticion_http.onreadystatechange = procesaBuscarTodosCli;
    console.log("despues onready");

    peticion_http.open("POST", "/Prueba03/S_GestionCliente", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaBuscarTodosCli() {
    console.log("en  procesaRespuestaTodosCli()");
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) {
        console.log("readyState: " + peticion_http.readyState);
        document.getElementById("tabla_muestra_cliente").innerHTML = "<img src='add/img/loading1.gif'>";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta = peticion_http.responseText;
                if (respuesta !== "error") {
                    console.log("resp " + respuesta);
                    var datosArt = respuesta.split("|");
                    var table = "<thead><tr><th>Rut</th><th>Desc</th><th>Clasificación</th><th>Razón social</th><th>Dirección</th><th>Ciudad</th><th>Teléfono</th><th>Correo</th><th><button class='buttonconfirm' onclick='javascript:openDialog();'>Eliminar</button></th></tr></thead><tbody>";
                    document.getElementById("tabla_muestra_cliente").innerHTML = table;
                    for (i = 0; i < datosArt.length; i++) {
                        var ele = datosArt[i].split(",");
                        table = "<tr><td  onclick='modCliente(" + i + ");'>" + ele[0] + "</td><td>" + ele[1] + "</td><td>" + ele[2] + "</td>\n\
                <td>" + ele[3] + "</td><td>" + ele[4] + "</td><td>" + ele[5] + "</td><td>" + ele[6] + "</td><td>" + ele[7] + "</td><td><input type='checkbox' value='" + ele[0] + "' /></td></tr>";
                        document.getElementById("tabla_muestra_cliente").innerHTML += table;
                    }
                    document.getElementById("tabla_muestra_cliente").innerHTML += "</tbody>";
                }
                else {
                    document.getElementById("tabla_muestra_cliente").innerHTML = "No se encontraron clientes";
                }
            }
        } else {
            document.getElementById("tabla_muestra_cliente").innerHTML = "proceso erroneo";
        }
    }
    return;
}


function actualizarCliente() {
    peticion_http = inicializa_objeto_ajax();
    console.log("en actualizarCliente()");
    var rut = document.getElementById("rut_act").value;
    console.log(rut);
    var des = document.getElementById("descuento_act").value;
    console.log(des);
    var cla = document.getElementById("clasificacion_act").value;
    console.log(cla);

    var jsonCli = encodeURIComponent(
            '{"rut_cliente":"' + rut
            + '","descuento":"' + des
            + '","clasificacion":"' + cla + '"}');

    var datos = "parametro_boton=boton_actualiza_cliente&jsonCli=" + jsonCli;

    peticion_http.onreadystatechange = procesaActualizarCliente;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()

    peticion_http.open("POST", "/Prueba03/S_GestionCliente", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaActualizarCliente() {
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        document.getElementById("div_respuesta_actualiza_proveedor").innerHTML = "<img src='add/img/loading1.gif'>";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                document.getElementById("div_respuesta_actualiza_cliente").innerHTML = peticion_http.responseText;
            }
        } else {
            document.getElementById("div_respuesta_actualiza_cliente").innerHTML = "proceso erroneo";
        }
    }
    return;
}



function guardarCliente() {
    peticion_http = inicializa_objeto_ajax();

    var rut = "rut_pers";
    var cla = "clasif_pers";
    var des = "descuento_pers";
    var rso = "nombre_pers";
    var dir = "direccion_pers";
    var ema = "email_pers";
    var tel = "telefono_pers";
    var ciu = "ciudad_pers";
    var pasw = "pass_cli";

    if (Rut(rut) && comprobar_nombre(rso) && validaEmail(ema) && validarTelefono(tel) && dir.length > 3 && ciu.length > 3) {
        console.log("datos correctos");
        rut1 = document.getElementById(rut).value;
        des1 = document.getElementById(des).value;
        cla1 = document.getElementById(cla).value;
        rso1 = document.getElementById(rso).value;
        dir1 = document.getElementById(dir).value;
        ema1 = document.getElementById(ema).value;
        tel1 = document.getElementById(tel).value;
        ciu1 = document.getElementById(ciu).value;
        pass = document.getElementById(pasw).value;
        console.log("rut_cliente: ");

        var jsonPro = encodeURIComponent(
                '{"rut_cliente":"' + rut1
                + '","descuento":"' + des1
                + '","clasificacion":"' + cla1
                + '","rsocial":"' + rso1
                + '","direccion":"' + dir1
                + '","ciudad":"' + ciu1
                + '","telefono":"' + tel1
                + '","email":"' + ema1 + '"}');
        console.log("Ingresa Cliente");

        var jsonUsr = encodeURIComponent(
                '{"usuario":"' + rut1
                + '","pass":"' + pass + '"}');

        var datos = "parametro_boton=boton_guarda_cliente&jsonCli=" + jsonPro + "&jsonUsr=" + jsonUsr;

        peticion_http.onreadystatechange = procesaGuardarCliente;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()

        peticion_http.open("POST", "/Prueba03/S_GestionCliente", true);
        peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
        peticion_http.setRequestHeader("Content-length", datos.length);
        peticion_http.setRequestHeader("Connection", "close");
        peticion_http.send(datos);

        return;
    } else {
        alert("datos incorrectos");
    }


}

function procesaGuardarCliente() {
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        document.getElementById("div_resp_pers").innerHTML = "<img src='add/img/loading1.gif'>";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                document.getElementById("div_resp_pers").innerHTML = peticion_http.responseText;
            }
        } else {
            document.getElementById("div_resp_pers").innerHTML = "proceso erroneo";
        }
    }
    return;
}


function actualizarPersonaCliente() {
    peticion_http = inicializa_objeto_ajax();
    console.log("en actualizarDestinatario()");
    var rut1 = document.getElementById("rut_act").value;
    console.log(rut1);
    var rso1 = document.getElementById("rsocial_act").value;
    console.log(rso1);
    var dir1 = document.getElementById("direccion_act").value;
    console.log(dir1);
    var ema1 = document.getElementById("correo_act").value;
    console.log(ema1);
    var tel1 = document.getElementById("telefono_act").value;
    console.log(tel1);
    var ciu1 = document.getElementById("ciudad_act").value;
    console.log(ciu1);
    
    var jsonPer = encodeURIComponent(
                    '{"rut":"' + rut1
                    + '","rsocial":"' + rso1
                    + '","direccion":"' + dir1
                    + '","ciudad":"' + ciu1
                    + '","telefono":"' + tel1
                    + '","email":"' + ema1 + '"}');
            
    var datos = "parametro_boton=boton_actualiza_persona_cliente&jsonPer=" + jsonPer;

    peticion_http.onreadystatechange = procesaActualizarPersonaCliente;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()

    peticion_http.open("POST", "/Prueba03/S_GestionCliente", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaActualizarPersonaCliente() {
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        document.getElementById("div_resp_busca_cliente").innerHTML = "<img src='add/img/loading1.gif'>";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                document.getElementById("div_resp_busca_cliente").innerHTML = peticion_http.responseText;
            }
        } else {
            document.getElementById("div_resp_busca_cliente").innerHTML = "proceso erroneo";
        }
    }
    return;
}


function completaDatosCliente() {
    peticion_http = inicializa_objeto_ajax();
    var datos = "parametro_boton=boton_completa_datos_cliente&parametro_rut=" + document.getElementById("rut_cliente").value;

    peticion_http.onreadystatechange = procesaCompletaDatosCliente;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()

    peticion_http.open("POST", "/Prueba03/S_GestionCliente", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;

}

function procesaCompletaDatosCliente() {
    console.log("en  procesaCompletaDatosCliente()");
    console.log("peticion_http.readyState: " + peticion_http.readyState);
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        console.log("readyState: " + peticion_http.readyState);
        document.getElementById("div_resp_busca_cliente").innerHTML = "<img src='add/img/loading1.gif'>";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta = peticion_http.responseText;
                if (respuesta !== "error") {
                    console.log("resp " + respuesta);
                    var datosCli = eval("(" + respuesta + ")");
                    document.getElementById("rut_act").value = datosCli.rut_cliente;
                    document.getElementById("rsocial_act").value = datosCli.rsocial;
                    document.getElementById("direccion_act").value = datosCli.direccion;
                    document.getElementById("ciudad_act").value = datosCli.ciudad;
                    document.getElementById("telefono_act").value =  datosCli.telefono;
                    document.getElementById("correo_act").value = datosCli.email;
                    document.getElementById("div_resp_busca_cliente").innerHTML = "&nbsp";
                }
                else {
                }
            }
        } else {
            document.getElementById("div_resp_busca_cliente").innerHTML = "proceso erroneo";
        }
    }
    return;
}


function completarModificarCliente(indice) {
    var tbl = document.getElementById("tabla_muestra_cliente");
    var as = tbl.getElementsByTagName("td");
    var a = as[indice * 9].innerHTML;
    document.getElementById("rut_act").value = a;

    var a = as[(indice * 9) + 1].innerHTML;
    document.getElementById("descuento_act").value = a;

    var a = as[(indice * 9) + 2].innerHTML;
    document.getElementById("clasificacion_act").value = a;

    document.getElementById("div_respuesta_actualiza_cliente").innerHTML = "&nbsp;";

}