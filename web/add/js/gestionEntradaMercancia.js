var READY_STATE_COMPLETE = 4;
var peticion_http;
var cod_ent;

function poneFecha() {
    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    if (month < 10)
        month = '0' + month;
    if (day < 10)
        day = '0' + day;
    var today = year + '/' + month + '/' + day;
    console.log(today);
    document.getElementById('fecha_entrada').value = today;

}
function agregaFila(id) {
    var fila = '<tr><td><input class="texto" type="text" style="width: 150px;"/></td>';
    fila += '<td><input class="texto" type="text" style="width: 150px;"/></td>';
    fila += '<td><a href="#"><img src="add/img/elim.png"></a></td></tr>';
    document.getElementById(id).innerHTML += fila;
}
function recorreTbl() {
    var tbl = document.getElementById("tabla_entrada");
    var cod_ent = document.getElementById("codigo_lista_entrada").value;
    var tds = tbl.getElementsByTagName("input");
    for (i = 0; i < tds.length; i += 2) {
        var jsonDetalle = encodeURIComponent(
                '{"codigo_entrada":"' + cod_ent
                + '","codigo_articulo":"' + tds[i].value
                + '","cantidad":"' + tds[i + 1].value + '"}');
        console.log(jsonDetalle);
        alert("articulo :"+tds[i].value+" Agregado correctamente a el detalle");
        guardaDetalleEntrada(jsonDetalle);
    }
}

function guardarEntradaMercancia() {
    peticion_http = inicializa_objeto_ajax();
    cod_ent = document.getElementById("codigo_lista_entrada").value;
    var fecha = document.getElementById("fecha_entrada").value;
    var patente = document.getElementById("patente_camion_entrada").value;
    var rut_cli = document.getElementById("rut_cliente").value;

    var jsonEntrada = encodeURIComponent(
            '{"codigo_entrada":"' + cod_ent
            + '","fecha":"' + fecha
            + '","patente_camion":"' + patente
            + '","rut_cliente":"' + rut_cli + '"}');

    var datos = "parametro_boton=boton_guarda_entrada_mercancia&jsonEntrada=" + jsonEntrada;
    console.log(datos);

    peticion_http.onreadystatechange = procesaGuardarEntradaMercancia;

    peticion_http.open("POST", "/Prueba03/S_GestionEntradaMercancia", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);
}

function procesaGuardarEntradaMercancia() {
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        document.getElementById("div_resp_listaEntrada").innerHTML = "<img src='add/img/loading1.gif'>";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta = peticion_http.responseText;
                if (respuesta !== "error") {
                    document.getElementById("div_resp_listaEntrada").innerHTML = respuesta;
                    recorreTbl();
                }
            }
        } else {
            document.getElementById("div_resp_listaEntrada").innerHTML = "proceso erroneo";
        }
    }
    return;
}

function guardaDetalleEntrada(jsonDetEnt) {
    var datos = "parametro_boton=boton_guarda_detalle_entrada_mercancia&jsonDetalle=" + jsonDetEnt;
    console.log(datos);

    peticion_http.onreadystatechange = procesaGuardarDetalleEntradaMercancia;

    peticion_http.open("POST", "/Prueba03/S_GestionDetalleEntradaMercancia", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);
}

function procesaGuardarDetalleEntradaMercancia() {
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        document.getElementById("div_resp_listaEntrada").innerHTML = "<img src='add/img/loading1.gif'>";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                document.getElementById("div_resp_listaEntrada").innerHTML = peticion_http.responseText;
                return;
            }
        } else {
            document.getElementById("div_resp_listaEntrada").innerHTML = "proceso erroneo";
            return;
        }
    }
    return;
}




function buscarListaEntrada(id) {
    console.log(id);
    peticion_http = inicializa_objeto_ajax();
    var codigo_entrada = document.getElementById('buscarcodigoentrada').value;
    console.log(codigo_entrada);
    var datos = "parametro_boton=boton_buscar_lista_entrada&codigo_entrada=" + codigo_entrada;
    console.log("datos: " + datos);

    console.log("antes onready");
    peticion_http.onreadystatechange = procesaBuscarListaEntrada;
    console.log("despues onready");

    peticion_http.open("POST", "/Prueba03/S_GestionEntradaMercancia", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaBuscarListaEntrada() {
    console.log("en  procesaBuscarListaEntrada()");
    console.log("peticion_http.readyState: " + peticion_http.readyState);
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        console.log("readyState: " + peticion_http.readyState);
        document.getElementById("tabla_muestra_lista_entrada").innerHTML = "Enviando...";
        document.getElementById("tabla_muestra_detalle_lista_entrada").innerHTML = "";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta = peticion_http.responseText;
                if (respuesta !== "error") {
                    datos = respuesta.split("#");

                    console.log("resp " + respuesta);
                    var datosList = eval("(" + datos[0] + ")");
                    var datosDeta = datos[1];
                    var table = "\n\
                <thead><tr><th>codigo_entrada </th><th>Fecha </th><th>Patente</th></tr></thead><tbody>\n\
                <tr><td>" + datosList.codigo_entrada + "</td><td>" + datosList.fecha + "</td><td>" + datosList.patente_camion + "</td>\n\ "
                            + "</td></tr></tbody>";


                    var datosArt = datosDeta.split("*");
                    var table2 = "<thead><tr><th>Codigo Articulo</th><th>Cantidad </th></tr></thead><tbody>";
                    document.getElementById("tabla_muestra_detalle_lista_entrada").innerHTML = table2;
                    for (i = 0; i < datosArt.length; i++) {
                        var ele = datosArt[i].split(",");
                        table2 = "<tr><td>" + ele[1] + "</td>\n\
                <td>" + ele[2] + "</td></tr>";
                        document.getElementById("tabla_muestra_detalle_lista_entrada").innerHTML += table2;
                    }
                    document.getElementById("tabla_muestra_detalle_lista_entrada").innerHTML += "</tbody>";

                }
                else {
                    table = "No se encontró lista de entrada";
                    table2 = "No se encontró detalle de lista de entrada";

                }
                document.getElementById("tabla_muestra_lista_entrada").innerHTML = table;
            }
        } else {
            document.getElementById("tabla_muestra_lista_entrada").innerHTML = "proceso erroneo";
            document.getElementById("tabla_muestra_detalle_lista_entrada").innerHTML = "";

        }
    }
    return;
}
function buscarTodasListaEntrada() {
    peticion_http = inicializa_objeto_ajax();
    var datos = "parametro_boton=botonBuscarTodasListasEntrada";
    console.log("datos: " + datos);

    console.log("antes onready");
    peticion_http.onreadystatechange = procesaBuscarTodasListaEntrada;
    console.log("despues onready");

    peticion_http.open("POST", "/Prueba03/S_GestionEntradaMercancia", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaBuscarTodasListaEntrada() {
    console.log("en  procesaBuscarTodaSListas()");
    console.log("peticion_http.readyState: " + peticion_http.readyState);
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        console.log("readyState: " + peticion_http.readyState);
        document.getElementById("tabla_muestra_lista_salida").innerHTML = "<img src='add/img/loading1.gif'>";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta = peticion_http.responseText;
                if (respuesta !== "error") {
                    console.log("resp " + respuesta);
                    var datosArt = respuesta.split("|");
                    var table = "<thead><tr><th>Codigo entrada </th><th>Fecha</th><th>Patente</th></tr></thead><tbody>";
                    for (i = 0; i < datosArt.length; i++) {
                        var ele = datosArt[i].split(",");
                        table += "<tr><td>" + ele[0] + "</td><td>" + ele[1] + "</td><td>" + ele[2] + "</td></tr></tbody>";
                    }
                    table += "</tbody>";

                }
                else {
                    table = "No se encontraron Listas de Entrada\n\
 ";
                }
                document.getElementById("tabla_muestra_lista_entrada").innerHTML = table;
                document.getElementById("tabla_muestra_detalle_lista_entrada").innerHTML = "";
            }
        } else {
            document.getElementById("tabla_muestra_lista_entrada").innerHTML = "proceso erroneo";
            document.getElementById("tabla_muestra_detalle_lista_entrada").innerHTML = "";
        }
    }
    return;
}