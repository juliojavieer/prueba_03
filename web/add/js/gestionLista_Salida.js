var READY_STATE_COMPLETE = 4;
var peticion_http;

function guardarListaSalida() {

    var fol = document.getElementById("folio").value;
    var rutc = document.getElementById("rut_cliente").value;
    var fech = document.getElementById("fecha").value;
    var cant = document.getElementById("cantidad").value;
    var cod_art = document.getElementById("codigo_articulo").value;
    var rutd = document.getElementById("rut_des").value;
    var cod_entra = document.getElementById("cod_entrada").value;
    if (fol.length > 0 && fech.length >= 10 && cant.length > 0 && cod_art.length > 1) {
        console.log("datos correctos");
        console.log("Ingresar Lista Salida");
        console.log(fol + "|" + rutc);
        var jsonList = encodeURIComponent(
                '{"folio":"' + fol
                + '","rut_cliente":"' + rutc
                + '","fecha":"' + fech
                + '"}');

        var jsonDetalle = encodeURIComponent(
                '{"folio":"' + fol
                + '","codigo_articulo":"' + cod_art
                + '","fecha_salida":"' + fech
                + '","cantidad":"' + cant
                + '","rut_destinatario":"' + rutd
                + '","codigo_entrada":"' + cod_entra
                + '"}');

        guardaLista_Salida(jsonList, jsonDetalle);



        return true;
    }
    else {
        console.log("datos incorrectos");
        document.getElementById("div_resp_listaSalida").innerHTML = "Datos incompletos o incorrectos";
    }

}
function guardaLista_Salida(jsonList, jsonDetalle) {
    peticion_http = inicializa_objeto_ajax();

    var datos = "parametro_boton=boton_guarda_Lista_Salida&jsonList=" + jsonList + "&jsonDetalle=" + jsonDetalle;

    peticion_http.onreadystatechange = procesaGuardarLista_Salida;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()

    peticion_http.open("POST", "/Prueba03/S_Gestion_Lista_Salida", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaGuardarLista_Salida() {
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        document.getElementById("div_resp_listaSalida").innerHTML = "Enviando...";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                document.getElementById("div_resp_listaSalida").innerHTML = peticion_http.responseText;
            }
        } else {
            document.getElementById("div_resp_listaSalida").innerHTML = "proceso erroneo";
        }
    }
    return;
}


function buscarLista_Salida(id) {
    console.log(id);
    peticion_http = inicializa_objeto_ajax();
    var foli = document.getElementById('buscarfoliolistasalida').value;
    console.log(foli);
    var datos = "parametro_boton=boton_buscar_lista_salida&folio=" + foli;
    console.log("datos: " + datos);

    console.log("antes onready");
    peticion_http.onreadystatechange = procesaBuscarListaSalida;
    console.log("despues onready");

    peticion_http.open("POST", "/Prueba03/S_Gestion_Lista_Salida", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaBuscarListaSalida() {
    console.log("en  procesaBuscarListaSalida()");
    console.log("peticion_http.readyState: " + peticion_http.readyState);
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        console.log("readyState: " + peticion_http.readyState);
        document.getElementById("tabla_muestra_lista_salida").innerHTML = "Enviando...";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) { 
                var respuesta = peticion_http.responseText;
                if (respuesta !== "error") {
                    datos=respuesta.split("#");
                    
                    console.log("resp " + respuesta);
                    var datosList = eval("(" + datos[0] + ")");
                    var datosDeta = datos[1];
                    var table = "\n\
                <thead><tr><th>folio </th><th>Fecha </th></tr></thead><tbody>\n\
                <tr><td>" + datosList.folio + "</td><td>" + datosList.fecha + "</td>\n\ "
                            + "</td></tr></tbody>";
          
          
                    var datosArt = datosDeta.split("|");
                    var table2 = "<thead><tr><th>Codigo articulo </th><th>Fecha salida</th><th>Cantidad</th><th>Rut dest</th><th>Codigo Entrada</th></tr></thead><tbody>";
                    document.getElementById("tabla_muestra_detalle_lista_salida").innerHTML = table2;
                    for (i = 0; i < datosArt.length; i++) {
                        var ele = datosArt[i].split(",");
                        table2 = "<tr><td>" + ele[1] + "</td><td>" + ele[2] + "</td>\n\
                <td>" + ele[3] + "</td><td>" + ele[4] + "</td><td>" + ele[5] + "</td></tr>";
                        document.getElementById("tabla_muestra_detalle_lista_salida").innerHTML += table2;
                    }
                    document.getElementById("tabla_muestra_detalle_lista_salida").innerHTML += "</tbody>";
                
                }
                else {
                    table = "No se encontró lista de salida";
                    table2 = "No se encontró detalle de lista de salida";

                }
                document.getElementById("tabla_muestra_lista_salida").innerHTML = table;
            }
        } else {
            document.getElementById("tabla_muestra_lista_salida").innerHTML = "proceso erroneo";
        }
    }
    return;
}
function buscarTodasListasSalidas() {
    peticion_http = inicializa_objeto_ajax();
    var datos = "parametro_boton=botonBuscarTodasListasSalida";
    console.log("datos: " + datos);

    console.log("antes onready");
    peticion_http.onreadystatechange = procesaBuscarTodasListasSalidas;
    console.log("despues onready");

    peticion_http.open("POST", "/Prueba03/S_Gestion_Lista_Salida", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaBuscarTodasListasSalidas() {
    console.log("en  procesaBuscarTodaSListas()");
    console.log("peticion_http.readyState: " + peticion_http.readyState);
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        console.log("readyState: " + peticion_http.readyState);
        document.getElementById("tabla_muestra_lista_salida").innerHTML = "<img src='add/img/loading1.gif'>";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta = peticion_http.responseText;
                if (respuesta !== "error") {
                    console.log("resp " + respuesta);
                    var datosArt = respuesta.split("|");
                    var table = "<thead><tr><th>Folio</th><th>Fecha</th></tr></thead><tbody>";
                    for (i = 0; i < datosArt.length; i++) {
                        var ele = datosArt[i].split(",");
                        table += "<tr><td>" + ele[0] + "</td><td>" + ele[1] + "</td></tr></tbody>";
                    }
                    table += "</tbody>";

                }
                else {
                    table = "No se encontraron Listas de Salida ";
                }
                document.getElementById("tabla_muestra_lista_salida").innerHTML = table;
                 document.getElementById("tabla_muestra_detalle_lista_salida").innerHTML = "";
            }
        } else {
            document.getElementById("tabla_muestra_lista_salida").innerHTML = "proceso erroneo";
            document.getElementById("tabla_muestra_detalle_lista_salida").innerHTML = "";

        }
    }
    return;
}