var READY_STATE_COMPLETE = 4;
var peticion_http;

function inicializa_objeto_ajax() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else
    if (window.ActiveXObject) {
        try {
            xmlHttpReq = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (exp1) {
            try {
                xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (exp2) {
                xmlHttpReq = false;
            }
        }
    }
}


function eliminarProveedor(id) {
    console.log(id);
    peticion_http = inicializa_objeto_ajax();
    var rut_proveedor = id;
    console.log(rut_proveedor);
    var datos = "parametro_boton=botonEliminarProveedor&parametro_rut=" + rut_proveedor;
    console.log("datos: " + datos);

    console.log("antes onready");
    peticion_http.onreadystatechange = procesaEliminarProveedor;
    console.log("despues onready");

    peticion_http.open("POST", "/Prueba03/S_GestionProveedores", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;

}
function procesaEliminarProveedor() {
    console.log("en  procesaEliminarProveedor()");
    console.log("peticion_http.readyState: " + peticion_http.readyState);
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        console.log("readyState: " + peticion_http.readyState);
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta = peticion_http.responseText;
                if (respuesta !== "error") {
                    console.log("resp " + respuesta);
                    var datosArt = eval("(" + respuesta + ")");
                    alert("eliminado");
                }
                else {
                    table = "No se encontró proveedor";
                }
            }
        } else {
            alert("Proceso erroneo.");
        }
    }
    return;
}


function verSelePro() {
    jQuery.fn.getCheckboxValues = function() {
        var values = [];
        var i = 0;
        this.each(function() {
            values[i++] = $(this).val();
            eliminarProveedor($(this).val());
        });
        console.log(values);
        return values;
    };
    var arr = $("input:checked").getCheckboxValues();
}

function buscarProveedor(id) {
    console.log(id);
    peticion_http = inicializa_objeto_ajax();
    var rut_proveedor = document.getElementById('buscarutproveedor').value;
    console.log(rut_proveedor);
    var datos = "parametro_boton=botonBuscarProveedor&parametro_rut=" + rut_proveedor;
    console.log("datos: " + datos);

    console.log("antes onready");
    peticion_http.onreadystatechange = procesaBuscarProveedor;
    console.log("despues onready");

    peticion_http.open("POST", "/Prueba03/S_GestionProveedores", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaBuscarProveedor() {
    console.log("en  procesaRespuestaProveedor()");
    console.log("peticion_http.readyState: " + peticion_http.readyState);
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        console.log("readyState: " + peticion_http.readyState);
        document.getElementById("tabla_muestra_proveedor").innerHTML = "<img src='add/img/loading1.gif'>";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta = peticion_http.responseText;
                if (respuesta !== "error") {
                    console.log("resp " + respuesta);
                    var datosArt = eval("(" + respuesta + ")");
                    var table = "\n\
                <thead><tr><th>Rut Prov</th><th>Nombre</th><th>Direccion</th><th>Ciudad</th><th>Telefono</th><th>Mail</th><th><button class='buttonconfirm' onclick='javascript:openDialog();'>Eliminar</button></th><tr></thead><tbody>\n\
                <tr><td onclick='modPro(0);'>" + datosArt.rut + "</td><td>" + datosArt.rsocial + "</td><td>" + datosArt.direccion + "</td>\n\
                <td>" + datosArt.ciudad + "</td><td>" + datosArt.telefono + "</td><td>" + datosArt.email + "</td><td><input type='checkbox' value='" + datosArt.rut_cli + "' /></td></tr></tbody>";
                }
                else {
                    table = "No se encontró proveedor";
                }
                document.getElementById("tabla_muestra_proveedor").innerHTML = table;
            }
        } else {
            document.getElementById("tabla_muestra_proveedor").innerHTML = "proceso erroneo";
        }
    }
    return;
}



function buscarTodosPro() {
    peticion_http = inicializa_objeto_ajax();
    var datos = "parametro_boton=boton_buscar_todos_proveedores";
    console.log("datos: " + datos);

    console.log("antes onready");
    peticion_http.onreadystatechange = procesaBuscarTodosPro;
    console.log("despues onready");

    peticion_http.open("POST", "/Prueba03/S_GestionProveedores", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaBuscarTodosPro() {
    console.log("en  procesaRespuestaTodosProveedor()");
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        console.log("readyState: " + peticion_http.readyState);
        document.getElementById("tabla_muestra_proveedor").innerHTML = "<img src='add/img/loading1.gif'>";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                var respuesta = peticion_http.responseText;
                if (respuesta !== "error") {
                    console.log("resp " + respuesta);
                    var datosArt = respuesta.split("|");
                    var table = "<thead><tr><th>Rut Prov</th><th>Nombre</th><th>Direccion</th><th>Ciudad</th><th>Telefono</th><th>Mail</th><th><button class='buttonconfirm' onclick='javascript:openDialog();'>Eliminar</button></th></tr></thead><tbody>";
                    document.getElementById("tabla_muestra_proveedor").innerHTML = table;
                    for (i = 0; i < datosArt.length; i++) {
                        var ele = datosArt[i].split(",");
                        table = "<tr><td  onclick='modPro(" + i + ");'>" + ele[0] + "</td><td>" + ele[1] + "</td><td>" + ele[2] + "</td>\n\
                <td>" + ele[3] + "</td><td>" + ele[4] + "</td><td>" + ele[5] + "</td><td><input type='checkbox' value='" + ele[0] + "' /></td></tr>";
                        document.getElementById("tabla_muestra_proveedor").innerHTML += table;
                    }
                    document.getElementById("tabla_muestra_proveedor").innerHTML += "</tbody>";
                }
                else {
                    document.getElementById("tabla_muestra_proveedor").innerHTML = "No se encontró proveedor";
                }
            }
        } else {
            document.getElementById("tabla_muestra_proveedor").innerHTML = "proceso erroneo";
        }
    }
    return;
}



function actualizarProveedor() {
    peticion_http = inicializa_objeto_ajax();
    console.log("en actualizarProveedor()");
    var rut1 = document.getElementById("rut_act").value;
    console.log(rut1);
    var rso1 = document.getElementById("rsocial_act").value;
    console.log(rso1);
    var dir1 = document.getElementById("direccion_act").value;
    console.log(dir1);
    var ema1 = document.getElementById("correo_act").value;
    console.log(ema1);
    var tel1 = document.getElementById("telefono_act").value;
    console.log(tel1);
    var ciu1 = document.getElementById("ciudad_act").value;
    console.log(ciu1);
    
    var jsonPer = encodeURIComponent(
                    '{"rut":"' + rut1
                    + '","rsocial":"' + rso1
                    + '","direccion":"' + dir1
                    + '","ciudad":"' + ciu1
                    + '","telefono":"' + tel1
                    + '","email":"' + ema1 + '"}');
            
    var datos = "parametro_boton=boton_actualiza_proveedor&jsonPer=" + jsonPer;

    peticion_http.onreadystatechange = procesaActualizarProveedor;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()

    peticion_http.open("POST", "/Prueba03/S_GestionProveedores", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaActualizarProveedor() {
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        document.getElementById("div_respuesta_actualiza_proveedor").innerHTML = "<img src='add/img/loading1.gif'>";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                document.getElementById("div_respuesta_actualiza_proveedor").innerHTML = peticion_http.responseText;
            }
        } else {
            document.getElementById("div_respuesta_actualiza_proveedor").innerHTML = "proceso erroneo";
        }
    }
    return;
}




function guardarProveedor(jsonPro) {
    peticion_http = inicializa_objeto_ajax();

    var datos = "parametro_boton=boton_guarda_proveedor&jsonPro=" + jsonPro;

    peticion_http.onreadystatechange = procesaGuardarProveedor;    // esto debe ir primero que el metodo open(), para capturar el momento en que se ejecuta open()

    peticion_http.open("POST", "/Prueba03/S_GestionProveedores", true);
    peticion_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=ISO-8859-1');
    peticion_http.setRequestHeader("Content-length", datos.length);
    peticion_http.setRequestHeader("Connection", "close");
    peticion_http.send(datos);

    return;
}

function procesaGuardarProveedor() {
    if (peticion_http.readyState === 1 || peticion_http.readyState === 2 || peticion_http.readyState === 3) { //procesando mientras se realiza la operacion
        document.getElementById("div_resp_pers").innerHTML = "<img src='add/img/loading1.gif'>";
    }
    else {
        if (peticion_http.readyState === READY_STATE_COMPLETE) {
            if (peticion_http.status === 200) {
                document.getElementById("div_resp_pers").innerHTML = peticion_http.responseText;
            }
        } else {
            document.getElementById("div_resp_pers").innerHTML = "proceso erroneo";
        }
    }
    return;
}


function completarModificar(indice) {
    var tbl = document.getElementById("tabla_muestra_proveedor");
    var as = tbl.getElementsByTagName("td");
    var a = as[indice * 7].innerHTML;
    document.getElementById("rut_act").value = a;

    var a = as[(indice * 7) + 1].innerHTML;
    document.getElementById("rsocial_act").value = a;

    var a = as[(indice * 7) + 2].innerHTML;
    document.getElementById("direccion_act").value = a;

    var a = as[(indice * 7) + 3].innerHTML;
    document.getElementById("ciudad_act").value = a;

    var a = as[(indice * 7) + 4].innerHTML;
    document.getElementById("telefono_act").value = a;

    var a = as[(indice * 7) + 5].innerHTML;
    document.getElementById("correo_act").value = a;
    
    document.getElementById("div_respuesta_actualiza_proveedor").innerHTML="&nbsp;";

}