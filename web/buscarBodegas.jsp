<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    try {
        HttpSession actual = request.getSession(true);
        String id = actual.getId();
        String nombre = (String) actual.getAttribute("logueado");
        String tipo = (String) actual.getAttribute("tipo");
        System.out.println("usuario" + nombre);
        if (actual.isNew()) {
            response.sendRedirect("index.jsp");
            return;
        }
        if (actual == null) {
            response.sendRedirect("index.jsp");
        } else {
            if (actual.getAttribute("logueado") == null) {
                response.sendRedirect("index.jsp");
            }
            if(tipo.equalsIgnoreCase("user")){
                System.out.println("user");
                response.sendRedirect("Cliente.jsp");
            }
        }
        out.print("<input type='hidden' id='rut_cliente' value='" + nombre + "'>");
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="add/css/busca_proveedor.css"/>
        <link rel="stylesheet" type="text/css" href="add/css/awesomecomplete.css"/>
        <script type="text/javascript" src="add/js/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="add/js/jquery.awesomecomplete.js"></script>
        <script type="text/javascript" src="add/js/AutocomBodega.js"></script>  
        <script type="text/javascript" src="add/js/gestionBodega.js"></script> 
        <link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic' rel='stylesheet' type='text/css'>	
        <link rel="stylesheet" href="add/css/estiloconfirmar.css">
        <link rel="stylesheet" href="add/css/avgrund.css">
        <script type="text/javascript" src="add/js/avgrund.js"></script>
        <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>

        <!-- Para mostrar los efectos css y jquery en el menu principal -->
        <link rel="stylesheet" href="add/css/colorbox.css" />
        <script type="text/javascript" src="add/js/jquery.min.js"></script>
        <script src="add/js/jquery.colorbox.js"></script>
        <link rel="stylesheet" href="add/css/stylemenu.css" type="text/css" media="screen"/>
        <script type="text/javascript" src="add/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="add/js/jqueryimagenes.js"></script>
        <script type="text/javascript" src="add/js/jquery.awesomecomplete.js"></script>
        
        <script>
            function openDialog() {
                Avgrund.show("#elim-dest");
            }
            function closeDialog() {
                Avgrund.hide();
            }
            
            function modBode(indice) {
                Avgrund.show("#modificar-bodega");
                completarModificarBodega(indice);
            
            }
            function CierraMod() {
                Avgrund.hide();
            }
        </script>

    </head>
    <body>

        <aside id="elim-dest" class="avgrund-popup">
            <center>
                <h2>Eliminar bodega</h2>
                <p>
                    Al eliminar estos datos, podrás tener problemas si este tiene datos propios.
                </p>
                <p>
                    ¿Estás seguro que deseas eliminar?
                </p>
                <button class="buttonconfirm" onclick="verSeleBodega();
                javascript:closeDialog();">Eliminar</button>
                <button class="buttonconfirm" onclick="javascript:closeDialog();">Cancelar</button>
            </center>
        </aside>
        
        <aside id="modificar-bodega" class="avgrund-popup" style="height: 250px; top: 55%;">
            <center>
                <h2>Modificar Bodega</h2><br>
                <table id="tabla_actualiza_bodega">
                    <tr><td>id</td><td><input size="25" readonly="readonly" onfocus="this.blur();" type="text" class="bbusca" id="id_bode"></td>
                    <tr><td>Nombre</td><td><input size="25" type="text" class="bbusca" id="nombre_bode"></td>
                </table>
                <p>
                    ¿Estás seguro de guardar?
                </p>
                <div id="div_respuesta_actualiza_bodega"></div>
                <button class="buttonconfirm" onclick="actualizarBodega();">Guardar Cambios</button>
                <button class="buttonconfirm" onclick="javascript:CierraMod();">Cerrar</button>
            </center>
        </aside>

        <h1 class="titulo">Buscar Bodegas</h1>
    <center>
        <br><br>
        <form id="form_busca_bodega" style="width: 355px; ">
            <input type="text" id="buscaidbodega" class="bbusca" placeholder="Nombre o id bodega..." required />
            <button id="boton_buscar_bodega" class="bbusca" type="button" onclick="buscarBodega();">Buscar</button>
            <button id="boton_buscar_todos_bodegas" class="bbusca" type="button" onclick="buscarTodasBod();"  >Buscar todas</button>   
        </form>

        <br><br><br>
        <div class="datagrid" style="width: 80%">
            <table id="tabla_muestra_bodega" style="width: 600px" >
            </table> 
        </div>
        <br><br><br>
    </center>

</body>
</html>
