<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    try {
        HttpSession actual = request.getSession(true);
        String id = actual.getId();
        String nombre = (String) actual.getAttribute("logueado");
        String tipo = (String) actual.getAttribute("tipo");
        System.out.println("usuario" + nombre);
        if (actual.isNew()) {
            response.sendRedirect("index.jsp");
            return;
        }
        if (actual == null) {
            response.sendRedirect("index.jsp");
        } else {
            if (actual.getAttribute("logueado") == null) {
                response.sendRedirect("index.jsp");
            }
        }
        out.print("<input type='hidden' id='rut_cliente' value='" + nombre + "'>");
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="add/css/busca_proveedor.css"/>
        <link rel="stylesheet" type="text/css" href="add/css/awesomecomplete.css"/>
        <script type="text/javascript" src="add/js/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="add/js/jquery.awesomecomplete.js"></script>
        <script type="text/javascript" src="add/js/AutocomCliente.js"></script>  
        <script type="text/javascript" src="add/js/gestionCliente.js"></script> 
        <link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic' rel='stylesheet' type='text/css'>	
        <link rel="stylesheet" href="add/css/estiloconfirmar.css">
        <link rel="stylesheet" href="add/css/avgrund.css">
        <script type="text/javascript" src="add/js/avgrund.js"></script>
        <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
        
        <!-- Para mostrar los efectos css y jquery en el menu principal -->
        <link rel="stylesheet" href="add/css/colorbox.css" />
        <script type="text/javascript" src="add/js/jquery.min.js"></script>
        <script src="add/js/jquery.colorbox.js"></script>
        <link rel="stylesheet" href="add/css/stylemenu.css" type="text/css" media="screen"/>
        <script type="text/javascript" src="add/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="add/js/jqueryimagenes.js"></script>
        <script type="text/javascript" src="add/js/jquery.awesomecomplete.js"></script>
        

        <script>
            function openDialog() {
                Avgrund.show("#elim-cliente");
            }
            function closeDialog() {
                Avgrund.hide();
            }
            function modCliente(indice) {
                Avgrund.show("#modificar-cliente");
                completarModificarCliente(indice);

            }
            function CierraCliente() {
                Avgrund.hide();
            }
        </script>

    </head>
    <body>

        <aside id="elim-cliente" class="avgrund-popup">
            <center>
                <h2>Eliminar cliente</h2>
                <p>
                    Al eliminar estos datos, podrás tener problemas si este tiene datos propios.
                </p>
                <p>
                    ¿Estás seguro que deseas eliminar?
                </p>
                <div id="div_elimina_cliente"></div>
                <button class="buttonconfirm" onclick="verSeleClientes();">Eliminar</button>
                <button class="buttonconfirm" onclick="javascript:closeDialog();">Cerrar</button>
            </center>
        </aside>
        
        <aside id="modificar-cliente" class="avgrund-popup" style="height: 270px; top: 55%;">
            <center>
                <h2>Modificar Cliente</h2><br>
                <table id="tabla_actualiza_cliente">
                    <tr><td>Rut</td><td><input size="25" readonly="readonly" onfocus="this.blur();" type="text" class="bbusca" id="rut_act"></td>
                    <tr><td>Descuento</td><td><input size="25" type="text" class="bbusca" id="descuento_act"></td>
                    <tr><td>Clasificación</td><td><input size="25" type="text" class="bbusca" id="clasificacion_act"></td>
                </table>
                <p>
                    ¿Estás seguro de guardar?
                </p>
                <div id="div_respuesta_actualiza_cliente">&nbsp;</div>
                <button class="buttonconfirm" onclick="actualizarCliente();">Guardar Cambios</button>
                <button class="buttonconfirm" onclick="javascript:CierraCliente();">Cerrar</button>
            </center>
        </aside>
        

        <h1 class="titulo">Buscar Cliente</h1>
    <center>
        <br><br>
        <form id="form_busca" style="width: 350px; ">
            <input type="text" onblur="Rut(id);" id="buscarutcliente" class="bbusca" placeholder="Ingrese rut o nombre..." required />
            <button id="boton_buscar_cli" class="bbusca" type="button" onclick="buscarCliente(id);">Buscar</button>
            <button id="boton_buscar_todos_cli" class="bbusca" type="button" onclick="buscarTodosCli();"  >Buscar todos</button>   
        </form>

        <br><br><br>
        <div class="datagrid" style="width: 80%">
            <table id="tabla_muestra_cliente" style="width: 1100px" >
            </table> 
        </div>
        <br><br><br>
    </center>

</body>
</html>
