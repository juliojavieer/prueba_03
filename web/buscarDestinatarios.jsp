<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    try {
        HttpSession actual = request.getSession(true);
        String id = actual.getId();
        String nombre = (String) actual.getAttribute("logueado");
        String tipo = (String) actual.getAttribute("tipo");
        System.out.println("usuario" + nombre);
        if (actual.isNew()) {
            response.sendRedirect("index.jsp");
            return;
        }
        if (actual == null) {
            response.sendRedirect("index.jsp");
        } else {
            if (actual.getAttribute("logueado") == null) {
                response.sendRedirect("index.jsp");
            }
            if(tipo.equalsIgnoreCase("admin")){
                System.out.println("admin");
                response.sendRedirect("Administrador.jsp");
            }
            if(tipo.equalsIgnoreCase("admin")){
                System.out.println("admin");
                response.sendRedirect("Administrador.jsp");
            }
        }
        out.print("<input type='hidden' id='rut_cliente' value='" + nombre + "'>");
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="add/css/busca_proveedor.css"/>
        <link rel="stylesheet" type="text/css" href="add/css/awesomecomplete.css"/>
        <script type="text/javascript" src="add/js/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="add/js/jquery.awesomecomplete.js"></script>
        <script type="text/javascript" src="add/js/AutocomDestinatario.js"></script>  
        <script type="text/javascript" src="add/js/gestionDestinatarios.js"></script> 
        <link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic' rel='stylesheet' type='text/css'>	
        <link rel="stylesheet" href="add/css/estiloconfirmar.css">
        <link rel="stylesheet" href="add/css/avgrund.css">
        <script type="text/javascript" src="add/js/avgrund.js"></script>
        <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
        
        <!-- Para mostrar los efectos css y jquery en el menu principal -->
        <link rel="stylesheet" href="add/css/colorbox.css" />
        <script type="text/javascript" src="add/js/jquery.min.js"></script>
        <script src="add/js/jquery.colorbox.js"></script>
        <link rel="stylesheet" href="add/css/stylemenu.css" type="text/css" media="screen"/>
        <script type="text/javascript" src="add/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="add/js/jqueryimagenes.js"></script>
        <script type="text/javascript" src="add/js/jquery.awesomecomplete.js"></script>

        <script>
            function openDialog() {
                Avgrund.show("#elim-dest");
            }
            function closeDialog() {
                Avgrund.hide();
            }
            function modDes(indice) {
                Avgrund.show("#modificar-des");
                completarModificarDest(indice);

            }
            function CierraDes() {
                Avgrund.hide();
            }
        </script>

    </head>
    <body>

        <aside id="elim-dest" class="avgrund-popup">
            <center>
                <h2>Eliminar destinatario</h2>
                <p>
                    Al eliminar estos datos, no los podrás recuperar nuevamente.
                </p>
                <p>
                    ¿Estás seguro que deseas eliminar?
                </p>
                <button class="buttonconfirm" onclick="verSeleDest();
                javascript:closeDialog();">Eliminar</button>
                <button class="buttonconfirm" onclick="javascript:closeDialog();">Cancelar</button>
            </center>
        </aside>
        
        <aside id="modificar-des" class="avgrund-popup" style="height: 400px; top: 55%;">
            <center>
                <h2>Modificar Destinatario</h2><br>
                <table id="tabla_actualiza_destinatario">
                    <tr><td>Rut</td><td><input size="25" readonly="readonly" onfocus="this.blur();" type="text" class="bbusca" id="rut_act"></td>
                    <tr><td>Razon Social</td><td><input onblur="comprobar_nombre(id);" size="25" type="text" class="bbusca" id="rsocial_act"></td>
                    <tr><td>Dirección</td><td><input size="25" type="text" class="bbusca" id="direccion_act"></td>
                    <tr><td>Ciudad</td><td><input size="25" type="text" class="bbusca" id="ciudad_act"></td>
                    <tr><td>Teléfono</td><td><input onblur="validarTelefono(id);" size="25" type="text" class="bbusca" id="telefono_act"></td>
                    <tr><td>Correo</td><td><input onblur="validaEmail(id);" size="25" type="text" class="bbusca" id="correo_act"></td>
                </table>
                <p>
                    ¿Estás seguro de guardar?
                </p>
                <div id="div_respuesta_actualiza_destinatario"></div>
                <button class="buttonconfirm" onclick="actualizarDestinatario();">Guardar Cambios</button>
                <button class="buttonconfirm" onclick="javascript:CierraDes();">Cerrar</button>
            </center>
        </aside>

        <h1 class="titulo">Buscar Destinatario</h1>
    <center>
        <br><br>
        <form id="form_busca" style="width: 348px; ">
            <input type="text" onblur="Rut(id);" id="buscarutdestinatario" class="bbusca" placeholder="Ingrese rut..." required />
            <button id="boton_buscar_pro" class="bbusca" type="button" onclick="buscarDestinatario(id);">Buscar</button>
            <button id="boton_buscar_todos_pro" class="bbusca" type="button" onclick="buscarTodosDest();"  >Buscar todos</button>   
        </form>

        <br><br><br>
        <div class="datagrid">
            <table id="tabla_muestra_destinatario" >
            </table> 
        </div>
        <br><br><br>
    </center>

</body>
</html>
