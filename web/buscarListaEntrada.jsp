<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="utilidades.Oraconex"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    try {
        HttpSession actual = request.getSession(true);
        String id = actual.getId();
        String nombre = (String) actual.getAttribute("logueado");
        String tipo = (String) actual.getAttribute("tipo");
        System.out.println("usuario" + nombre);
        if (actual.isNew()) {
            response.sendRedirect("index.jsp");
            return;
        }
        if (actual == null) {
            response.sendRedirect("index.jsp");
        } else {
            if (actual.getAttribute("logueado") == null) {
                response.sendRedirect("index.jsp");
            }
            if(tipo.equalsIgnoreCase("admin")){
                System.out.println("admin");
                response.sendRedirect("Administrador.jsp");
            }
        }
        out.print("<input type='hidden' id='rut_cliente' value='" + nombre + "'>");
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="add/css/busca_proveedor.css"/>
        <link rel="stylesheet" type="text/css" href="add/css/awesomecomplete.css"/>
        <script type="text/javascript" src="add/js/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="add/js/jquery.awesomecomplete.js"></script>
        <script src="add/js/gestionEntradaMercancia.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic' rel='stylesheet' type='text/css'>	
        <link rel="stylesheet" href="add/css/estiloconfirmar.css">
        <link rel="stylesheet" href="add/css/avgrund.css">
        <script type="text/javascript" src="add/js/avgrund.js"></script>
        <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>

        
        <!-- Para mostrar los efectos css y jquery en el menu principal -->
        <link rel="stylesheet" href="add/css/colorbox.css" />
        <script type="text/javascript" src="add/js/jquery.min.js"></script>
        <script src="add/js/jquery.colorbox.js"></script>
        <link rel="stylesheet" href="add/css/stylemenu.css" type="text/css" media="screen"/>
        <script type="text/javascript" src="add/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="add/js/jqueryimagenes.js"></script>
        <script type="text/javascript" src="add/js/jquery.awesomecomplete.js"></script>
        
        <script>
            function openDialog() {
                Avgrund.show("#elim-lista_salida");
            }
            function closeDialog() {
                Avgrund.hide();
            }
        </script>

    </head>
    <body>

          <h1 class="titulo">Buscar Lista Entrada</h1>
    <center>
        <br><br>
        <form id="form_busca" style="width: 358px; ">
            <input type="text" id="buscarcodigoentrada" class="bbusca" placeholder="Ingrese folio..." required />
            <button id="boton_buscar_lista_entrada" class="bbusca" type="button" onclick="buscarListaEntrada(id);">Buscar</button>
            <button id="boton_buscar_todas_lista_entrada" class="bbusca" type="button" onclick="buscarTodasListaEntrada();"  >Buscar todos</button>   
        </form>

        <br><br><br>
       <div class="datagrid">
            <table id="tabla_muestra_lista_entrada"  style="width: 300px; box-shadow: 0px 0px 0px 0px #000 " >
            </table>
                    <br>
            <table id="tabla_muestra_detalle_lista_entrada"  style="width: 400px; box-shadow: 0px 0px 0px 0px #000 " >
            </table> 
            <br>

        </div>
        <br><br><br>
    </center>

</body>
</html>