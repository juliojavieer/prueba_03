<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="utilidades.Oraconex"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    try {
        HttpSession actual = request.getSession(true);
        String id = actual.getId();
        String nombre = (String) actual.getAttribute("logueado");
        String tipo = (String) actual.getAttribute("tipo");
        System.out.println("usuario" + nombre);
        if (actual.isNew()) {
            response.sendRedirect("index.jsp");
            return;
        }
        if (actual == null) {
            response.sendRedirect("index.jsp");
        } else {
            if (actual.getAttribute("logueado") == null) {
                response.sendRedirect("index.jsp");
            }
        }
        out.print("<input type='hidden' id='rut_cliente' value='" + nombre + "'>");
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="add/css/busca_proveedor.css"/>
        <link rel="stylesheet" type="text/css" href="add/css/awesomecomplete.css"/>
        <script type="text/javascript" src="add/js/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="add/js/jquery.awesomecomplete.js"></script>
        <script type="text/javascript" src="add/js/gestionUbicacion.js"></script> 
        <script type="text/javascript" src="add/js/gestionEntra_a_ubicacion.js"></script> 
        <link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic' rel='stylesheet' type='text/css'>	
        <link rel="stylesheet" href="add/css/estiloconfirmar.css">
        <link rel="stylesheet" href="add/css/avgrund.css">
        <script type="text/javascript" src="add/js/avgrund.js"></script>
        <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
        
        <!-- Para mostrar los efectos css y jquery en el menu principal -->
        <link rel="stylesheet" href="add/css/colorbox.css" />
        <script type="text/javascript" src="add/js/jquery.min.js"></script>
        <script src="add/js/jquery.colorbox.js"></script>
        <link rel="stylesheet" href="add/css/stylemenu.css" type="text/css" media="screen"/>
        <script type="text/javascript" src="add/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="add/js/jqueryimagenes.js"></script>
        <script type="text/javascript" src="add/js/jquery.awesomecomplete.js"></script>

    </head>
    <script>
        function openDialog() {
            Avgrund.show("#elim-ubi");
        }
        function closeDialog() {
            Avgrund.hide();
        }
        function EntraAUbi(indice) {
            Avgrund.show("#entra-a-ubi");
            completarModificar(indice);

        }
        function CierraMod() {
            Avgrund.hide();
        }
    </script>
    <body>

        <aside id="elim-ubi" class="avgrund-popup">
            <center>
                <h2>Eliminar ubicacion</h2>
                <p>
                    Al eliminar estos datos, no los podrás recuperar nuevamente.
                </p>
                <p>
                    ¿Estás seguro que deseas eliminar?
                </p>
                <button class="buttonconfirm" onclick="verSeleUbi();
            javascript:closeDialog();">Eliminar</button>
                <button class="buttonconfirm" onclick="javascript:closeDialog();">Cancelar</button>
            </center>
        </aside>



        <aside id="entra-a-ubi" class="avgrund-popup" style="height: 420px; top: 55%;">
            <center>
                <h2>Entra a ubicacion</h2><br>
                <table id="tabla_entra_a_ubicacion">
                    <tr><td>Fecha hora ingreso</td><td><input size="25" type="text"  class="bbusca" id="fecha_hora_ingreso"></td>
                    <tr><td> Codigo articulo </td><td><input size="25" type="text" class="bbusca" id="cod_art"></td>      
                    <tr><td> Fecha retiro total</td><td><input size="25" type="text" class="bbusca" id="fecha_retiro_total"></td>      
                    <tr><td>Id ubicacion</td><td><input size="25"  readonly="readonly" onfocus="this.blur();" type="text" class="bbusca" id="id_ubi"></td>      
                    <tr><td>Cantidad inicial</td><td><input size="25" type="text" class="bbusca" id="cantidad_inicial"></td>      
                    <tr><td>Stock</td><td><input size="25" type="text" class="bbusca" id="stock"></td>      
                    <tr><td>Codigo Entrada</td><td><input size="25" type="text" class="bbusca" id="codigo_entrada"></td>      


                </table>
                <p>
                    ¿Estás seguro de guardar?
                </p>
                <div id="div_respuesta_entra_a_ubicacion"></div>
                <button class="buttonconfirm" onclick="guardarEntra_a_ubicacion();">Guardar</button>
                <button class="buttonconfirm" onclick="javascript:CierraMod();">Cerrar</button>
                <button class="buttonconfirm" type="reset">Limpiar</button>
            </center>
        </aside>

        <h1 class="titulo">Buscar Ubicacion</h1>
    <center>
        <br><br>
        <form id="form_busca" style="width: 500px; ">
            <input type="text" id="buscaridubicacion" class="bbusca" placeholder="Ingrese id ubicacion..." required />
            <button id="boton_buscar_ubicacion" class="bbusca" type="button" onclick="buscarUbicacion(id);">Buscar</button>
            <button id="boton_buscar_todos_ubicacion" class="bbusca" type="button" onclick="buscarTodasUbicacion();"  >Buscar todos</button>   
            <button id="boton_buscar_ubicaciones_libres" class="bbusca" type="button" onclick="buscarUbicacionesLibres();"  >Ubicaciones Libres</button>   
        </form>

        <br><br><br>
        <div class="datagrid">
            <table id="tabla_muestra_ubicacion" >
            </table> 
        </div>
        <br><br><br>
    </center>

</body>
</html>