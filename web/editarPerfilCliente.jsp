<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- <%  //File="incluir.jspf"; %>-->
<%
    String nom = "";
    try {
        HttpSession actual = request.getSession(true);
        String id = actual.getId();
        String nombre = (String) actual.getAttribute("logueado");
        String tipo = (String) actual.getAttribute("tipo");
        nom = (String) actual.getAttribute("nombre");
        System.out.println("usuario" + nombre);
        if (actual.isNew()) {
            response.sendRedirect("index.jsp");
            return;
        }
        if (actual == null) {
            response.sendRedirect("index.jsp");
        } else {
            if (actual.getAttribute("logueado") == null) {
                response.sendRedirect("index.jsp");
            }
            if (tipo.equalsIgnoreCase("admin")) {
                System.out.println("admin");
                response.sendRedirect("Administrador.jsp");
            }
        }
        out.print("<input type='hidden' id='rut_cliente' value='" + nombre + "'>");
    } catch (Exception e) {
        System.out.println(e.getMessage());
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="add/css/busca_proveedor.css"/>
        <link rel="stylesheet" type="text/css" href="add/css/awesomecomplete.css"/>
        <script type="text/javascript" src="add/js/jquery-1.9.1.js"></script>

        <script type="text/javascript" src="add/js/gestionCliente.js"></script> 
        <link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic' rel='stylesheet' type='text/css'>	
        <link rel="stylesheet" href="add/css/estiloconfirmar.css">
        <link rel="stylesheet" href="add/css/avgrund.css">
        <script type="text/javascript" src="add/js/avgrund.js"></script>
        <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>

        <!-- Para mostrar los efectos css y jquery en el menu principal -->
        <link rel="stylesheet" href="add/css/colorbox.css" />
        <script type="text/javascript" src="add/js/jquery.min.js"></script>
        <script src="add/js/jquery.colorbox.js"></script>
        <link rel="stylesheet" href="add/css/stylemenu.css" type="text/css" media="screen"/>
        <script type="text/javascript" src="add/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="add/js/jqueryimagenes.js"></script>
        <script type="text/javascript" src="add/js/jquery.awesomecomplete.js"></script>

    </head>
    <body>

        <h1 class="titulo">Perfil de <% out.print(nom);%></h1>
    <center>
        <br>
        <br>
        <input type="button" class="bbusca" value="Completar Datos" onclick="completaDatosCliente();">
        <br>
        <br>
        <table id="tabla_actualiza_clientes">
            <tr><td>Rut</td><td><input size="25" readonly="readonly" onfocus="this.blur();" type="text" class="bbusca" id="rut_act"></td>
            <tr><td>Razon Social</td><td><input onblur="comprobar_nombre(id);" size="25" type="text" class="bbusca" id="rsocial_act"></td>
            <tr><td>Dirección</td><td><input size="25" type="text" class="bbusca" id="direccion_act"></td>
            <tr><td>Ciudad</td><td><input size="25" type="text" class="bbusca" id="ciudad_act"></td>
            <tr><td>Teléfono</td><td><input onblur="validarTelefono(id);" size="25" type="text" class="bbusca" id="telefono_act"></td>
            <tr><td>Correo</td><td><input onblur="validaEmail(id);" size="25" type="text" class="bbusca" id="correo_act"></td>
        </table>
        <br>
        <input type="button" class="bbusca" value="Actualizar Datos" onclick="actualizarPersonaCliente();">
        <div id="div_respuesta_actualiza_clientes">&nbsp;</div><div id="div_resp_busca_cliente">&nbsp;</div>
        
        <br>
        <br>
        <br>
    </center>

</body>
</html>
