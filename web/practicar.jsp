<!doctype html>  
<html lang="en">
    <head>
        <meta charset="utf-8">

        <title>Ejem</title>

        <link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic' rel='stylesheet' type='text/css'>	
        <link rel="stylesheet" href="add/css/estiloconfirmar.css">
        <link rel="stylesheet" href="add/css/avgrund.css">

        <script>
            function openDialog() {
                Avgrund.show("#default-popup");
            }
            function closeDialog() {
                Avgrund.hide();
            }
        </script>

    </head>

    <body>

        <aside id="default-popup" class="avgrund-popup">
            <h2>That's all, folks</h2>
            <p>
                You can hit ESC or click outside to close the modal. Give it a go to see the reverse transition.
            </p>
            <p>
                If you like this you would probably enjoy <a href="http://lab.hakim.se/meny">Meny</a>, <a href="http://lab.hakim.se/reveal-js">reveal.js</a> and <a href="http://lab.hakim.se/scroll-effects">stroll.js</a>.
            </p>
            <button onclick="javascript:closeDialog();">Close</button>
        </aside>

        <article class="avgrund-contents">
            <h1>Hola</h1>
            <p>
                A modal concept which aims to give a sense of depth between the page and modal layers. Click the button below to give it a try.
            </p>
            <button onclick="javascript:openDialog();">Open Avgrund</button>
            <p>
                Uses CSS transforms to scale components and CSS filters to blur the page. Built for the fun of 
                it, not intended for any practical use.
            </p>
            <p>
                <em>Avgrund</em> is Swedish for abyss.
            </p>

        </article>


        <script type="text/javascript" src="add/js/avgrund.js"></script>
        <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>

    </body>
</html>